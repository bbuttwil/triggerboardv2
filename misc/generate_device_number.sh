#!/bin/bash
# Path to the file where the counter is stored
#COUNTER_FILE="/var/run/udev_device_counter"

# Ensure the counter file exists
#if [ ! -f "$COUNTER_FILE" ]; then
#    echo 0 > "$COUNTER_FILE"
#fi

# Read the current value, increment it, and store the new value
#counter=$(cat "$COUNTER_FILE")
#echo $((counter + 1)) > "$COUNTER_FILE"

# Print the incremented counter value
#echo $counter

# Get the highest number of existing triggerDev devices
max_num=$(ls /dev/triggerDev* 2>/dev/null | awk -F'triggerDev' '{print $2}' | sort -n | tail -n1)

# If no existing devices, start with 0
if [ -z "$max_num" ]; then
    max_num=0
else
    max_num=$((max_num + 1))
fi

# Print the new number
echo $max_num
