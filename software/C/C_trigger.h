// $Id: C_trigger.h 1143 2025-02-07 16:03:06Z angelov $:
#ifndef TRIGGER_H
#define TRIGGER_H

// $Id: C_trigger.h 1143 2025-02-07 16:03:06Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <tuple>
#include "lib/CLogger.h"

// global base address, used in the main progam to init the class
#define ADDR_BRIDGE         0x0000

// relative to ADDR_BRIDGE
#define OFFS_BASE_CONF      0x0100  // several config registers
#define OFFS_BASE_DAC       0x0200  // I2C or SPI master
#define OFFS_BASE_TRIGG     0x2000  // trigger unit
#define OFFS_BASE_IMEM      0x4000  // 4000 .. 5FFF CPU instruction memory (24-bit data)
#define OFFS_BASE_SHMEM     0x8100  // 8100 .. 81FF shared memory

#define CPU_IMEM_SIZE       0x1000  // in 24-bit words

// general config registers, relavive to OFFS_BASE_CONF
#define OFFS_CONF_CPU_PAGE 0x0F // 0 boot, 1 work
#define OFFS_CONF_LEDS     0x11
#define OFFS_CONF_SVN      0x19
#define OFFS_CONF_CMP      0x1C
#define OFFS_CONF_CMD      0x10
#define OFFS_CONF_VER      OFFS_CONF_CMD
// bits withinn command/ver register
#define BIT_CONF_VER_HW    1    // 3 bits, 0 or 2
#define BIT_CONF_VER_AS    6    // 2 bits, now 0
#define BIT_CONF_VER_IMG   0    // 1 bit, 0 for gold, 1 for work
#define BIT_CONF_CPU_OFF   4    // turn off/on the CPU when 1/0
#define BIT_CONF_SFT_RST   3    // soft reset, auto cleared

#define STA_FPGA_SIZE     16    // 23..16 FPGA size in kLUT
#define STA_CPU_DM_AB     12    // 15..12 address bits in CPU DMEM
#define STA_CPU_IM_AB      8    // 11.. 8 address bits in CPU IMEM


#define TRIGGER_CLOCK       200000000 // 200 MHz clock of the trigger design
#define TRG_CLK_PERIOD_NS  (1000000000.0/TRIGGER_CLOCK)
#define TRG_CLK_PERIOD_US  (1000000.0/TRIGGER_CLOCK)
#define TRG_CLK_PERIOD_MS  (1000.0/TRIGGER_CLOCK)

// number of bits when specifying dgg width and delay in 100 MHz clocks
// deat time
#define TRG_DTM_BITS               16
#define TRG_DTM_MASK    ( (1 << TRG_DTM_BITS) - 1 )
// input delay gate genarator width and delay
#define TRG_DGG_BITS                4
#define TRG_DGG_MASK    ( (1 << TRG_DGG_BITS) - 1 )
// trigger output width
#define TRG_TWD_BITS                6
#define TRG_TWD_MASK    ( (1 << TRG_TWD_BITS) - 1 )
// bits in the trigger threshold
#define TRG_THR_BITS                3
#define TRG_THR_MASK    ( (1 << TRG_THR_BITS) - 1 )
// number of channels
#define TRG_NCH                     4
#define TRG_CH_MASK     ( (1 << TRG_NCH) - 1 )


#define OFFS_TRG_INP_INV_ENA        0
// inv_b(8) & inv(7..4) & ena(3..0) => 9 bits
#define INP_INV_DEF               0x0
#define INP_ENA_DEF               0x3
#define BUSY_INV_DEF                0
#define BUSY_ENA_DEF                1

#define OFFS_TRG_DGG_WIDTH_DEL      1
// bits/channel for width and delay: 4
// channels : 4
// width & delay[3] & delay[2] & delay[1] & delay[0] => 20 bits
#define TRG_DGG_WIDTH_DEF           3
#define TRG_DGG_DELAY_DEF           0


#define OFFS_TRG_INV_THR_WIDTH      2
// inv(9) &  thresh(8..6) & width(5..0) => 8 bits
#define TRG_THR_DEF                 1
#define TRG_TWD_DEF                20
#define TRG_INV_DEF                 0

#define OFFS_TRG_DTIME              3
// dead time after triggered
#define TRG_DTIME_DEF           20000

// Past protection on channel basis 4..7

#define OFFS_PP_CHX(x)            (4+x)
#define PAST_PROT_CH_DEF          0xF9F

// end address of all configuration registers
#define MAX_REG_TRG_CNF            12

// Test generator 8..11
#define OFFS_TG_MSK0               8
#define OFFS_TG_MSK1               9
    #define SEQ_LEN 8
// bits  7.. 0 - shift register init for channel 0, first send bit7
// bits 15.. 8, 23..16, 31..24 - shift register init for channels 1..3, first send the MSBit ...
// _MSK0 : for the first group of pulses
// _MSK1 : for the second group of pulses
    #define TG_MSK0_DEF            0xFFFFFFFF
    #define TG_MSK1_DEF            0xFFFFFFFF

// upper 16 bits - distance between the two groups of pulses, lower 16 bits - period
#define OFFS_TG_DIST              10
#define OFFS_TG_PER               11
    #define TG_PERIOD_DEF        200000
    #define TG_DIST_0_TO_1_DEF    200
// writing to MSK0, MSK1 and DIS_PER resets the generator (stops if started)

// number of repetitions : bits 23..0, bit 31 is start, bit 30 is use test generator instead of the inputs
#define OFFS_TG_REP               12
    #define BITS_REPETITIONS      24
    #define BIT_TG_REP_START      31
    #define BIT_TG_REP_USE_TG     30

// the input is from the I/O [bits NCH-1..0], enable comparator output at I/O [bits 2*NCH-1..NCH]
// when bit n (0..NCH-1) is 1, bit n+NCH is automatically cleared
// power up state is 0x00
#define OFFS_INP_FR_CMP           13
    #define BIT_POS_INP_IO         0
    #define BIT_POS_IO_ENA        TRG_NCH


#define OFFS_LA_PRESAMPL          14    // 10 bit presamples, reading 23..16 LA status:
// n = bits 3..0 - size of the ring buffer is 2^n
// bits 7..4: sm state: 4 - idle, 5 - started, 6 - ready, wait for clear after reading data, 7 - finish (very short state)

#define OFFS_LA_TIMESTA           15    // 32 bit timestamp
// writing here: bit 0 time clear, bit 1 ring buffer clear
    #define LA_CMD_CLR         2
    #define LA_CMD_TIMER_CLR   1

// bits in address of the trigger unit interface
// total size 2048
// - first 1/4 is configuration
// - second 1/4 is counter
#define TRIGG_ABITS             11
// Counters + config
#define OFFS_TRG_CNT            (1 << (TRIGG_ABITS-2) )  //  second 1/4
#define OFFS_TRG_LA             (1 << (TRIGG_ABITS-1) )  //  upper half
#define LA_SAMPLES              (1 << (TRIGG_ABITS-1) )
#define LA_MEM_DEPTH            (1 << (TRIGG_ABITS-1) )


#define LA_SIGNALS      15
#define TIME_RESOL       5  // ns


#define OFFS_TRG_TIMER            (OFFS_TRG_CNT + 3)    // ro
// the 4 registers can be read at offsets 4..7 relative to OFFS_TRG_CNT

// counters, 32..32 + 4 + 4 + 4
#define N_COUNTERS                12
#define FULL_CNT_MASK             ( (1U << N_COUNTERS) - 1)
#define DEFL_CNT_MASK             FULL_CNT_MASK
#define OFFS_TRG_COUNTERS         (OFFS_TRG_CNT + 16)    // ro
// inside the counters entity
#define OFFS_TRG_PRESCALE         (OFFS_TRG_CNT + 0)
    #define BITS_TRG_PRESCALE     18
#define OFFS_TRG_END_TIME         (OFFS_TRG_CNT + 1)
#define OFFS_TRG_CMD_STA          (OFFS_TRG_CNT + 2)
    // command
    #define BIT_TRG_CNT_CLR        0
    #define BIT_TRG_CNT_START      1
    #define BIT_TRG_CNT_LATCH      2
    #define BIT_TRG_CNT_STOP       3
    // r/w
    #define BIT_TRG_CNT_CONT       4
    // status
    #define BIT_TRG_CNT_RUN        BIT_TRG_CNT_START
    #define BIT_TRG_CNT_NEW        BIT_TRG_CNT_LATCH
    // read the periods since last clear, 24-bit counter
    #define BIT_TRG_CNT_PRDS       8

#define ADDR_CPU_TASK       (OFFS_BASE_SHMEM + 0)
#define CPU_TASK_BIT_RUN    0
#define CPU_TASK_BIT_STOP   1
#define CPU_TASK_BIT_INIT   2
#define CPU_TASK_BIT_LA     3
#define CPU_TASK_BIT_CNT    4

#define ADDR_CPU_TASK_CP    (OFFS_BASE_SHMEM + 1)

#define ADDR_CPU_NSAMPLES   (OFFS_BASE_SHMEM + 2)  // number of samples to send, accepted only when < LA_SAMPLES
#define ADDR_CPU_CNT_MASK   (OFFS_BASE_SHMEM + 3)  // mask for the counters to be sent


#define DEF_LOG                     1

#define UNDEFINED   0xFFFFFFFF
#define HW_VER_NEW  2               // HW version < 2 uses I2C DAC, 2 uses SPI DAC


// from one_max_fpga8_32 class
#define AV_BASE             0x0000
#define AV_CHIP_ID         (AV_BASE + 0x28)

typedef struct
{
    const uint8_t ser_nr;       // Serial Nr. of the board
    const uint8_t doc_version;   //
    const uint8_t hw_version;   //
    const uint32_t fpga_id;     // lower 32-bits = lower 32-bits XOR upper 32-bits of the ALTERA FPGA ID
} fpga_id_type;

#define NUMBER_OF_BOARDS        10 // 0 - not found, 1-2 old PCB, 3-4 first new production, 5 - our first PCB design, second production5..
const fpga_id_type all_board_ids[NUMBER_OF_BOARDS] = {
// Ser. Nr.| PCB ver. | HW ver. | FPGA short ID
{   0,  0xFF,    0xFF,      0           }, // not found

{   1,     1,       0,      0xf0c28690  }, // old PCB
{   2,     1,       0,      0x0         }, // old PCB

{   3,     2,       2,      0x0         }, // PCB-2, produced 2023
{   4,     2,       2,      0xa562ce8c  }, // PCB-2, produced 2023

{   5,     2,       2,      0x25bdce8c  }, // PCB-2, produced 2025
{   6,     3,       2,      0x2592ce8c  }, // PCB-3, produced 2025
{   7,     3,       2,      0x9cad5998  },
{   8,     3,       2,      0x48751998  },
{   9,     3,       2,      0xa57dce8c  }};
// PCB version - layout
// HW version - for FPGA design


class C_trigger : public CLogger
{
protected:
    // pointer to the UART interface
    uart32* m_uart32;
    // base address of the SPI master in the FPGA design
    uint32_t base_addr_cnf;
    uint32_t base_addr_trg;
    uint32_t last_status;
    uint32_t last_svn;
    uint32_t last_cmp;
public:
    // set the pointer to the UART device, the base address in FPGA design, init is still not used
    C_trigger(uart32* p_uart32, uint32_t new_base_addr = 0);
    ~C_trigger();

    // copy a bit slice from src[pos_high-pos_low..0] to dst[pos_high..pos_low]
    uint16_t cp_bit_slice(uint16_t src, uint16_t dst, uint8_t pos_high, uint8_t pos_low);

    // invert busy, invert discr. inputs mask, enable discr. inputs mask
    int set_inv_ena(uint8_t ena_busy, uint8_t inv_busy, uint8_t inv_dis_inp, uint8_t ena_dis_inp, uint8_t ldebug=DEF_LOG);
    std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> get_inv_ena();

    // delay gate genarators for each discriminator inputs, width of the generated pulse and individual delays
    int set_dgg_width_delay(uint8_t dgg_width, uint8_t dgg_del0, uint8_t dgg_del1, uint8_t dgg_del2, uint8_t dgg_del3, uint8_t ldebug=DEF_LOG);
    std::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> get_dgg_width_delay();

    // set the trigger threshold (how many discriminator channels must overlap for trigger)
    //         invert trigger or not, width of the output pulse in trigger clocks
    int set_trg_thresh_inv_width(uint8_t trg_inv, uint8_t trg_thresh, uint8_t trg_width, uint8_t ldebug=DEF_LOG);
    std::tuple<uint8_t, uint8_t, uint8_t> get_trg_thresh_inv_width();

    // set the dead time after a fired trigger
    int set_trg_dtime(uint16_t trg_dtime, uint8_t ldebug=DEF_LOG);
    uint16_t get_trg_dtime();

    int set_pp_time(uint8_t ch_mask, uint16_t pp_time, uint8_t ldebug=DEF_LOG);
    std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> get_pp_time();

    int set_cmp_inp_out(uint8_t tg_inp_from_io, uint8_t cmp_out2io, uint8_t ldebug=DEF_LOG);
    int print_cmp_inp_out(uint8_t tg_inp_cmp_ena);

    int set_tg_masks(uint32_t tg_mask0, uint32_t tg_mask1, uint8_t ldebug=DEF_LOG);
    int print_tg_mask(uint32_t tg_mask);

    int set_tg_distance_period(uint32_t tg_dist, uint32_t tg_period, uint8_t ldebug=DEF_LOG);
    int set_tg_repetitions(uint32_t tg_rep, uint8_t tg_start, uint8_t tg_use, uint8_t ldebug=DEF_LOG);
    //int set_tg_repetitions(uint32_t tg_rep, uint8_t ldebug=DEF_LOG);
    int tg_disable(uint8_t ldebug=DEF_LOG);

    uint32_t get_status(void);
    uint32_t get_hw_ver(void);
    uint32_t get_svn_id(void);
    uint16_t get_svn_nr(void);
    uint32_t get_fpga_size(void);
    uint32_t get_compile_id(void);
    uint32_t print_compile_id( uint32_t cmp_id);
    uint32_t print_compile_id();
    uint32_t print_svn_id( uint32_t svn_id);
    uint32_t print_svn_id();
    // read and print the FPGA status (compilation time, svn time, configuration source...)
    uint8_t  print_dstatus();
    //uint32_t config_status();
    //uint8_t  print_fstatus();

    int set_trg_cnt(uint32_t trg_cnt_presc, uint32_t trg_cnt_endtm, uint8_t trg_cnt_cmd, uint8_t ldebug=DEF_LOG);
    int print_trg_cnt_cmd(uint8_t trg_cmd);
    int trg_cnt_cmd(uint8_t trg_cnt_cmd, uint8_t ldebug=DEF_LOG);
    std::array<uint32_t, 16> get_trg_cnt();
    int print_trg_cnt(int wait_new = 0);

    int print_trg_config();

    // len is 1..4, write to a single configuration register 1 to 4 bytes
    uint8_t  WriteReg(uint8_t len, uint32_t addr, uint32_t wdata);
    uint32_t ReadReg( uint8_t len, uint32_t addr);

    uint32_t set_cpu_state(int cpu_cmd_bits, int ldebug = DEF_LOG);
    uint32_t read_imem_file(FILE *f, uint32_t *prog_data, uint32_t max_length);
    int32_t  set_sh_mem(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc);
    // get the logic analyzer data, still don't check & wait until some event stored!
    uint32_t get_la();
    uint32_t rep_la();
    void set_la_pres(uint32_t la_pres);
    void trg_cpu_run(uint32_t trg_cmd, uint32_t nsamples = LA_SAMPLES, uint32_t cnt_mask = DEFL_CNT_MASK);

    uint32_t read_short_trace_id();
    int get_ser_nr(FILE *f);
};

#endif // TRIGGER_H
