#ifndef __ONE_MAX10_FPGA8_32__
#define __ONE_MAX10_FPGA8_32__

// $Id: one_max10_fpga8_32.h 982 2023-07-18 16:00:17Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions, for close() */
#include <fcntl.h>   /* File control definitions, for open() */
#include <signal.h>  /* ANSI C signal handling */
#include <memory>
#include <sys/time.h>
#include "CLogger.h"

// send so many 32-bit words to the flash before checking the status
// the FIFO in the UART RX should be at least 4*FLASH_WR_AT_ONCE + 10-20 bytes!
// even 16 is very good, larger number doesn't bring a lot in the programming speed.
#define FLASH_WR_AT_ONCE 64

// with or without Sweet-16 uController
#define SW16_CPU 1

// Device   UFM1   UFM0    CFM2    CFM1    CFM0    PageSize,kb UMemSize, kb    CMemSize, kb
//  10M02   3       3       0       0       34      16          96              544
//  10M04   0       8      41      29       70      16          1,248           2,240
//  10M08   8       8      41      29       70      16          1,376           2,240
//  10M16   4       4      38      28       66      32          2,368           4,224
//  10M25   4       4      52      40       92      32          3,200           5,888
//  10M40   4       4      48      36       84      64          5,888           10,752
//  10M50   4       4      48      36       84      64          5,888           10,752

// interface to flash memory
#define AV_BASE             0x0000
#define AV_CSR_BASE        (0x00 + AV_BASE)
#define AV_INT_BASE        (0x20 + AV_BASE)
#define AV_CHIP_ID         (AV_INT_BASE + 8)
#define AV_ADDR_CNT        (AV_INT_BASE + 0)
#define AV_RESET           (AV_INT_BASE + 4)
#define AV_DBG_SEL         (AV_INT_BASE + 6)
// when reading from AV_DBG_SEL:
// bits 1..0 are debug_sel
// bits 4..2 are fpga size: 0 - unspecified, 1 - 02, 2 - 04, 3 - 08, 4 - 16, 5 - 25, 6 - 40, 7 - 50
#define AV_RCU_BASE        (0x40 + AV_BASE)
#define AV_RCU_TRIGGER      AV_RCU_BASE          // Bit 0 trigger, bit 1 reset watchdog timer
#define AV_RCU_CONF_SEL    (AV_RCU_BASE+4)       // Bit 0 trigger conf_sel_overwr, bit 1 write conf_sel to the inp reg ?
#define AV_RCU_STATUS      (AV_RCU_BASE+3*4)       // Bit 0 busy

// read from here up to 128 bytes (32 dwords) at once with autoincrement, otherwise the address will go out of the range
#define AV_MEM_BASE        (0x80 + AV_BASE)
#define AV_MEM_BSIZE        128      // problems when verifying the flash memory in MAX10 ???
//#define AV_MEM_BSIZE        64         // no problems, must be 2**N

// Addresses and sizes in BYTES!!!
#define AVBUS_UFM1_BASE_08 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_08 0x04000 // 8 Pages, each 16 kb = 2kB
#define AVBUS_UFM0_BASE_08 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_08 0x04000
#define AVBUS_CFM2_BASE_08 0x08000
#define AVBUS_CFM2_SIZE_08 0x14800
#define AVBUS_CFM1_BASE_08 0x1C800
#define AVBUS_CFM1_SIZE_08 0x0E800
#define AVBUS_CFM0_BASE_08 0x2B000
#define AVBUS_CFM0_SIZE_08 0x23000
#define PAGE_SIZE_BYTES_08    2048

#define AVBUS_UFM1_BASE_16 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_16 0x04000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_16 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_16 0x04000
#define AVBUS_CFM2_BASE_16 0x08000 // 0x08000 ... 0x49FFF
#define AVBUS_CFM2_SIZE_16 0x26000
#define AVBUS_CFM1_BASE_16 0x2E000
#define AVBUS_CFM1_SIZE_16 0x1C000
#define AVBUS_CFM0_BASE_16 0x4A000
#define AVBUS_CFM0_SIZE_16 0x42000
#define PAGE_SIZE_BYTES_16    4096

#define AVBUS_UFM1_BASE_25 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_25 0x04000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_25 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_25 0x04000
#define AVBUS_CFM2_BASE_25 0x08000 // 0x08000 ... 0x63FFF
#define AVBUS_CFM2_SIZE_25 0x34000
#define AVBUS_CFM1_BASE_25 0x3C000
#define AVBUS_CFM1_SIZE_25 0x28000
#define AVBUS_CFM0_BASE_25 0x64000
#define AVBUS_CFM0_SIZE_25 0x5C000
#define PAGE_SIZE_BYTES_25    4096

#define AVBUS_UFM1_BASE_40 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_40 0x08000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_40 0x08000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_40 0x08000
#define AVBUS_CFM2_BASE_40 0x10000 // 0x10000 ... 0x6FFFF
#define AVBUS_CFM2_SIZE_40 0x60000
#define AVBUS_CFM1_BASE_40 0x70000 // 0x70000 ... 0xB7FFF
#define AVBUS_CFM1_SIZE_40 0x48000
#define AVBUS_CFM0_BASE_40 0xB8000 // 0xB8000 ...0x15FFFF
#define AVBUS_CFM0_SIZE_40 0xA8000
#define PAGE_SIZE_BYTES_40    8192

#define AVBUS_UFM1_BASE_50 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_50 0x08000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_50 0x08000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_50 0x08000
#define AVBUS_CFM2_BASE_50 0x10000 // 0x10000 ... 0x6FFFF
#define AVBUS_CFM2_SIZE_50 0x60000
#define AVBUS_CFM1_BASE_50 0x70000 // 0x70000 ... 0xB7FFF
#define AVBUS_CFM1_SIZE_50 0x48000
#define AVBUS_CFM0_BASE_50 0xB8000 // 0xB8000 ...0x15FFFF
#define AVBUS_CFM0_SIZE_50 0xA8000
#define PAGE_SIZE_BYTES_50    8192

// put later the data for FPGA size 40 and 50 kLUT!

#define UFM1    1
#define UFM0    2
#define CFM2    3
#define CFM1    4
#define CFM2p1  6
#define CFM0    5

#define ADDR_STATUS         0x110
#define ADDR_LED            0x111
#define ADDR_SVN            0x119
#define ADDR_COMPILE        0x11C

// command for configuration, can be ORed, here the only possible combinations:
//      erase, program, verify    xFM_ERASE | xFM_PROGRAM | xFM_VERIFY
//                        same                xFM_PROGRAM | xFM_VERIFY
//      erase and check if blank: xFM_ERASE | xFM_VERIFY
//      only erase:               xFM_ERASE
//      only verify:              xFM_VERIFY

#define xFM_ERASE               1
#define xFM_PROGRAM             2
#define xFM_VERIFY              4

// return code of the program when fail in verify
#define VERIFY_ERR              1

class one_max10_fpga8_32 : public CLogger
{

protected:
    uint8_t last_status;
    uint32_t last_acnt;
    uint32_t last_av_sta;
    uint32_t last_av_cnf;
    uint8_t debug;
    uart32* m_uart32;
    uint8_t fpga_size;

    // swap bits inside a byte: 0 <-> 7, 1 <-> 6, 2 <-> 5, 3 <-> 4
    uint8_t  swap_bits(uint8_t bt);

public:
    one_max10_fpga8_32(uint8_t new_debug, uart32* p_uart32);
    ~one_max10_fpga8_32();

    // 8-bit status register of the design, bit 0 is config source
    uint8_t  get_status(void);
    int32_t  set_leds(uint8_t led);
    // get (and print) subversion revision nr, date & time, compilation time etc.
    uint32_t get_svn_id(void);
    uint16_t get_svn_nr(void);
    uint8_t  get_fpga_size(void);
    uint32_t get_compile_id(void);
    uint32_t print_compile_id( uint32_t cmp_id);
    uint32_t print_compile_id();
    uint32_t print_svn_id( uint32_t svn_id);
    uint32_t print_svn_id();
    // read and print the FPGA status (compilation time, svn time, configuration source...)
    uint8_t  print_fstatus();
    // read the 64-bit Chip_ID from the FPGA, s. Altera doc
    uint64_t read_trace_id(uint8_t *trid);
    uint64_t read_trace_id();
    uint32_t read_short_trace_id();
    // read (and print) the current AV bus status register (s. Altera doc)
    uint32_t get_av_status(void);
    uint32_t print_av_status();
    // set the AV bus config register
    uint32_t set_av_config(uint32_t av_cnf);
    // read (and print) the current AV bus config register (s. Altera doc)
    uint32_t get_av_config(void);
    uint32_t print_av_config();
    // read back (and print) the current flash address
    uint32_t get_addr_cnt(void);
    uint32_t print_addr_cnt();
    // set the address counter for the subsequent flash read or write operation
    uint32_t set_addr_cnt(uint32_t acnt, uint8_t burst_l);
    // for any flash block or part of a block
    uint32_t read_flash_block(uint32_t saddr, int32_t nbytes, uint32_t *rdata);
    // using the known device size and so the addresses and the sizes of the 5 flash blocks
    uint32_t read_flash_block(uint8_t sector, uint32_t *rdata);
    // sector: byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    uint32_t write_flash_block(uint32_t sector, int32_t nbytes, uint32_t *wdata);
    // with any device size, up to now 08, 16 and 25 possible
    // byte 0 is sector id, byte 1 is start page
    uint32_t flash_start_address(uint32_t sector, uint8_t fpga_size);
    // byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    int32_t flash_size(uint32_t sector, uint8_t fpga_size);
    uint32_t flash_page_size(uint8_t fpga_size);
    // using the already set deivce size
    uint32_t flash_start_address(uint32_t sector);
    // byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    int32_t flash_size(uint32_t sector);
    // function for read/write configuration or flash files
    // write the data to a text file one byte per line in hex format
    uint32_t write_conf_file32(FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    uint32_t write_conf_file8( FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    uint32_t write_conf_file16(FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    // read the data from a file with one byte per line in hex format
    uint32_t read_conf_file8(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
    // read the data from a file with one 16-bit word per line in hex format
    uint32_t read_conf_file16(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
    // read the data from a file with one 32-bit word per line in hex format
    uint32_t read_conf_file32(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
//    uint32_t read_conf_file(FILE *f, uint8_t  *prog_data, uint32_t max_length); // destination is uint8_t
    // read the config data from .rpd file, created by quartus, and store to a array
    // the data in the file are binary (no text!), in a group of 4 the order of the bytes and of the bits
    // in the byte should be reversed!!! S. remote_update.c example of Altera!
    uint32_t read_rpd_file(FILE *f, uint32_t *prog_data, uint32_t max_length);  // destination is uint32_t
    uint32_t read_rpd_file(FILE *f, uint8_t  *prog_data, uint32_t max_length);  // destination is uint8_t
    // compare config data
    uint32_t comp_cfg_data(int32_t nbytes, uint32_t *cfg1, uint32_t *cfg2);
    // cut the tail filled with 0xFF (if any) from the configuration data, returns the new length
    // the bytes are 0xFF after erase, so 0xFF doesn't need to be programmed!
    uint32_t clip_conf_data(uint32_t *prog_data, uint32_t length); // array is uint32_t

    // trigger the reconfiguration from internal flash, image=0|1 - from image 0 or 1, any other number ->
    // using the signal at the CONFIG_SEL pin
    uint32_t trigger_reconf(uint8_t image);
    // print the configuration status register, not informative!?
    uint32_t print_msm_cs(uint8_t msm_cs);
    uint32_t config_status();
    uint32_t config_source();
    uint32_t epv_config(char *filename, uint8_t operation, uint8_t x_flash_code);

    // work with an array in the IO space
    int32_t check_sh_mem8(uint16_t msize, uint16_t baddr, uint8_t set_mem, uint8_t chk_mem);
    int32_t check_sh_mem16(uint16_t msize, uint16_t baddr, uint8_t set_mem, uint8_t chk_mem);
    uint8_t gen_psrg(uint8_t present);
    int32_t get_sh_mem(uint16_t msize, uint16_t baddr, uint8_t *sh_mem);
    int32_t set_sh_mem(uint16_t msize, uint16_t baddr, uint8_t *sh_mem);
    int32_t view_ram(uint8_t *myram, uint16_t len, uint16_t saddr);
    int32_t view_ram(uint16_t *myram, uint16_t len, uint16_t saddr);
    int32_t view_ram(uint32_t *myram, uint16_t len, uint16_t saddr);
    int32_t view_ram16(uint8_t *myram, uint16_t len, uint16_t saddr);
    int32_t view_ram(uint8_t *myram, uint16_t len, uint16_t saddr, uint16_t bytes_in_row);
    int32_t diff_ram(uint8_t *myram, uint8_t *myref, uint16_t len);
    int32_t cp_ram(uint8_t *myram, uint8_t *mydest, uint16_t len);
    uint8_t block_code(char *block_name);


protected:
    uint32_t av_reset(void);
    uint32_t clip_conf_data(uint8_t  *prog_data, uint32_t length); // array is uint8_t
    uint32_t comp_cfg_data(int32_t nbytes, uint8_t  *cfg1, uint8_t  *cfg2);
    // set the debug source, TEMP!
    uint32_t av_dbg(uint8_t dbg_sel);

};

#endif   // __ONE_MAX10_FPGA8_32__
