#ifndef ONE_MAX10_FPGA32
#define ONE_MAX10_FPGA32

// $Id: one_max10_fpga32.h 945 2022-12-14 14:52:16Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions, for close() */
#include <fcntl.h>   /* File control definitions, for open() */
#include <signal.h>  /* ANSI C signal handling */
#include <memory>
#include <sys/time.h>
#include "CLogger.h"

// Device   UFM1   UFM0    CFM2    CFM1    CFM0    PageSize,kb UMemSize, kb    CMemSize, kb
//  10M02   3       3       0       0       34      16          96              544
//  10M04   0       8      41      29       70      16          1,248           2,240
//  10M08   8       8      41      29       70      16          1,376           2,240
//  10M16   4       4      38      28       66      32          2,368           4,224
//  10M25   4       4      52      40       92      32          3,200           5,888
//  10M40   4       4      48      36       84      64          5,888           10,752
//  10M50   4       4      48      36       84      64          5,888           10,752

// boot 0x000c_0000 .. 1F
// flash_mem.data 0x0..0 - 0x0004_DFFF
// flash_mem.csr  0x000C_0028 .. 2F
// sys_id         0x000C_0020 .. 27
// ext-bridge     0x0008_0000 .. 0x000B_FFFF  size 0x4_0000

// Byte addresses, as came from QSYS!          08 device    40 device

#define AV_FLASH_MEM_DAT    0x00000000  // .. 0x0004DFFF   .. 0x15 FFFF

// Byte addresses, as came from QSYS!
#define AV_BOOT             0x00240000  // .. 0x0024001F
#define AV_FLASH_MEM_CSR    0x00240020  // .. 0x00240027
#define AV_SYS_ID           0x00240028  // .. 0x0024002F
// word addresses!
#define AV_EXT_BRIDGE       0x00080000  // .. 0x000BFFFF    size 0x40000 bytes or 0x10000 32-bit words

// attached to the external bridge, the offsets below must be added to AV_EXT_BRIDGE !
#define OFFS_EXT_BR_CONF    0x0020  // several config registers, still undefined
#define OFFS_EXT_BR_SMEM    0x0100  // small memory of 16 x 32-bit words, to play with the interface only
#define SMALL_SH_MEM_SIZE      256
#define OFFS_EXT_BR_PSRG    0x2000  // pseudorandom generator for testing the interface

// interface to flash memory
#define AV_FLASH_CSR_STA    (AV_FLASH_MEM_CSR >> 2)
#define AV_FLASH_CSR_CTR   ((AV_FLASH_MEM_CSR >> 2)+1)

#define AV_RCU_BASE        (AV_BOOT >> 2)
#define AV_RCU_TRIGGER      AV_RCU_BASE          // Bit 0 trigger reconfiguration, bit 1 reset watchdog timer
#define AV_RCU_CONF_SEL    (AV_RCU_BASE+1)       // Bit 0 trigger conf_sel_overwr, bit 1 write conf_sel to the inp reg ?
#define AV_RCU_STATUS      (AV_RCU_BASE+3)       // Bit 0 busy

// block size when reading from the flash in 32-bit dwords, the old interface had 128 bytes!
// the read time for cfm0 is here 0.9 s, with the old interface 1.7 s.
#define AV_MEM_BSIZE        512
#define FLASH_WR_AT_ONCE     64   // this must correspond to the RX FIFO depth in design, now is 64 bytes.
                                  // Because of the protocol and the status read about 16 bytes must remain free.
                                  // More than 8 doesn't bring a lot in the programming speed, but increases
                                  // the chance to discover too late a not successful write!

// Addresses and sizes in BYTES!!!
#define AVBUS_UFM1_BASE_08 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_08 0x04000 // 8 Pages, each 16 kb = 2kB
#define AVBUS_UFM0_BASE_08 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_08 0x04000
#define AVBUS_CFM2_BASE_08 0x08000
#define AVBUS_CFM2_SIZE_08 0x14800
#define AVBUS_CFM1_BASE_08 0x1C800
#define AVBUS_CFM1_SIZE_08 0x0E800
#define AVBUS_CFM0_BASE_08 0x2B000
#define AVBUS_CFM0_SIZE_08 0x23000
#define PAGE_SIZE_BYTES_08    2048

#define AVBUS_UFM1_BASE_16 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_16 0x04000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_16 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_16 0x04000
#define AVBUS_CFM2_BASE_16 0x08000 // 0x08000 ... 0x49FFF
#define AVBUS_CFM2_SIZE_16 0x26000
#define AVBUS_CFM1_BASE_16 0x2E000
#define AVBUS_CFM1_SIZE_16 0x1C000
#define AVBUS_CFM0_BASE_16 0x4A000
#define AVBUS_CFM0_SIZE_16 0x42000
#define PAGE_SIZE_BYTES_16    4096

#define AVBUS_UFM1_BASE_25 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_25 0x04000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_25 0x04000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_25 0x04000
#define AVBUS_CFM2_BASE_25 0x08000 // 0x08000 ... 0x63FFF
#define AVBUS_CFM2_SIZE_25 0x34000
#define AVBUS_CFM1_BASE_25 0x3C000
#define AVBUS_CFM1_SIZE_25 0x28000
#define AVBUS_CFM0_BASE_25 0x64000
#define AVBUS_CFM0_SIZE_25 0x5C000
#define PAGE_SIZE_BYTES_25    4096

#define AVBUS_UFM1_BASE_40 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_40 0x08000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_40 0x08000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_40 0x08000
#define AVBUS_CFM2_BASE_40 0x10000 // 0x10000 ... 0x6FFFF
#define AVBUS_CFM2_SIZE_40 0x60000
#define AVBUS_CFM1_BASE_40 0x70000 // 0x70000 ... 0xB7FFF
#define AVBUS_CFM1_SIZE_40 0x48000
#define AVBUS_CFM0_BASE_40 0xB8000 // 0xB8000 ...0x15FFFF
#define AVBUS_CFM0_SIZE_40 0xA8000
#define PAGE_SIZE_BYTES_40    8192

#define AVBUS_UFM1_BASE_50 0x00000 // 0x00000 ... 0x03FFF (bits 14..19 are 0)
#define AVBUS_UFM1_SIZE_50 0x08000 // 4 Pages, each 32 kb = 4kB
#define AVBUS_UFM0_BASE_50 0x08000 // 0x04000 ... 0x07FFF
#define AVBUS_UFM0_SIZE_50 0x08000
#define AVBUS_CFM2_BASE_50 0x10000 // 0x10000 ... 0x6FFFF
#define AVBUS_CFM2_SIZE_50 0x60000
#define AVBUS_CFM1_BASE_50 0x70000 // 0x70000 ... 0xB7FFF
#define AVBUS_CFM1_SIZE_50 0x48000
#define AVBUS_CFM0_BASE_50 0xB8000 // 0xB8000 ...0x15FFFF
#define AVBUS_CFM0_SIZE_50 0xA8000
#define PAGE_SIZE_BYTES_50    8192

#define UFM1    1
#define UFM0    2
#define CFM2    3
#define CFM1    4
#define CFM2p1  6
#define CFM0    5

// word addresses
#define ADDR_PSRG_FIFO_R   (AV_EXT_BRIDGE+OFFS_EXT_BR_PSRG+0x00)
#define ADDR_PSRG_FIFO_W   (AV_EXT_BRIDGE+OFFS_EXT_BR_PSRG+0x01)
#define ADDR_PSRG_CONFIG   (AV_EXT_BRIDGE+OFFS_EXT_BR_PSRG+0x02)
#define ADDR_PSRG_WCNT     (AV_EXT_BRIDGE+OFFS_EXT_BR_PSRG+0x03)

#define ADDR_CONF_CMD      (AV_EXT_BRIDGE+OFFS_EXT_BR_CONF+0x01)
#define BIT_CONF_CMD_CPU_RST  0    // as status - CPU is off
#define BIT_CONF_CMD_CPU_OFF  1    // as status - CPU is off
#define BIT_CONF_CMD_SFT_RST  2
#define BIT_CONF_STA_ID_RDY   2    // as status - CHIP_ID is valid
#define BIT_CONF_CMD_ADS_RST  3

#define ADDR_CONF_SVN      (AV_EXT_BRIDGE+OFFS_EXT_BR_CONF+0x10)
#define ADDR_CONF_CMP      (AV_EXT_BRIDGE+OFFS_EXT_BR_CONF+0x11)
#define ADDR_CONF_VER      (AV_EXT_BRIDGE+OFFS_EXT_BR_CONF+0x12)
#define BIT_CONF_FPGA_SZ   8
#define BIT_CONF_VER_HW    0
#define BIT_CONF_VER_AS    4
#define BIT_CONF_VER_IMG   16
// from bit 20 as readonly the status bits above: CPU_OFF, SFT_RST -> CHIP_ID_RDY
#define ADDR_CHIP_ID       (AV_EXT_BRIDGE+OFFS_EXT_BR_CONF+0x14)

// command for configuration, can be ORed, here the only possible combinations:
//      erase, program, verify    xFM_ERASE | xFM_PROGRAM | xFM_VERIFY
//                        same                xFM_PROGRAM | xFM_VERIFY
//      erase and check if blank: xFM_ERASE | xFM_VERIFY
//      only erase:               xFM_ERASE
//      only verify:              xFM_VERIFY

#define xFM_ERASE               1
#define xFM_PROGRAM             2
#define xFM_VERIFY              4

// return code of the program when fail in verify
#define VERIFY_ERR              1

#define UNKNOWN 0xFFFFFFFF

class one_max10_fpga32 : public CLogger
{

protected:
    uint32_t last_status;
    uint32_t last_svn;
    uint32_t last_cmp;
    uint32_t last_av_sta;
    uint32_t last_av_cnf;
    uint8_t debug;
    uint32_t last_cmd_s;
    uint32_t last_cmd_r;
    uart32* m_uart32;
    uint8_t fpga_size;

    // swap bits inside a byte: 0 <-> 7, 1 <-> 6, 2 <-> 5, 3 <-> 4
    uint8_t  swap_bits(uint8_t bt);

public:
    one_max10_fpga32(uint8_t new_debug, uart32* p_uart32, uint8_t init=0);
    ~one_max10_fpga32();

    // 8-bit status register of the design, bit 0 is config source
    uint32_t  get_status(void);
    int32_t  set_leds(uint8_t led);
    // get (and print) subversion revision nr, date & time, compilation time etc.
    uint32_t get_svn_id(void);
    uint16_t get_svn_nr(void);
    uint8_t  get_fpga_size(void);
    uint32_t get_compile_id(void);
    uint32_t print_compile_id( uint32_t cmp_id);
    uint32_t print_compile_id();
    uint32_t print_svn_id( uint32_t svn_id);
    uint32_t print_svn_id();
    // read and print the FPGA status (compilation time, svn time, configuration source...)
    uint8_t  print_fstatus();

    // read the 64-bit Chip_ID from the FPGA, s. Altera doc
    uint64_t read_trace_id();
    uint32_t read_short_trace_id();

    // read (and print) the current AV bus status register (s. Altera doc)
    uint32_t get_av_status(void);
    uint32_t print_av_status();

    // set the AV bus config register
    uint32_t set_av_config(uint32_t av_cnf);
    // read (and print) the current AV bus config register (s. Altera doc)
    uint32_t get_av_config(void);
    uint32_t print_av_config();

    uint32_t config_status();
    uint32_t config_source();

    // for any flash block or part of a block
    uint32_t read_flash_block(uint32_t saddr, int32_t nbytes, uint32_t *rdata);
    // using the known device size and so the addresses and the sizes of the 5 flash blocks
    uint32_t read_flash_block(uint8_t sector, uint32_t *rdata);
    // trigger the reconfiguration from internal flash, image=0|1 - from image 0 or 1, any other number ->
    // using the signal at the CONFIG_SEL pin
    uint32_t trigger_reconf(uint8_t image);

    // sector: byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    uint32_t write_flash_block(uint32_t sector, int32_t nbytes, uint32_t *wdata);
    uint32_t epv_config(char *filename, uint8_t operation, uint8_t x_flash_code);

    // with any device size, up to now 08, 16 and 25 possible
    // byte 0 is sector id, byte 1 is start page
    uint32_t flash_start_address(uint32_t sector, uint8_t fpga_size);
    // byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    int32_t flash_size(uint32_t sector, uint8_t fpga_size);
    uint32_t flash_page_size(uint8_t fpga_size);
    // using the already set deivce size
    uint32_t flash_start_address(uint32_t sector);
    // byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
    int32_t flash_size(uint32_t sector);
    // function for read/write configuration or flash files
    // write the data to a text file one byte per line in hex format
    uint32_t write_conf_file32(FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    uint32_t write_conf_file8( FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    uint32_t write_conf_file16(FILE *f_out, uint32_t *rdata, int32_t nbytes);    // source is uint32_t
    // read the data from a file with one byte per line in hex format
    uint32_t read_conf_file8(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
    // read the data from a file with one 16-bit word per line in hex format
    uint32_t read_conf_file16(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
    // read the data from a file with one 32-bit word per line in hex format
    uint32_t read_conf_file32(FILE *f, uint32_t *prog_data, uint32_t max_length); // destination is uint32_t
//    uint32_t read_conf_file(FILE *f, uint8_t  *prog_data, uint32_t max_length); // destination is uint8_t
    // read the config data from .rpd file, created by quartus, and store to a array
    // the data in the file are binary (no text!), in a group of 4 the order of the bytes and of the bits
    // in the byte should be reversed!!! S. remote_update.c example of Altera!
    uint32_t read_rpd_file(FILE *f, uint32_t *prog_data, uint32_t max_length);  // destination is uint32_t
    uint32_t read_rpd_file(FILE *f, uint8_t  *prog_data, uint32_t max_length);  // destination is uint8_t
    // compare config data
    uint32_t comp_cfg_data(int32_t nbytes, uint32_t *cfg1, uint32_t *cfg2);
    // cut the tail filled with 0xFF (if any) from the configuration data, returns the new length
    // the bytes are 0xFF after erase, so 0xFF doesn't need to be programmed!
    uint32_t clip_conf_data(uint32_t *prog_data, uint32_t length); // array is uint32_t

    // print the configuration status register, not informative!?
    uint32_t print_msm_cs(uint8_t msm_cs);

    // work with an array in the IO space
    int32_t check_sh_mem(uint16_t msize, uint8_t bitsize, uint32_t baddr, uint8_t ainc, uint8_t set_mem, uint8_t chk_mem, uint32_t seed=0);
    uint32_t psrg_next(uint8_t nbits, uint32_t present);

    int32_t get_sh_mem(  uint32_t msize, uint32_t baddr, uint8_t  *sh_mem, uint8_t ainc=1);
    int32_t get_sh_mem(  uint32_t msize, uint32_t baddr, uint16_t *sh_mem, uint8_t ainc=1);
    int32_t get_sh_mem24(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc=1);
    int32_t get_sh_mem(  uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc=1);

    int32_t set_sh_mem(  uint32_t msize, uint32_t baddr, uint8_t  *sh_mem, uint8_t ainc=1);
    int32_t set_sh_mem(  uint32_t msize, uint32_t baddr, uint16_t *sh_mem, uint8_t ainc=1);
    int32_t set_sh_mem24(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc=1);
    int32_t set_sh_mem(  uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc=1);

    int32_t view_ram(  uint8_t  *myram, uint16_t len, uint32_t saddr, uint16_t bytes_in_row);
    int32_t view_ram(  uint16_t *myram, uint16_t len, uint32_t saddr, uint16_t words_in_row);
    int32_t view_ram24(uint32_t *myram, uint16_t len, uint32_t saddr, uint16_t dwords_in_row);
    int32_t view_ram(  uint32_t *myram, uint16_t len, uint32_t saddr, uint16_t dwords_in_row);

    int32_t diff_ram(uint8_t *myram, uint8_t *myref, uint16_t len);
    int32_t cp_ram(uint8_t *myram, uint8_t *mydest, uint16_t len);
    uint8_t block_code(char *block_name);

    uint8_t psrg_fifo_rd_test(uint8_t nbits, uint32_t seed, uint32_t nwords);
    uint32_t send_command(uint8_t command);
    uint32_t get_command();

#ifdef FLASH_READY

#endif

protected:
//    uint32_t av_reset(void);
    uint32_t clip_conf_data(uint8_t  *prog_data, uint32_t length); // array is uint8_t
    uint32_t comp_cfg_data(int32_t nbytes, uint8_t  *cfg1, uint8_t  *cfg2);

};

#endif
