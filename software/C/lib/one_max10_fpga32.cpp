
// $Id: one_max10_fpga32.cpp 963 2023-05-18 07:27:13Z angelov $:

#include "iostream"
#include "ctime"
#include "uart32.h"
#include "one_max10_fpga32.h"
#include <math.h>
#include <memory>

one_max10_fpga32::one_max10_fpga32(uint8_t new_debug, uart32* p_uart32, uint8_t init)
{
    m_uart32 = p_uart32;
    debug = new_debug;
    if (init)
    {
        get_fpga_size();
        logWrite( "Got FPGA size from design %02d\n", fpga_size);
    }
    else
        fpga_size = 0; // unknown

    last_status = UNKNOWN;
    last_svn    = UNKNOWN;
    last_cmp    = UNKNOWN;
    last_cmd_s  = UNKNOWN;
    last_cmd_r  = UNKNOWN;
    last_av_cnf = UNKNOWN;
    last_av_sta = UNKNOWN;
}


one_max10_fpga32::~one_max10_fpga32()
{
    // do nothing
}

    // get (and print) subversion revision nr, date & time, compilation time etc.
uint32_t one_max10_fpga32::get_svn_id(void)
{
    last_svn=m_uart32->Read3Bytes(ADDR_CONF_SVN);
    return last_svn;
}

uint16_t one_max10_fpga32::get_svn_nr(void)
{
    return (m_uart32->Read3Bytes(ADDR_CONF_SVN) >> 13) & 0x7FF;
}

uint32_t one_max10_fpga32::get_compile_id(void)
{
    last_cmp=m_uart32->Read3Bytes(ADDR_CONF_CMP);
    return last_cmp;
}

uint32_t one_max10_fpga32::print_compile_id(uint32_t cmp_id)
{
   logWrite(  "# Compiled on %4d-%02d-%02d at %02d:%02d\n", (cmp_id       & 0x0F) + 2015,
                                                            (cmp_id >> 4) & 0x0F,
                                                            (cmp_id >> 8) & 0x1F,
                                                            (cmp_id >>13) & 0x1F,
                                                            (cmp_id >>18) & 0x3F);

    return cmp_id;
}

uint32_t one_max10_fpga32::print_compile_id()
{
    return print_compile_id(get_compile_id());
}

uint32_t one_max10_fpga32::print_svn_id(uint32_t svn_id)
{
    logWrite(  "# SVN revision %d from %4d-%02d-%02d\n", (svn_id >>13) & 0x7FF,
                                                         (svn_id       & 0x0F) + 2015,
                                                         (svn_id >> 4) & 0x0F,
                                                         (svn_id >> 8) & 0x1F);
    return svn_id;
}

uint32_t one_max10_fpga32::print_svn_id()
{
    return print_svn_id(get_svn_id() );
}

uint32_t one_max10_fpga32::get_status(void)
{
    last_status = m_uart32->Read3Bytes(ADDR_CONF_VER);
    return last_status;
}

    // read (and print) the current AV bus status register (s. Altera doc)
uint32_t one_max10_fpga32::get_av_status(void)
{
    last_av_sta = m_uart32->ReadDWord(AV_FLASH_CSR_STA);
    return last_av_sta;
}

uint32_t one_max10_fpga32::print_av_status()
{
    logWrite(  "# Flash Status: 0x%08x Flash interface is ", last_av_sta);

    switch (last_av_sta & 3)
    {
        case 0 : {logWrite(  "idle."); break; }
        case 1 : {logWrite(  "busy-erasing."); break; }
        case 2 : {logWrite(  "busy-writing."); break; }
        case 3 : {logWrite(  "busy-reading."); break; }
    }

    logWrite(  " Read Success %d, Write Success %d, Erase Success %d\n",
                        (last_av_sta >> 2) & 1,
                        (last_av_sta >> 3) & 1,
                        (last_av_sta >> 4) & 1 );

    logWrite(  "# Protection UFM1:%d, UFM0:%d, CFM2:%d, CFM1:%d, CFM0:%d\n",
                        (last_av_sta >> 5) & 1,
                        (last_av_sta >> 6) & 1,
                        (last_av_sta >> 7) & 1,
                        (last_av_sta >> 8) & 1,
                        (last_av_sta >> 9) & 1);

    return last_av_sta;
}

    // set the AV bus config register
uint32_t one_max10_fpga32::set_av_config(uint32_t av_cnf)
{
    av_cnf |= 0xF0000000;
    m_uart32->WriteDWord(AV_FLASH_CSR_CTR, av_cnf);
    last_av_cnf = av_cnf;
    if (debug)
    {
        logWrite( "# Just wrote to AV CFG 0x%08X\n", av_cnf);


        print_av_config();
    }
    return last_av_cnf;
}


uint32_t one_max10_fpga32::send_command(uint8_t command)
{
    if ( (command >> BIT_CONF_CMD_CPU_RST) & 1) logWrite( "# Send CPU reset\n");
    if ( (command >> BIT_CONF_CMD_CPU_OFF) & 1) logWrite( "# Send CPU off\n");
    if ( (command >> BIT_CONF_CMD_SFT_RST) & 1) logWrite( "# Send soft reset\n");
    if ( (command >> BIT_CONF_CMD_ADS_RST) & 1) logWrite( "# Send ADS reset\n");
    m_uart32->WriteByte(ADDR_CONF_CMD, command);
    last_cmd_s = command;
    return 0;
}

uint32_t one_max10_fpga32::get_command()
{
    last_cmd_r = m_uart32->ReadByte(ADDR_CONF_CMD);
    if ( (last_cmd_r >> BIT_CONF_CMD_CPU_RST) & 1) logWrite( "# CPU is off\n");
    if ( (last_cmd_r >> BIT_CONF_CMD_SFT_RST) & 1) logWrite( "# CHIP_ID is valid\n");
    return last_cmd_r;
}

    // read (and print) the current AV bus config register (s. Altera doc)
uint32_t one_max10_fpga32::get_av_config(void)
{
    last_av_cnf = m_uart32->ReadDWord(AV_FLASH_CSR_CTR);
    return last_av_cnf;
}

uint32_t one_max10_fpga32::print_av_config()
{
    logWrite(  "# Flash Control Reg: Erase Page Addr 0x%05x, Sector ", last_av_cnf & 0xFFFFF);

    switch ((last_av_cnf >> 20) & 7)
    {
        case 1 : {logWrite(  "UFM1."); break; }
        case 2 : {logWrite(  "UFM0."); break; }
        case 3 : {logWrite(  "CFM2."); break; }
        case 4 : {logWrite(  "CFM1."); break; }
        case 5 : {logWrite(  "CFM0."); break; }
        case 7 : {logWrite(  "not set."); break; }
        default : logWrite(  "invalid!");
    }

    logWrite(  " WP UFM1:%d, UFM0:%d, CFM2:%d, CFM1:%d, CFM0:%d\n",
                        (last_av_cnf >> 23) & 1,
                        (last_av_cnf >> 24) & 1,
                        (last_av_cnf >> 25) & 1,
                        (last_av_cnf >> 26) & 1,
                        (last_av_cnf >> 27) & 1);

    return last_av_cnf;
}

uint8_t one_max10_fpga32::get_fpga_size(void)
{
    uint32_t sta;

    if (fpga_size > 0) return fpga_size;

    sta = get_status();
    fpga_size = (sta >> BIT_CONF_FPGA_SZ) & 0xFF;
    return fpga_size;
}

    // read and print the FPGA status (compilation time, svn time, configuration source...)
uint8_t one_max10_fpga32::print_fstatus()
{
    uint32_t  sta;
    uint64_t tr_id;
    uint32_t tr_id_short;

    logWrite(  "#\n");


    sta = get_status();
    logWrite(  "# FPGA size is %02d kLUT\n", (sta >> BIT_CONF_FPGA_SZ) & 0xFF);
    logWrite(  "# Hardware Ver %d, Assembly Ver %d\n", (sta >> BIT_CONF_VER_HW) & 0xF, (sta >> BIT_CONF_VER_AS) & 0xF);
    logWrite(  "# Design compiled for image %d\n", (sta >> BIT_CONF_VER_IMG) & 1);

    tr_id = read_trace_id();
    tr_id_short = (tr_id ^ (tr_id >> 32)) & 0xFFFFFFFF;
    logWrite(  "# Full TRACE_ID is: %08x %08x, short id is 0x%08x\n", (uint32_t) (tr_id >> 32), (uint32_t) (tr_id & 0xFFFFFFFF), tr_id_short );

    logWrite(  "# Status of the interface to the internal flash memory\n");

    get_av_status();
    print_av_status();
    get_av_config();
    print_av_config();

    print_svn_id();
    print_compile_id();
    logWrite(  "# Boot info:\n");

    config_status();
    logWrite(  "#\n");

    return 0;
}

    // read the 64-bit Chip_ID from the FPGA, s. Altera doc
uint64_t one_max10_fpga32::read_trace_id()
{
    uint64_t wrk;
    uint32_t trace_id[2];

    m_uart32->RecvBurst(2, 1, 1, ADDR_CHIP_ID, trace_id);

    wrk = trace_id[1];
    wrk <<= 32;
    wrk |= trace_id[0];

    return wrk;
}

uint32_t one_max10_fpga32::read_short_trace_id()
{
    uint32_t trace_id[2];

    m_uart32->RecvBurst(2, 1, 1, ADDR_CHIP_ID, trace_id);

    return trace_id[0] ^ trace_id[1];
}

uint32_t one_max10_fpga32::flash_page_size(uint8_t fpga_size)
{
    switch (fpga_size)
    {
        case 8:
            return PAGE_SIZE_BYTES_08;

        case 16:
            return PAGE_SIZE_BYTES_16;

        case 25:
            return PAGE_SIZE_BYTES_25;

        case 40:
            return PAGE_SIZE_BYTES_40;

        case 50:
            return PAGE_SIZE_BYTES_50;

        default:
            return 0;
    }
}

    // with any FPGA device size, up to now 08, 16, 25, 40 and 50 possible
    // byte 0 is sector id, byte 1 is start page, only for UFMx!
uint32_t one_max10_fpga32::flash_start_address(uint32_t sector, uint8_t fpga_size)
{
    uint32_t offset;

    offset  = (sector >> 8) & 0xFF;
    offset *= flash_page_size(fpga_size);
    sector &= 0xFF;

    if ( (sector < 1) || (sector > 6) ) return 0xFFFFF;

    if (debug)
    {
       logWrite( "FPGA size is %d, sector is %d\n", fpga_size, sector);

    }
    switch (fpga_size)
    {
        case 8:
            switch (sector)
            {
                case CFM0: // CFM0
                    return AVBUS_CFM0_BASE_08;
                case CFM1: // CFM1
                    return AVBUS_CFM1_BASE_08;
                case CFM2p1: // CFM2+1
                case CFM2: // CFM2
                    return AVBUS_CFM2_BASE_08;
                case UFM0: // UFM0
                    return AVBUS_UFM0_BASE_08 + offset;
                case UFM1: // UFM1
                    return AVBUS_UFM1_BASE_08 + offset;
            }

        case 16:
            switch (sector)
            {
                case CFM0: // CFM0
                    return AVBUS_CFM0_BASE_16;
                case CFM1: // CFM1
                    return AVBUS_CFM1_BASE_16;
                case CFM2p1: // CFM2+1
                case CFM2: // CFM2
                    return AVBUS_CFM2_BASE_16;
                case UFM0: // UFM0
                    return AVBUS_UFM0_BASE_16 + offset;
                case UFM1: // UFM1
                    return AVBUS_UFM1_BASE_16 + offset;
            }

        case 25:
            switch (sector)
            {
                case CFM0: // CFM0
                    return AVBUS_CFM0_BASE_25;
                case CFM1: // CFM1
                    return AVBUS_CFM1_BASE_25;
                case CFM2p1: // CFM2+1
                case CFM2: // CFM2
                    return AVBUS_CFM2_BASE_25;
                case UFM0: // UFM0
                    return AVBUS_UFM0_BASE_25 + offset;
                case UFM1: // UFM1
                    return AVBUS_UFM1_BASE_25 + offset;
            }

        case 40:
            switch (sector)
            {
                case CFM0: // CFM0
                    return AVBUS_CFM0_BASE_40;
                case CFM1: // CFM1
                    return AVBUS_CFM1_BASE_40;
                case CFM2p1: // CFM2+1
                case CFM2: // CFM2
                    return AVBUS_CFM2_BASE_40;
                case UFM0: // UFM0
                    return AVBUS_UFM0_BASE_40 + offset;
                case UFM1: // UFM1
                    return AVBUS_UFM1_BASE_40 + offset;
            }

        case 50:
            switch (sector)
            {
                case CFM0: // CFM0
                    return AVBUS_CFM0_BASE_50;
                case CFM1: // CFM1
                    return AVBUS_CFM1_BASE_50;
                case CFM2p1: // CFM2+1
                case CFM2: // CFM2
                    return AVBUS_CFM2_BASE_50;
                case UFM0: // UFM0
                    return AVBUS_UFM0_BASE_50 + offset;
                case UFM1: // UFM1
                    return AVBUS_UFM1_BASE_50 + offset;
            }

    default : return 0xFFFFFFFF;
    }
}

    // with any FPGA device size, up to now 08, 16, 25, 40 and 50 possible
    // byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
int32_t one_max10_fpga32::flash_size(uint32_t sector, uint8_t fpga_size) // in bytes
{
    int32_t page_beg, page_len, sec_size;


    page_beg = (sector >>  8) & 0xFF;
    page_len = (sector >> 16) & 0xFF;

    sector &= 7;

    //logWrite( "sector requested %d, page %d, page-length %d, fpga size %d\n", sector, page_beg, page_len, fpga_size);

    if ( (sector < 1) || (sector > 6) )
    {
        logWrite( "Wrong sector requested %d, page %d, page-length %d\n", sector, page_beg, page_len);

        return 0;
    }

    switch (fpga_size)
    {
        case 8:
        {
            switch (sector)
            {
                case CFM0: // CFM0
                    { sec_size =  AVBUS_CFM0_SIZE_08; break;}
                case CFM1: // CFM1
                    { sec_size =  AVBUS_CFM1_SIZE_08; break;}
                case CFM2: // CFM2
                    { sec_size =  AVBUS_CFM2_SIZE_08; break;}
                case CFM2p1: // CFM2+1
                    { sec_size =  (AVBUS_CFM1_SIZE_08 + AVBUS_CFM2_SIZE_08); break;}
                case UFM0: // UFM0
                    { sec_size =  AVBUS_UFM0_SIZE_08; break;}
                case UFM1: // UFM1
                    { sec_size =  AVBUS_UFM1_SIZE_08; break;}
            }
            break;
        }

        case 16:
        {
            switch (sector)
            {
                case CFM0: // CFM0
                    { sec_size =  AVBUS_CFM0_SIZE_16; break;}
                case CFM1: // CFM1
                    { sec_size =  AVBUS_CFM1_SIZE_16; break;}
                case CFM2: // CFM0
                    { sec_size =  AVBUS_CFM2_SIZE_16; break;}
                case CFM2p1: // CFM2+1
                    { sec_size =  (AVBUS_CFM1_SIZE_16 + AVBUS_CFM2_SIZE_16); break;}
                case UFM0: // UFM0
                    { sec_size =  AVBUS_UFM0_SIZE_16; break;}
                case UFM1: // UFM1
                    { sec_size =  AVBUS_UFM1_SIZE_16; break;}
            }
            break;
        }

        case 25:
        {
            switch (sector)
            {
                case CFM0: // CFM0
                    { sec_size =  AVBUS_CFM0_SIZE_25; break;}
                case CFM1: // CFM1
                    { sec_size =  AVBUS_CFM1_SIZE_25; break;}
                case CFM2: // CFM2
                    { sec_size =  AVBUS_CFM2_SIZE_25; break;}
                case CFM2p1: // CFM2+1
                    { sec_size =  (AVBUS_CFM1_SIZE_25 + AVBUS_CFM2_SIZE_25); break;}
                case UFM0: // UFM0
                    { sec_size =  AVBUS_UFM0_SIZE_25; break;}
                case UFM1: // UFM1
                    { sec_size =  AVBUS_UFM1_SIZE_25; break;}
            }
            break;
        }
        case 40:
        {
            switch (sector)
            {
                case CFM0: // CFM0
                    { sec_size =  AVBUS_CFM0_SIZE_40; break;}
                case CFM1: // CFM1
                    { sec_size =  AVBUS_CFM1_SIZE_40; break;}
                case CFM2: // CFM2
                    { sec_size =  AVBUS_CFM2_SIZE_40; break;}
                case CFM2p1: // CFM2+1
                    { sec_size = (AVBUS_CFM1_SIZE_40 + AVBUS_CFM2_SIZE_40); break;}
                case UFM0: // UFM0
                    { sec_size =  AVBUS_UFM0_SIZE_40; break;}
                case UFM1: // UFM1
                    { sec_size =  AVBUS_UFM1_SIZE_40; break;}
            }
            break;
        }

        case 50:
        {
            switch (sector)
            {
                case CFM0: // CFM0
                    { sec_size =  AVBUS_CFM0_SIZE_50; break;}
                case CFM1: // CFM1
                    { sec_size =  AVBUS_CFM1_SIZE_50; break;}
                case CFM2: // CFM2
                    { sec_size =  AVBUS_CFM2_SIZE_50; break;}
                case CFM2p1: // CFM2+1
                    { sec_size = (AVBUS_CFM1_SIZE_50 + AVBUS_CFM2_SIZE_50); break;}
                case UFM0: // UFM0
                    { sec_size =  AVBUS_UFM0_SIZE_50; break;}
                case UFM1: // UFM1
                    { sec_size =  AVBUS_UFM1_SIZE_50; break;}
            }
            break;
        }
    default : sec_size =  0;
    }
//    logWrite( "sec size is 0x%06x\n", sec_size);
    if (page_len == 0) return sec_size;

    page_len *= flash_page_size(fpga_size);
    if (page_len <= sec_size)
       return page_len;
    logWrite( "Invalid sector size requested with sector=0x%06x !\n", sector);

    return sec_size;
}
    // using the already set deivce size
    // byte 0 is sector id, byte 1 is start page
uint32_t one_max10_fpga32::flash_start_address(uint32_t sector)
{
    return flash_start_address(sector, get_fpga_size());
}

// sector: byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
int32_t one_max10_fpga32::flash_size(uint32_t sector) // in bytes
{
    return flash_size(sector, get_fpga_size());
}

    // function for read/write configuration or flash files
    // write the data to a text file one byte per line in hex format
uint32_t one_max10_fpga32::write_conf_file32(FILE *f_out, uint32_t *rdata, int32_t nbytes)
{
    nbytes &= 0xFFFFFFFC;
    while (nbytes > 0)
    {
        fprintf(f_out, "0x%08x\n",  *rdata);
        rdata++;
        nbytes -= 4;
    }
    return 0;
}

uint32_t one_max10_fpga32::write_conf_file16(FILE *f_out, uint32_t *rdata, int32_t nbytes)
{
    nbytes &= 0xFFFFFFFC;
    while (nbytes > 0)
    {
        fprintf(f_out, "0x%04x\n",   *rdata        & 0xFFFF);
        fprintf(f_out, "0x%04x\n",  (*rdata >> 16) & 0xFFFF);
        rdata++;
        nbytes -= 4;
    }
    return 0;
}

uint32_t one_max10_fpga32::write_conf_file8(FILE *f_out, uint32_t *rdata, int32_t nbytes)
{
    nbytes &= 0xFFFFFFFC;
    while (nbytes > 0)
    {
        fprintf(f_out, "0x%02x\n",  *rdata        & 0xFF);
        fprintf(f_out, "0x%02x\n", (*rdata >>  8) & 0xFF);
        fprintf(f_out, "0x%02x\n", (*rdata >> 16) & 0xFF);
        fprintf(f_out, "0x%02x\n", (*rdata >> 24) & 0xFF);
        rdata++;
        nbytes -= 4;
    }
    return 0;
}

    // compare config data
uint32_t one_max10_fpga32::comp_cfg_data(int32_t nbytes, uint8_t *cfg1, uint8_t *cfg2)
{
    uint32_t err;
    int32_t i;

    err = 0;

    for (i=0; i<nbytes; i++)
    {
        if (*cfg1 != *cfg2) err++;
        cfg1++;
        cfg2++;
    }
    return err;
}

    // compare config data
uint32_t one_max10_fpga32::comp_cfg_data(int32_t nbytes, uint32_t *cfg1, uint32_t *cfg2)
{
    uint32_t err;
    int32_t i;

    err = 0;

    for (i=0; i<nbytes; i+=4)
    {
        if (*cfg1 != *cfg2) err++;
        cfg1++;
        cfg2++;
    }
    return err;
}

    // cut the tail filled with 0xFF (if any) from the configuration data, returns the new length
    // the bytes are 0xFF after erase, so 0xFF doesn't need to be programmed!
uint32_t one_max10_fpga32::clip_conf_data(uint8_t *prog_data, uint32_t length)
{
    uint8_t *prog_data_end;

    prog_data_end = prog_data + length - 1; // point to the last
    while ( (*prog_data_end == 0xFF) && (length > 0) )
    {
        length--;
        prog_data_end--;
    }
    return length;
}

uint32_t one_max10_fpga32::clip_conf_data(uint32_t *prog_data, uint32_t length)
{
    uint32_t *prog_data_end;
    prog_data_end = prog_data + length - 1; // end address
    while ( (*prog_data_end == 0xFFFFFFFF) && (length > 0) )
    {
        length--;
        prog_data_end--;
    }
    return length;
}

uint32_t one_max10_fpga32::read_conf_file8(FILE *f, uint32_t *prog_data, uint32_t max_length)
{
    char linebuf[128];
    uint32_t dat, sh;
    int  args;
    uint32_t nbytes;

    if (max_length == 0) max_length--; // 0xFFF..FF
    nbytes = 0;
    sh     = 0;
    while ( (fgets(linebuf, 128, f)) && (nbytes < max_length) )
    {
        args = sscanf(linebuf, "%x", &dat);
        if (args >= 1)
        {
            *prog_data |= (dat & 0xFF) << sh;
            nbytes++;
            sh += 8;
            if (sh == 32)
            {
                sh = 0;
                if (debug)
                {
                    logWrite( "read_conf_file32: %4d  0x%08x\n", nbytes-4, *prog_data);

                }
                prog_data++;
            }

        }
    }
    return nbytes;
}

uint32_t one_max10_fpga32::read_conf_file32(FILE *f, uint32_t *prog_data, uint32_t max_length)
{
    char linebuf[128];
    uint32_t dat;
    int  args;
    uint32_t nbytes;

    if (max_length == 0) max_length--; // 0xFFF..FF
    nbytes = 0;
    while ( (fgets(linebuf, 128, f)) && (nbytes < max_length) )
    {
        args = sscanf(linebuf, "%x", &dat);
        if (args >= 1)
        {
            *prog_data = dat;
            if (debug)
            {
               logWrite( "read_conf_file32: %4d  0x%08x\n", nbytes, *prog_data);

            }

            nbytes += 4;
            prog_data++;
        }
    }
    return nbytes;
}

uint32_t one_max10_fpga32::read_conf_file16(FILE *f, uint32_t *prog_data, uint32_t max_length)
{
    char linebuf[128];
    uint32_t dat, sh;
    int  args;
    uint32_t nbytes;

    if (max_length == 0) max_length--; // 0xFFF..FF
    nbytes = 0;
    sh     = 0;
    while ( (fgets(linebuf, 128, f)) && (nbytes < max_length) )
    {
        args = sscanf(linebuf, "%x", &dat);
        if (args >= 1)
        {
            *prog_data |= (dat & 0xFFFF) << sh;
            nbytes += 2;
            sh += 16;
            if (sh == 32)
            {
                sh = 0;
                if (debug)
                {
                    logWrite( "read_conf_file32: %4d  0x%08x\n", nbytes-4, *prog_data);

                }
                prog_data++;
            }

        }
    }
    return nbytes;
}

    // swap bits inside a byte: 0 <-> 7, 1 <-> 6, 2 <-> 5, 3 <-> 4
uint8_t one_max10_fpga32::swap_bits(uint8_t bt)
{
    bt = ( (bt & 0xaa) >> 1) | ( (bt & 0x55) << 1);
    bt = ( (bt & 0xcc) >> 2) | ( (bt & 0x33) << 2);
    bt = ( (bt & 0xf0) >> 4) | ( (bt & 0x0f) << 4);
    return bt;
}

    // read the config data from .rpd file, created by quartus, and store to a array
    // the data in the file are binary (no text!), in a group of 4 the order of the bytes and of the bits
    // in the byte should be reversed!!! S. remote_update.c example of Altera!
uint32_t one_max10_fpga32::read_rpd_file(FILE *f, uint8_t *prog_data, uint32_t max_length)
{
    uint8_t c[4];
    int ci, i;
    uint32_t nbytes;

    nbytes = 0;
    if (max_length == 0) max_length--; // 0xFFF..FF
    do
    {
        for (i=0; i<4; i++)
        {
            ci = fgetc(f);
            if (ci != EOF)
                c[i] = swap_bits(ci & 0xFF);
            else
                break;
        }
        if (ci != EOF)
        for (i=3; i>=0; i--)
        {
            *prog_data = c[i];
            prog_data++;
            nbytes++;
        }
    }
    while ( (ci != EOF) && (nbytes < max_length) );
    return nbytes;
}

uint32_t one_max10_fpga32::read_rpd_file(FILE *f, uint32_t *prog_data, uint32_t max_length)
{
    uint8_t c[4];
    int ci, i;
    uint32_t nbytes;

    nbytes = 0;
    if (max_length == 0) max_length--; // 0xFFF..FF
    do
    {
        for (i=0; i<4; i++)
        {
            ci = fgetc(f);
            if (ci != EOF)
                c[i] = swap_bits(ci & 0xFF);
            else
                break;
        }
        if (ci != EOF)
        {
            *prog_data = (c[0] << 24) | (c[1] << 16) | (c[2] << 8) | c[3];
            prog_data++;
            nbytes += 4;
        }
    }
    while ( (ci != EOF) && (nbytes < max_length) );
    return nbytes;
}

uint32_t one_max10_fpga32::print_msm_cs(uint8_t msm_cs)
{
    // Master State Machine Current State Description
// msm_cs Values        Description
//  0010                Image 0 is being loaded
//  0011                Image 1 is being loaded after a revert in app image happens
//  0100                Image 1 is being loaded
//  0101                Image 0 is being loaded after a revert in app image happens
    switch (msm_cs)
    {
        case 2: logWrite(  "Image 0 is being loaded"); return 0;
        case 3: logWrite(  "Image 1 is being loaded after a revert in app image happens"); return 0;
        case 4: logWrite(  "Image 1 is being loaded"); return 0;
        case 5: logWrite(  "Image 0 is being loaded after a revert in app image happens"); return 0;
        default: logWrite(  "unknown state %d", msm_cs);
    }

    return 1;
}

int32_t one_max10_fpga32::view_ram(uint8_t *myram, uint16_t len, uint32_t saddr, uint16_t bytes_in_row)
{
    int32_t i, mod;
    for (i=0; i<len; i++)
    {
        mod = i % bytes_in_row;
        if (mod == 0x0)
        {
            logWrite( "0x%04x : ", i+saddr);

        }
        logWrite( " 0x%02x",myram[i])  ;

        if (mod == (bytes_in_row-1))
        {
            logWrite( "\n");

        }
    }
    if (mod != (bytes_in_row-1))
    {
      logWrite( "\n");

    }
    return 0;
}

int32_t one_max10_fpga32::view_ram(uint16_t *myram, uint16_t len, uint32_t saddr, uint16_t words_in_row)
{
    int32_t i, mod;
    for (i=0; i<len; i++)
    {
        mod = i % words_in_row;
        if (mod == 0x0)
        {
            logWrite( "0x%06x : ", i+saddr);

        }
        logWrite( " 0x%04x", myram[i])  ;

        if (mod == (words_in_row-1))
        {
            logWrite( "\n");

        }
    }
    if (mod != (words_in_row-1))
    {
      logWrite( "\n");

    }
    return 0;
}

int32_t one_max10_fpga32::view_ram(uint32_t *myram, uint16_t len, uint32_t saddr, uint16_t dwords_in_row)
{
    int32_t i, mod;
    for (i=0; i<len; i++)
    {
        mod = i % dwords_in_row;
        if (mod == 0x0)
        {
            logWrite( "0x%06x : ", i+saddr);

        }
        logWrite( " 0x%08x", myram[i])  ;

        if (mod == (dwords_in_row-1))
        {
            logWrite( "\n");

        }
    }
    if (mod != (dwords_in_row-1))
    {
      logWrite( "\n");

    }
    return 0;
}

int32_t one_max10_fpga32::view_ram24(uint32_t *myram, uint16_t len, uint32_t saddr, uint16_t dwords_in_row)
{
    int32_t i, mod;
    for (i=0; i<len; i++)
    {
        mod = i % dwords_in_row;
        if (mod == 0x0)
        {
            logWrite( "0x%06x : ", i+saddr);

        }
        logWrite( " 0x%06x", myram[i] & 0xFFFFFF)  ;

        if (mod == (dwords_in_row-1))
        {
            logWrite( "\n");

        }
    }
    if (mod != (dwords_in_row-1))
    {
      logWrite( "\n");

    }
    return 0;
}

int32_t one_max10_fpga32::diff_ram(uint8_t *myram, uint8_t *myref, uint16_t len)
{
    int32_t err, i;
    err = 0;
    for (i=0; i<len; i++)
    {
        if (myram[i] != myref[i]) err++;
    }
    return err;
}

int32_t one_max10_fpga32::cp_ram(uint8_t *myram, uint8_t *mydest, uint16_t len)
{
    int32_t i;
    for (i=0; i<len; i++)
    {
        mydest[i] = myram[i];
    }
    return 0;
}

// calculate the next pseudorandom number from the present for number of bits 3..32
// the output bits >= nbits are 0
// if the calculated output is 0, 1 will be return to prevent stucking there
uint32_t one_max10_fpga32::psrg_next(uint8_t nbits, uint32_t present)
{
    uint32_t bit0;
    uint32_t bmask;

    if (nbits < 3) return 0xFFFFFFFF;
    if (nbits > 32) nbits=32;

    if (nbits==32)
        bmask=0;
    else
        bmask = 1L << nbits;
    bmask--;

    switch (nbits)
    {
        case 32 : bit0 = (present >> 31) ^ (present >> 21) ^ (present >> 1) ^ (present >> 0); break;
        case 31 : bit0 = (present >> 30) ^ (present >> 27)                                  ; break;
        case 30 : bit0 = (present >> 29) ^ (present >>  5) ^ (present >> 3) ^ (present >> 0); break;
        case 29 : bit0 = (present >> 28) ^ (present >> 26)                                  ; break;
        case 28 : bit0 = (present >> 27) ^ (present >> 24)                                  ; break;
        case 27 : bit0 = (present >> 26) ^ (present >>  4) ^ (present >> 1) ^ (present >> 0); break;
        case 26 : bit0 = (present >> 25) ^ (present >>  5) ^ (present >> 1) ^ (present >> 0); break;
        case 25 : bit0 = (present >> 24) ^ (present >> 21)                                  ; break;
        case 24 : bit0 = (present >> 23) ^ (present >> 22) ^ (present >>21) ^ (present >>16); break;
        case 23 : bit0 = (present >> 22) ^ (present >> 17)                                  ; break;
        case 21 : bit0 = (present >> 20) ^ (present >> 18)                                  ; break;
        case 20 : bit0 = (present >> 19) ^ (present >> 16)                                  ; break;
        case 19 : bit0 = (present >> 18) ^ (present >>  5) ^ (present >> 1) ^ (present >> 0); break;
        case 18 : bit0 = (present >> 17) ^ (present >> 10)                                  ; break;
        case 17 : bit0 = (present >> 16) ^ (present >> 13)                                  ; break;
        case 16 : bit0 = (present >> 15) ^ (present >> 14) ^ (present >>12) ^ (present >> 3); break;
        case 14 : bit0 = (present >> 13) ^ (present >>  4) ^ (present >> 2) ^ (present >> 0); break;
        case 13 : bit0 = (present >> 12) ^ (present >>  3) ^ (present >> 2) ^ (present >> 0); break;
        case 12 : bit0 = (present >> 11) ^ (present >>  5) ^ (present >> 3) ^ (present >> 0); break;
        case 11 : bit0 = (present >> 10) ^ (present >>  8)                                  ; break;
        case 10 : bit0 = (present >>  9) ^ (present >>  6)                                  ; break;
        case  9 : bit0 = (present >>  8) ^ (present >>  4)                                  ; break;
        case  8 : bit0 = (present >>  7) ^ (present >>  5) ^ (present >> 4) ^ (present >> 3); break;
        case  5 : bit0 = (present >>  4) ^ (present >>  2)                                  ; break;
        default : // 22, 15, 7, 6, 4, 3
                  bit0 = (present >> (nbits-1)) ^ (present >> (nbits-2)); break;
    }

    bit0 &= 1;
    present <<= 1;
    present |= bit0;
    present &= bmask;

    if (present==0) present = 1; // prevent stuck at 0

    return present;
}

uint8_t one_max10_fpga32::psrg_fifo_rd_test(uint8_t nbits, uint32_t seed, uint32_t nwords)
{
    uint32_t *test_new, *test, i, gen_data, rd_data, err=0, wcnt, patt=3;

    test_new = new uint32_t[nwords];
    test = test_new;
    patt &= 3;


// Bits 1..0: TP (0 - count down, 1 - count up, 2 - walking 1, 3 - psrg)
// Bits 6..2: number of bits in TP, from 4-1 to max Nbits-1 for 4..Nbits
// writing anything here clears the error counter for incoming data and the word counter (both FIFO modes)

    m_uart32->WriteDWord(ADDR_PSRG_CONFIG, ((nbits-1) << 2) | patt); // 32 bits, up counter

// Offset 0 - FIFO output register (FIFO read),  seed point (write once)
//                \ reading from here delivers the data from the pattern generator

    m_uart32->WriteDWord(ADDR_PSRG_FIFO_R, seed); // start word

// now read
    get_sh_mem(nwords, ADDR_PSRG_FIFO_R, test, 0);

    gen_data=seed;
    for (i=0; i<nwords; i++)
    {
        rd_data = *test++;
        if (rd_data != gen_data)
        {
            err++;
            if (err < 20)
                logWrite("offs %4d err %d read 0x%08x expected 0x%08x\n", i, err, *test++, gen_data);
        }
        gen_data=psrg_next(nbits, gen_data);
    }
    wcnt = m_uart32->ReadDWord(ADDR_PSRG_WCNT);
    if (err==0) logWrite("Done. %d or 0x%02x words transferred\n", wcnt, wcnt);
    else
                logWrite("%d or 0x%02x words transferred and %d errors found!\n", wcnt, wcnt, err);
    delete test_new;
    return err;
}


uint8_t one_max10_fpga32::block_code(char *block_name)
{
    if (strcmp(block_name,"ufm0") == 0 )
        return UFM0;
    else
    if (strcmp(block_name,"ufm1") == 0 )
        return UFM1;
    else
    if ( (strcmp(block_name,"cfm0") == 0 ) || (strcmp(block_name,"cfg0") == 0 ) )
        return CFM0;
    else
    if (strcmp(block_name,"cfm1") == 0 )
        return CFM1;
    else
    if (strcmp(block_name,"cfm2") == 0 )
        return CFM2;
    else
    if ( (strcmp(block_name,"cfm2p1") == 0 ) || (strcmp(block_name,"cfg1") == 0 ) )
        return CFM2p1;
    else
    {
        logWrite( "! Unknown block name !\n");

        return 7;
    }
}

int32_t one_max10_fpga32::check_sh_mem(uint16_t msize, uint8_t bitsize, uint32_t baddr, uint8_t ainc, uint8_t set_mem, uint8_t chk_mem, uint32_t seed)
{
    int32_t ret, i, wsize;
    uint32_t last_word, read_word;

    uint8_t   *p_Bytes, *p_Bytes_new;
    uint16_t  *p_Words, *p_Words_new;
    uint32_t  *p_DWords, *p_DWords_new;

    if (bitsize > 32) bitsize=32;
    if (bitsize ==0 ) return 2;

    ret = 0;
    wsize = (bitsize-1) >> 3;

    if (seed==0)
        seed = msize ^ (msize >> 4) ^ (msize >> 8) ^ wsize ^ baddr ^ (baddr >> 8) ^ (baddr >> 16) ^ bitsize;

    switch (wsize)
    {
        case WSIZE_BYTE:
            p_Bytes_new = new uint8_t[msize];
            p_Bytes = p_Bytes_new;
            break;
        case WSIZE_WORD:
            p_Words_new = new uint16_t[msize];
            p_Words = p_Words_new;
            break;
        case WSIZE_3_BT :
        case WSIZE_DWORD:
            p_DWords_new = new uint32_t[msize];
            p_DWords = p_DWords_new;
            break;
    }

    last_word = psrg_next(bitsize, seed);

    if (set_mem)
    {
        for (i=0; i<msize; i++)
        {
            switch (wsize)
            {
                case WSIZE_BYTE:
                    *p_Bytes++ = last_word;
                    break;
                case WSIZE_WORD:
                    *p_Words++ = last_word;
                    break;
                case WSIZE_3_BT:
                case WSIZE_DWORD:
                    *p_DWords++ = last_word;
            }
            last_word = psrg_next(bitsize, last_word);
        }
        switch (wsize)
        {
            case WSIZE_BYTE:
                ret=set_sh_mem(msize, baddr, p_Bytes_new, ainc);
                break;
            case WSIZE_WORD:
                ret=set_sh_mem(msize, baddr, p_Words_new, ainc);
                break;
            case WSIZE_3_BT:
            case WSIZE_DWORD:
                ret=set_sh_mem(msize, baddr, p_DWords_new, ainc);
        }
    }


    ret=0;

    if (chk_mem)
    {
        last_word = psrg_next(bitsize, seed);
        switch (wsize)
        {
            case WSIZE_BYTE:
                get_sh_mem(msize, baddr, p_Bytes_new, ainc);
                p_Bytes = p_Bytes_new;
                break;
            case WSIZE_WORD:
                get_sh_mem(msize, baddr, p_Words_new, ainc);
                p_Words = p_Words_new;
                break;
            case WSIZE_3_BT:
                get_sh_mem24(msize, baddr, p_DWords_new, ainc);
                p_DWords = p_DWords_new;
                break;
            case WSIZE_DWORD:
                get_sh_mem(msize, baddr, p_DWords_new, ainc);
                p_DWords = p_DWords_new;
        }
        for (i=0; i<msize; i++)
        {
            switch (wsize)
            {
                case WSIZE_BYTE:
                    read_word = *p_Bytes++;
                    break;
                case WSIZE_WORD:
                    read_word = *p_Words++;
                    break;
                case WSIZE_3_BT:
                case WSIZE_DWORD:
                    read_word = *p_DWords++;
            }
            if (read_word != last_word)
            {
                if (ret <= 20)
                    logWrite( "Offset 0x%04x: wrote 0x%02x, read 0x%02x\n", i, last_word, read_word);
                else
                    logWrite( ".");

                ret++;
            }
            last_word = psrg_next(bitsize, last_word);
       }
       if (ret > 20)
          logWrite( "\n");

       if (ret > 0)
           logWrite( "Number of errors %d\n", ret);
       else
           logWrite( "Done.\n");

    }
    return ret;
}

int32_t one_max10_fpga32::get_sh_mem(uint32_t msize, uint32_t baddr, uint8_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->RecvBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::get_sh_mem(uint32_t msize, uint32_t baddr, uint16_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->RecvBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::get_sh_mem24(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->RecvBurst24(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::get_sh_mem(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->RecvBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}


int32_t one_max10_fpga32::set_sh_mem(uint32_t msize, uint32_t baddr, uint8_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->SendBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::set_sh_mem(uint32_t msize, uint32_t baddr, uint16_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->SendBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::set_sh_mem24(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->SendBurst24(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

int32_t one_max10_fpga32::set_sh_mem(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->SendBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

uint32_t one_max10_fpga32::config_status()
{
    int i;
    uint32_t rd_reg[8]; // the dual config core has 8 addresses, we will read not all of them, but will reserve all 8

    m_uart32->WriteByte(AV_RCU_BASE+2, 0x0F); // trigger the reading
    // read data max 17-bit , reading 0..2 is not ok, they are write only!
    m_uart32->RecvBurst24(5, 1, 1, AV_RCU_BASE+3, rd_reg+3);
    if (debug)
        for (i=3; i<8; i++)
            logWrite(  "# Status regs at offset %d: 0x%08x\n", i, rd_reg[i] );

    logWrite(  "# ");

    print_msm_cs((rd_reg[6] >> 4) & 0xF);
    logWrite(  " (oldest)\n#\t => ");

    print_msm_cs((rd_reg[5] >> 4) & 0xF);
    logWrite(  " (previous)\n#\t\t => ");

    print_msm_cs((rd_reg[4] >> 13) & 0xF);
    logWrite(  " (present)\n");

    logWrite(  "# config_sel_overwrite is %d, config_sel(intern) is %d\n", rd_reg[7] & 1, (rd_reg[7] >> 1) & 1);

    return 0;
}

uint32_t one_max10_fpga32::config_source()
{
   // int i;
    uint32_t rd_reg;

    m_uart32->WriteByte(AV_RCU_BASE+2, 0x0F);  // trigger the reading
    rd_reg = m_uart32->Read3Bytes(AV_RCU_BASE+4);
    rd_reg >>= 13;
    rd_reg &= 0xF;
    //print_msm_cs(, rd_reg);
    return ( (rd_reg - 1) & 0x2) >> 1;
}



    // for any flash block or part of a block
uint32_t one_max10_fpga32::read_flash_block(uint32_t saddr, int32_t nbytes, uint32_t *rdata)
{
    uint32_t my_buf32[AV_MEM_BSIZE];
    uint32_t block_size, i, empty;
    int32_t rbytes, barstep, barnext;
    // 1) program the start address
    // 2) read up to 128 times from the data register
    // 3) read again up to 128 times...
//    av_reset();
//    set_addr_cnt(saddr, 1);
//  if (debug)
//  {
//      logWrite(  "Begin :");
//
//      print_addr_cnt();
//  }
    nbytes &= 0xFFFFFFFC; // the two LSB should be 0
    rbytes = 0;
    empty = 0xFFFFFFFF;
    saddr >>= 2;
    logWrite( "# Read block of 0x%02x bytes from address 0x%06x\n", nbytes, saddr);

    logWrite(  "Progress __________________________________________________\n");

    logWrite(  "         ");

    barstep = nbytes/50;
    barnext = barstep;
    while (nbytes > 0)
    {
        if (nbytes > AV_MEM_BSIZE) block_size = AV_MEM_BSIZE; else block_size = nbytes;
        m_uart32->RecvBurst(block_size, 1, 1, saddr, my_buf32);
        for (i = 0; i < block_size ; i++)
        {
            *rdata = my_buf32[i];
            empty &= *rdata;
            rdata++;
        }

        nbytes -= 4*block_size;
        rbytes += 4*block_size;
        saddr  += block_size;

        if ( rbytes > barnext )
        {
            logWrite(  "|");

            barnext += barstep;
        }
    }

    logWrite(  " DONE!\n");

    if (debug)
    {
        logWrite(  "End   :");

    }
    if (empty == 0xFFFFFFFF)
    {
        if (debug)
        {
            logWrite(  "# The memory is empty (all bytes are 0xFF)!\n\n");

        }
        rbytes |= (1 << 31);
    }
    else
        if (debug)
        {
            logWrite(  "# The memory is not empty!\n\n");

        }
    return rbytes;
}

uint32_t one_max10_fpga32::read_flash_block(uint8_t sector, uint32_t *rdata)
{
    return read_flash_block(flash_start_address(sector), flash_size(sector), rdata);
}

    // trigger the reconfiguration from internal flash, image=0|1 - from image 0 or 1, any other number ->
    // using the signal at the CONFIG_SEL pin
uint32_t one_max10_fpga32::trigger_reconf(uint8_t image)
{
    if (image < 2)
    {
        logWrite(  "# Trigger reconfiguration from image %d\n", image);


        // set the config_sel_overwrite flag (bit 0) and set the desired image # in bit 1
        m_uart32->WriteByte(AV_RCU_CONF_SEL, (image << 1) | 1);
//      logWrite(  "# Busy is %d\n", m_uart32->ReadDWord(AV_RCU_STATUS | AUTOINC_ADDR, false) );
//      logWrite(  "# Status regs at offset 7: 0x%08x\n", m_uart32->ReadDWord((AV_RCU_BASE+4*7) | AUTOINC_ADDR, false) );
    }
    else
    {
        // clear the config_sel_overwrite flag (bit 0)
        m_uart32->WriteByte(AV_RCU_CONF_SEL, 0);
        image = 0xFF;
        logWrite(  "# Trigger reconfiguration according to the level on the CONFIG_SEL pin\n");

    }
    m_uart32->WriteByte(AV_RCU_TRIGGER, 1);
    return image;
}

// Sector = 1-UFM1, 2-UFM0, 3-CFM2, 4-CFM1, 5-CFM0
// if nbytes = 0 just erase and stop
// sector: byte 0 is sector id, byte 1 is start page, byte 2 is number of pages, 0 for all
uint32_t one_max10_fpga32::write_flash_block(uint32_t sector, int32_t nbytes, uint32_t *wdata)
{
    uint32_t wp, tout, ret_code, saddr;
    int32_t barstep, barnext, rbytes;
    int32_t page_beg, page_len, p, ldebug=0, timeout, block_flash_wr;

// xFM erase sector
// 1) disable the write protection mode
// 2) write to control register to select sector erase location, check before if busy is 00b
// 3) busy = 01b
// 4) waitrequest?
// 5-6) check erase successfull field in the status register 1-ok, 0-failed, max erase time is 350 ms
// 7) repeat 2..5 if more sectors have to be erased
// 8) enable back the write protection mode
    ret_code = 0;

    page_beg = (sector >>  8) & 0xFF;
    page_len = (sector >> 16) & 0xFF;
    sector &= 0x7;

    if ((sector < 1) || (sector > 5))
    {
        logWrite( "Invalid sector %d, exiting!\n", sector);

        return 1;
    }
    wp = 0x1F;
    wp ^= 1 << (sector -1);
    if (ldebug)
    {
        logWrite( "# Page beg is 0x%02x, Page length is 0x%02x\n", page_beg, page_len);
        logWrite( "# Sector is %d, wp bits are 0x%02x\n", sector, wp);

        get_av_status();
        print_av_status();
    }
    logWrite( "# Disable the write protection...\n");

    set_av_config( (     7 << 20) | 0xFFFFF | (wp << 23) );
    if (ldebug)
    {
        logWrite( "# Read back the AV config register 1...\n");

        get_av_config();
        print_av_config();
        get_av_status();
        print_av_status();
    }
    logWrite( "# ... and start erasing...\n\n");

    p=0;
    do
    {
        if (page_len) // page by page
            set_av_config( (7      << 20) | flash_start_address(sector | ((page_beg+p) << 8) ) | (wp << 23) );
        else
            set_av_config( (sector << 20) | 0xFFFFF | (wp << 23) );
        if (ldebug)
        {
            logWrite( "# Read back the AV config register 2...\n");

            get_av_config();
            print_av_config();
            get_av_status();
            print_av_status();
            logWrite( "\n");
        }
        timeout=100;
        do
        {
            timeout--;
            get_av_status();
            if (ldebug)
                print_av_status();
        }
        while ( ((last_av_sta & 0x3) > 0) && (timeout > 0));
        if ((last_av_sta & 0x13) == 0x10)
        {
            if (page_len)
                logWrite( "# Erase successfull of page %d of %d!\n", p, page_len);
            else
                logWrite( "# Erase successfull!\n");

        }
        else
        {
            logWrite( "# Erase failed!\n");

            ret_code = 2;
        }
        if (ldebug)
        {
            logWrite( "# Read back the AV config register 3...\n");

            get_av_config();
            print_av_config();
        }
        p++;
    } while (p < page_len);

    nbytes &= 0xFFFFFFFC; // should be multiple of 4
    rbytes = nbytes;

    if (nbytes > 0) // if only erase, nbytes is 0!
    {
        saddr = flash_start_address(sector);

        logWrite(  "# Start writing to the FPGA of size %d, flash sector %d, %d bytes at 0x%06x ...\n", fpga_size, sector, rbytes, saddr);

        saddr >>= 2; // convert to word addresses

        barstep = rbytes/50;
        barnext = rbytes-barstep;

        logWrite(  "Progress __________________________________________________\n");

        logWrite(  "         ");

        while (rbytes > 3) // actually > 0 is ok
        {
            if (rbytes < (4*FLASH_WR_AT_ONCE) )
                block_flash_wr = 1;
            else
                block_flash_wr = FLASH_WR_AT_ONCE;
            // send many words at once, check only after the last if successful. Recommended from Altera to check after each word.
            // If somethin goes wrong, it will be seen even if we don't check after each word.
            m_uart32->SendBurst(block_flash_wr, 1, 1, saddr, wdata);
            //usleep(300); // necessary when the UART in FPGA has no RX FIFO!
            rbytes -= 4*block_flash_wr;
            wdata += block_flash_wr;
            saddr += block_flash_wr;
            // check if successfull, with 115200 bits/s not necessary, write cycle about 90 us
            tout = 10;
            do
            {
                get_av_status();
                if ((last_av_sta & 0x0B) == 0x00)
                {
                    print_av_status();
                    logWrite(  "\nWriting to flash failed! Exitting!\n");

                    return 3;
                }
                tout--;
                if (tout == 0)
                {
                    print_av_status();
                    logWrite(  "\nTimeout waiting for writing to be ready! Exitting!\n");

                    return 4;
                }
            }
            while ((last_av_sta & 0x0B) != 0x08);
            if ( rbytes < barnext )
            {
                logWrite(  "|");

                barnext -= barstep;
            }
        }
        logWrite(  " DONE!\n");

        if (ldebug)
        {
            get_av_status();
            print_av_status();
        }
    }

    logWrite( "# Re-enable write protection ...\n");

    wp = 0x1F;
    set_av_config( (7 << 20) | 0xFFFFF | (wp << 23) );
    if (ldebug)
    {
        logWrite( "# Read back the AV config register 4...\n");

        get_av_config();
        print_av_config();
    }
    return ret_code;
}


// command for configuration, can be ORed, here the only possible combinations:
//      erase, program, verify    xFM_ERASE | xFM_PROGRAM | xFM_VERIFY
//                        same                xFM_PROGRAM | xFM_VERIFY
//      erase and check if blank: xFM_ERASE | xFM_VERIFY
//      only erase:               xFM_ERASE
//      only verify:              xFM_VERIFY

uint32_t one_max10_fpga32::epv_config(char *filename, uint8_t operation, uint8_t x_flash_code)
// Note: CFM2 and CFM1 are automatically combined into one block if CFM2p1 is specified!
{
    uint8_t ret_code, blockw[2], blockr[2]; //, empty[2];
    int32_t nbytes, nbytes_non_FF, lenw[2], lenr[2], lenr_clipped;
    uint32_t *prog_data;
    int errc, i;
    FILE *f;                         // was /4!
    uint32_t my_bufw[AVBUS_CFM0_SIZE_40/2]; // the max buffer size, can be made with new and depending on the actual size of the FPGA!
    uint32_t my_bufr[AVBUS_CFM0_SIZE_40/2]; // the max buffer size, can be made with new and depending on the actual size of the FPGA!

    ret_code = 0;
    nbytes_non_FF = 0;

    // check for invalid flash block code
    if ((x_flash_code > CFM2p1) || (x_flash_code < 1) )  // invalid code
    {
        return 0xF0;
    }

    if (operation & xFM_PROGRAM) operation &= ~(xFM_ERASE); // clear the erase bit, erase will be made anyway before writing!

    if ( ( (operation & xFM_PROGRAM) || (operation & xFM_VERIFY) ) && ( (operation & xFM_ERASE) == 0) && (strlen(filename) > 0) ) // read the file
    {

        f = fopen(filename,"r");
        if(f == NULL)
        {
                logWrite( "Error opening file %s\n", filename);

                return 1;
        }
        if (strstr(filename,".rpd") == NULL )
        {
            logWrite( "# Reading from %s file in text-hex format, one byte per line.", filename);
            nbytes = read_conf_file8(f, my_bufw, flash_size(x_flash_code)); // read a text file with hex data, 1 byte per line
        }
        else
        {
            logWrite( "# Reading from %s file in rpd-format.", filename);

            nbytes = read_rpd_file(f, my_bufw, flash_size(x_flash_code)); // read .rpd file
        }
        if (nbytes == 0)
            return (operation << 4);
        nbytes_non_FF = 4*clip_conf_data(my_bufw, nbytes / 4);
        logWrite( " The flash block\n# size is \t%7u or 0x%06x.\n",
                    flash_size(x_flash_code), flash_size(x_flash_code));

        logWrite( "# Bytes read \t%7u or 0x%06x, after clipping 0xFF..FF at the end \n# the size is \t%7u or 0x%06x.\n",
                    nbytes, nbytes, nbytes_non_FF, nbytes_non_FF);

        if (x_flash_code==CFM2p1)
        {
            if (nbytes_non_FF < flash_size(CFM2))
                logWrite( "# CFM2 size \t%7u or 0x%06x is enough to store the configuration.\n",
                    flash_size(CFM2), flash_size(CFM2));
            else
                logWrite( "# CFM2 size \t%7u or 0x%06x is NOT enough to store the configuration, using both CFM2 and CFM1.\n",
                    flash_size(CFM2), flash_size(CFM2));

        }
    }

    if (x_flash_code==CFM2p1)
    {
        blockw[0] = CFM2;
        blockw[1] = CFM1;
        blockr[0] = CFM2;
        blockr[1] = CFM1;
        if (nbytes_non_FF < flash_size(CFM2))
        {
            lenw[0] = nbytes_non_FF;
            lenw[1] = 0;
        }
        else
        {
            lenw[0] = flash_size(CFM2);
            lenw[1] = nbytes_non_FF - flash_size(CFM2);
            if (lenw[1] > flash_size(CFM1) )
            {
                logWrite( "The data length in CFM1 %d is more than the block size %d!\n", lenw[1], flash_size(CFM1) );

                return (operation << 4);
            }
        }
    }
    else
    {
        blockw[0] = x_flash_code;
        blockw[1] = 0;
        blockr[0] = x_flash_code;
        blockr[1] = 0;
        lenw[0] = nbytes_non_FF;
        lenw[1] = 0;
    }

    if (operation & xFM_ERASE) // only erase without writing
    {
        for (i=0; i<2; i++)
            if (blockw[i] > 0)
            {
                errc = write_flash_block(blockw[i], 0, my_bufw);
                if (errc > 0)
                    return (xFM_ERASE << 4); // return the code of the failed operation shifted left
            }
            ret_code |= xFM_ERASE;       // put the successfull operation code into the return code
    }

    if (operation & xFM_PROGRAM)
    {
        prog_data = my_bufw;
        for (i=0; i<2; i++)
            if (blockw[i] > 0)
            {
                errc = write_flash_block(blockw[i], lenw[i], prog_data);
                if (errc > 0)
                    return (xFM_ERASE << 4); // return the code of the failed operation shifted left
                prog_data += lenw[i]/4; // pointer to the rest of the data for CFM1
            }
        ret_code |= xFM_ERASE;
        ret_code |= xFM_PROGRAM;
    }

    if (operation & xFM_VERIFY)
    {
        logWrite( "# Reading flash memory...\n");

        prog_data = my_bufr;
        for (i=0; i<2; i++)
            if (blockr[i] > 0)
            {
                lenr[i] = read_flash_block(blockr[i], prog_data);
                //empty[i] = (lenr[i] >> 31) & 1;
                lenr[i] &= 0x7FFFFFFF;
                prog_data += lenr[i]/4;
            }
            else
                lenr[i] = 0;
        lenr_clipped = 4*clip_conf_data(my_bufr, (lenr[0] + lenr[1]) / 4);
        if ( (operation & xFM_ERASE) || (nbytes_non_FF == 0) )
        {
            if (lenr_clipped == 0)
            {
                logWrite(  "# Memory verified to be empty!\n");

                ret_code |= xFM_VERIFY;
                return ret_code;
            }
            else
            {
                logWrite(  "# Memory not empty!\n");

                ret_code |= (xFM_VERIFY << 4);
                return ret_code;
            }
        }
        if (nbytes_non_FF != lenr_clipped)
        {
            logWrite(  "# Non-empty part of the written (%d) and read (%d) data have different lengths!\n", nbytes_non_FF, lenr_clipped);

            ret_code |= (xFM_VERIFY << 4);
//            return ret_code;
        }
        errc = comp_cfg_data(nbytes_non_FF, my_bufw, my_bufr);
        if (errc > 0)
        {
            logWrite(  "# Memory compare FAILED with %d differences!\n", errc);

            ret_code |= (xFM_VERIFY << 4);
            return ret_code;
        }
        logWrite(  "# Memory compare OK!\n");

        ret_code |= xFM_VERIFY;
    }
    return ret_code;
}
