// $Id: max10_fpga32.cpp 981 2023-07-18 10:02:45Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions, for close() */
#include <fcntl.h>   /* File control definitions, for open() */
//#include <termios.h> /* POSIX terminal control definitions */
#include <signal.h>  /* ANSI C signal handling */
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <ctime>

#include <sys/time.h>

#include "uart32.h"
#include "one_max10_fpga32.h"
#ifdef _ZYNQ
//#ifdef _IF38
//#include "../PROJECTS/IF38A_HV_Curr_Rel/C/CDIOs.h"
//#endif
#endif

#ifdef _WIN32
    #define DEVICE_PREFIX "//./COM"
    #define DEVICE_PORT   5
    #define STARTING_PORT 1
        #define sleep Sleep
#else
    #ifdef __CYGWIN__
        #define DEVICE_PREFIX "/dev/ttyS"
        #define DEVICE_PORT   4
        #define STARTING_PORT 0
    #else
                #ifdef _ZYNQ
                        #define DEVICE_PREFIX ""
                        #define DEVICE_PORT   0
                        #define STARTING_PORT 0
                        #define DEF_BRATE 4000000       // default bit rate
                #else
                        #define DEVICE_PREFIX "/dev/triggerDev"
//                        #define DEVICE_PORT   1
                        #define STARTING_PORT 0
                #endif

    #endif
#endif


#define MAX_CFG_DATA AVBUS_CFM0_SIZE_40  // this is the max size of the FPGA supported by the classes!
//#define ID_DEF  1

int32_t read_hex_dec(char *c, uint32_t *res);

void print_usage(uint8_t full)
{
    printf("* General utility program for all MAX10-FPGA designs with 24-bit address & 32-bit data UART interface\n");
    printf("max10_fpga32 [options]\n");
    printf("\n*** Serial interface ***\n");
    printf("\t--usage : print the short help to all command line options\n");
    printf("\t--dev <device_id>, like /dev/ttyUSB0 (linux) or /dev/ttyS0 (cygwin) or //./COM5 (windows), default %s%d\n", DEVICE_PREFIX, DEVICE_PORT);
    printf("\t--port <%d..> : alternative to --dev, just specify the number of the port, default %d\n", STARTING_PORT, DEVICE_PORT);
    printf("\t--br <1200|2400|4800...115200|...1000000|...|4000000>, default %d\n", DEF_BRATE);
    printf("\t--id <8-bit id_mask>, default %d, 0 or negative - don't use it\n", ID_DEF);
    printf("\n*** Debug serial interface ***\n");
    printf("\t--pingc <pings> : send <pings> times a byte-counter to the UART\n");
    printf("\t--pinga <pings> : send <pings> times 0xAA 0x55 to the UART\n");
    //
    printf("\n*** IO space ***\n");
    printf("\t--cmd <command_byte> : send command, bits 0..3 are CPU reset, CPU off, Soft reset, ADS reset\n");
    printf("\t--gmem   <size> <start_address> : read bytes from IO space and dump on screen\n");
    printf("\t--gmem16 <size> <start_address> : read 16-bit words from IO space and dump on screen\n");
    printf("\t--gmem24 <size> <start_address> : read 24-bit words from IO space and dump on screen\n");
    printf("\t--gmem32 <size> <start_address> : read 32-bit dwords from IO space and dump on screen\n");

    printf("\t--w1 <address> < 8-bit data> : write to IO space at address one byte\n");
    printf("\t--w2 <address> <16-bit data> : write to IO space at address one 16-bit word\n");
    printf("\t--w3 <address> <24-bit data> : write to IO space at address one 24-bit word\n");
    printf("\t--w4 <address> <32-bit data> : write to IO space at address one 32-bit word\n");

    printf("\t--r1 <address> : read from address one byte\n");
    printf("\t--r2 <address> : read from address one 16-bit word\n");
    printf("\t--r3 <address> : read from address one 24-bit word\n");
    printf("\t--r4 <address> : read from address one 32-bit word\n");

    printf("\t--chk_mem <msize> <bitsize> <address> <0|1 (set?)> <0|1 (check?)> : test shared memory with msize bitsize-bit words at address, set? the memory content, check? the expected memory content\n");
    printf("\t--chk_fifo <msize> <bitsize> <seed> : (read) test the link using the built-in test pattern checker\n");
    //
    printf("\n*** FPGA firmware functions ***\n");
    printf("\t--fstatus : read and print the FPGA status\n");
    printf("*** Note: FPGA flash memory consists of ufm0, ufm1 (user flash memory) and cfm0, cfm1, cfm2 (config flash memory).");
    printf(" cfm0 and (cfm1 + cfm2) can hold the FPGA configuration images 0 and 1. Use the abbreviation cfg1 for cfm1+cfm2!");
    printf(" For consistency you can use the alias cfg0 for cfm0.\n");
    printf("\t--epv_cfg <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <input_text/rpd-file> : erase, program & verify\n");
    printf("\t--vrf_cfg <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <input_text/rpd-file> : verify only\n");
    printf("*** Note: the rpd-format comes from Altera and is accepted without conversion by the --epv_cfg and --vrf_cfg options!\n");
    printf("\t--refresh <0|1|2> : refresh the configuration from image 0|1 or using the signal at the CONFIG_SEL pin (>1).\n");
    printf("\t--svn_min <svn_min> : reprogram only when SVN revision in the FPGAs is less than the specified.\n");
    printf("\t--rd_flash <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <output_text-file> : read and store to file the corresponding flash block (32-bit words)\n");
    printf("\t--rd_flash16 <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <output_text-file> : read and store to file the corresponding flash block (16-bit words)\n");
    printf("\t--wr_flash <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <input_text-file>  : read from the file, erase and write to the corresponding flash block (32-bit words)\n");
    printf("\t--wr_flash16 <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> <input_text-file>  : read from the file, erase and write to the corresponding flash block (16-bit words)\n");
    printf("\t--erase    <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> : erase the corresponding flash block\n");
    printf("\t--vb_cfg   <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> : check if empty (erased)\n");
    printf("\t--ev_cfg   <ufm0|ufm1|cfm0|cfm1|cfm2|cfg0|cfg1> : erase and check if empty\n");
    printf("\t--rpd_convert <input_rpd-file[.rpd]> <output_text-file> : convert the .rpd file to a hex text file\n");
}

#define debug_main 0

int main(int argc, char** argv)
{
    uint32_t  cfg_data[   MAX_CFG_DATA / 2];
    uint32_t  cfg_data_rd[MAX_CFG_DATA / 2]; // was 4
    uart32* my_uart32;
    uint16_t idx, svn_min;
    int32_t br, pings, nbytes, i;
    FILE *f_out, *f_inp;
    // filenames
    char out_file[512];
    char inp_file[512];
    // device name
    char dev_id[512];
    uint32_t maddr, mdata, msize, set_mem, chk_mem, bitsize;

    uint8_t block;
    uint8_t *my_ram;
    uint16_t *my_ram16;
    uint32_t *my_ram32;
    uint32_t id_mask = ID_DEF;
    uint32_t awidth = 24;
    uint32_t dwidth = 32;

    one_max10_fpga32 *p_design;

    int full_reset;
    int debug_class = 0;

    setbuf(stdout, 0);
    sprintf(dev_id, "%s", DEVICE_PREFIX); // default
    br = DEF_BRATE;
    svn_min = 0xFFFF;
    full_reset=0;

    printf("Running on ");
    #ifdef _WIN32
        printf("WINDOWS\n");
    #else
        #ifdef __CYGWIN__
            printf("CYGWIN\n");
        #else
            #ifdef _ZYNQ
               printf("ZYNQ\n");
            #else
               printf("LINUX\n");
                        #endif
        #endif
    #endif

    if (argc < 2)
    {
        printf("program path: %s\n",argv[0]);
        print_usage(0);
        return(1);
    }
    else
    for (idx=1; idx<argc; idx++)
    {
        if (debug_main)
            printf("Current cmd line option is %s\n",argv[idx]);
        if (strcmp(argv[idx],"--dev") == 0 )
        {
            strcpy(dev_id, argv[++idx]);
        }
        else
        if (strcmp(argv[idx],"--port") == 0 )
        {
            sprintf(dev_id, "%s%d", DEVICE_PREFIX, atoi(argv[++idx]));
        }
        else
        if (strcmp(argv[idx],"--br") == 0 )
        {
            br = atoi(argv[++idx]);
        }
        // the address bus width should not be changed (and remain 24) !
        else
        if (strcmp(argv[idx],"--aw") == 0 )
        {
            awidth = atoi(argv[++idx]);
        }
        else
        // the data bus width should not be changed (and remain 32) !
        if (strcmp(argv[idx],"--dw") == 0 )
        {
            dwidth = atoi(argv[++idx]);
        }
        else
        if (strcmp(argv[idx],"--id") == 0 )
        {
            read_hex_dec(argv[++idx], &id_mask);
        }
        else
        if (strcmp(argv[idx],"-r") == 0 )
        {
            full_reset=1;
        }
        else
        if (strcmp(argv[idx],"--svn_min") == 0 )
        {
             svn_min = atoi(argv[++idx]);
             printf("# Setting min SVN revision to %d, for all FPGA config functions\n", svn_min);
        }
    }

    my_uart32 = new uart32(dev_id, br);
    my_uart32->set_slave_mask(id_mask);
    my_uart32->set_max_wsize(dwidth);
    my_uart32->set_max_asize(awidth);

    if (full_reset)
    {
        my_uart32->WriteByte(ADDR_CONF_CMD, 0xFF);
        sleep(1);
    }
    p_design = new one_max10_fpga32(debug_class, my_uart32);

    if (debug_main)
        printf("UART %s open at speed %d\n", dev_id, br);

    for (idx=1; idx<argc; idx++)
    {
        if (debug_main)
            printf("Current cmd line option is %s\n",argv[idx]);
        if (strcmp(argv[idx],"--dev") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--port") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--br") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--aw") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--dw") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--id") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"-r") == 0 )
        {
        }
        else
        if (strcmp(argv[idx],"--svn_min") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--usage") == 0 )
        {
            print_usage(1);
        }
        else
        if (strcmp(argv[idx],"--pingc") == 0 )
        {
            pings = atoi(argv[++idx]);
            printf("Sending %d times a counter\n",pings);
            for (i=0; i<pings; i++)
            {
                usleep(1000);
                my_uart32->WriteSingleByte(i & 0xFF);
            }
        }
        else
        if (strcmp(argv[idx],"--pinga") == 0 )
        {
            pings = atoi(argv[++idx]);
            printf("Sending %d times 0xAA 0x55\n",pings);
            for (i=0; i<pings; i++)
            {
                usleep(1000);
                my_uart32->WriteSingleByte(0xAA);
                //usleep(1);
                my_uart32->WriteSingleByte(0x55);
            }
        }
        // simple read/write to the internal configuration space
        else
        // read from a range of addresses
        if (strcmp(argv[idx],"--gmem") == 0 )
        {
            read_hex_dec(argv[++idx], &msize);
            read_hex_dec(argv[++idx], &maddr);
            my_ram = new uint8_t[msize];
            p_design->get_sh_mem(msize, maddr, my_ram);
            p_design->view_ram(my_ram, msize, maddr, 16);
            delete my_ram;
        }
        else
        if (strcmp(argv[idx],"--gmem16") == 0 )
        {
            read_hex_dec(argv[++idx], &msize);
            read_hex_dec(argv[++idx], &maddr);
            my_ram16 = new uint16_t[msize];
            my_uart32->RecvBurst(msize, 1, 1, maddr, my_ram16);
            p_design->view_ram(my_ram16, msize, maddr, 8);
            delete my_ram16;
        }
        else
        if (strcmp(argv[idx],"--gmem24") == 0 )
        {
            read_hex_dec(argv[++idx], &msize);
            read_hex_dec(argv[++idx], &maddr);
            my_ram32 = new uint32_t[msize];
            my_uart32->RecvBurst24(msize, 1, 1, maddr, my_ram32);
            p_design->view_ram(my_ram32, msize, maddr, 8);
            delete my_ram32;
        }
        else
        if (strcmp(argv[idx],"--gmem32") == 0 )
        {
            read_hex_dec(argv[++idx], &msize);
            read_hex_dec(argv[++idx], &maddr);
            my_ram32 = new uint32_t[msize];
            my_uart32->RecvBurst(msize, 1, 1, maddr, my_ram32);
            p_design->view_ram(my_ram32, msize, maddr, 8);
            delete my_ram32;
        }
        else
        // write a byte
        if (strcmp(argv[idx],"--cmd") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            p_design->send_command(maddr);
            p_design->get_command();
        }
        else
        if (strcmp(argv[idx],"--w1") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            read_hex_dec(argv[++idx], &mdata);
            mdata &= 0xFF;
            printf("Write 0x%02x to address 0x%06x\n", mdata, maddr);
            my_uart32->WriteByte(maddr, mdata);
        }
        else
        if (strcmp(argv[idx],"--w2") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            read_hex_dec(argv[++idx], &mdata);
            mdata &= 0xFFFF;
            printf("Write 0x%04x to address 0x%06x\n", mdata, maddr);
            my_uart32->WriteWord(maddr, mdata);
        }
        else
        if (strcmp(argv[idx],"--w3") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            read_hex_dec(argv[++idx], &mdata);
            mdata &= 0xFFFFFF;
            printf("Write 0x%06x to address 0x%06x\n", mdata, maddr);
            my_uart32->Write3Bytes(maddr, mdata);
        }
        else
        if (strcmp(argv[idx],"--w4") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            read_hex_dec(argv[++idx], &mdata);
            mdata &= 0xFFFFFFFF;
            printf("Write 0x%08x to address 0x%06x\n", mdata, maddr);
            my_uart32->WriteDWord(maddr, mdata);
        }
        else
        if (strcmp(argv[idx],"--r1") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            mdata=my_uart32->ReadByte(maddr);
            printf("Read 0x%02x from address 0x%06x\n", mdata, maddr);
        }
        else
        if (strcmp(argv[idx],"--r2") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            mdata=my_uart32->ReadWord(maddr);
            printf("Read 0x%04x from address 0x%06x\n", mdata, maddr);
        }
        else
        if (strcmp(argv[idx],"--r3") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            mdata=my_uart32->Read3Bytes(maddr);
            printf("Read 0x%06x from address 0x%06x\n", mdata, maddr);
        }
        else
        if (strcmp(argv[idx],"--r4") == 0 )
        {
            read_hex_dec(argv[++idx], &maddr);
            mdata=my_uart32->ReadDWord(maddr);
            printf("Read 0x%08x from address 0x%06x\n", mdata, maddr);
        }
        else
        if (strcmp(argv[idx],"--chk_mem") == 0 ) // <size> <bitsize> <addr> <set> <check>
        {
            read_hex_dec(argv[++idx], &msize);
            bitsize = atoi(argv[++idx]);
            read_hex_dec(argv[++idx], &maddr);
            set_mem = atoi(argv[++idx]);
            chk_mem = atoi(argv[++idx]);
            printf("Check memory, size 0x%04x, bitsize %d, address 0x%06x, set %d, check %d\n", msize, bitsize, maddr, set_mem, chk_mem);
            p_design->check_sh_mem(msize, bitsize, maddr, 1, set_mem, chk_mem);
        }
        else
        // FPGA
        if (strcmp(argv[idx],"--chk_fifo") == 0 ) // <size> <bitsize> <pattern>
        {
            read_hex_dec(argv[++idx], &msize);
            bitsize = atoi(argv[++idx]);
            set_mem = atoi(argv[++idx]); // seed
            p_design->psrg_fifo_rd_test(bitsize, set_mem, msize) ;
        }
        else
        if (strcmp(argv[idx],"--fstatus") == 0 )
        {
            p_design->print_fstatus();
//            p_design->config_status();
        }
        else
        // erase one block of the flash memory
        if (strcmp(argv[idx],"--erase") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);
            block = p_design->block_code(argv[++idx]);
            nbytes = p_design->write_flash_block(block, 0, cfg_data_rd);
        }
        else
        // erase, program, verify config memory
        if (strcmp(argv[idx],"--epv_cfg") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);

            strcpy(inp_file, argv[++idx]);
            if (p_design->get_svn_nr() < svn_min)
                nbytes = p_design->epv_config(inp_file, xFM_ERASE | xFM_PROGRAM | xFM_VERIFY, block);
            else
                printf("The svn revision of the design %d is not smaller than the required minimum %d, skipping!\n", p_design->get_svn_nr(), svn_min);
        }
        else
        // erase verify if blank
        if (strcmp(argv[idx],"--ev_cfg") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);
            nbytes = p_design->epv_config(inp_file, xFM_ERASE | xFM_VERIFY, block);
        }
        else
        // blank check only
        if (strcmp(argv[idx],"--vb_cfg") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);
            strcpy(inp_file, "");
            nbytes = p_design->epv_config(inp_file, xFM_VERIFY, block);
        }
        else
        // verify only
        if (strcmp(argv[idx],"--vrf_cfg") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);
            strcpy(inp_file, argv[++idx]);
            nbytes = p_design->epv_config(inp_file, xFM_VERIFY, block);
        }
        else
        // reload the FPGA config from the internal flash (0 or 1) or using the signal at the CONFIG_SEL pin (any other number)
        if (strcmp(argv[idx],"--refresh") == 0 )
        {
            pings = atoi(argv[++idx]);
            p_design->trigger_reconf(pings);
        }
        else
        // convert the .rpd file from quartus to a .hex text file
        if (strcmp(argv[idx],"--rpd_convert") == 0 )
        {
            strcpy(inp_file, argv[++idx]);
            printf("Reading rpd file %s\n", inp_file);
            f_inp = fopen(inp_file,"r");
            // read rpd file format
            nbytes = p_design->read_rpd_file(f_inp, cfg_data_rd, 0);
            fclose(f_inp);
            strcpy(out_file, argv[++idx]);
            printf("Writing text file %s\n", out_file);
            f_out = fopen(out_file,"w");
            // and store one byte per line hex-text file
            p_design->write_conf_file32(f_out, cfg_data_rd, nbytes);
            fclose(f_out);
        }
        else
        if (strcmp(argv[idx],"--rd_flash") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);
            // open the file with the output data
            block = p_design->block_code(argv[++idx]);
            strcpy(out_file, argv[++idx]);
            f_out = fopen(out_file, "w");
            nbytes = p_design->read_flash_block(block, cfg_data_rd);
            if ((nbytes >> 31) & 1)
                fprintf(stdout, "# The flash memory was empty (0xFF...FF)!\n");
            p_design->write_conf_file32(f_out, cfg_data_rd, nbytes & 0x7FFFFFFF);
            fclose(f_out);
        }
        else
        if (strcmp(argv[idx],"--rd_flash16") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);
            // open the file with the output data
            block = p_design->block_code(argv[++idx]);
            strcpy(out_file, argv[++idx]);
            f_out = fopen(out_file, "w");
            nbytes = p_design->read_flash_block(block, cfg_data_rd);
            if ((nbytes >> 31) & 1)
                fprintf(stdout, "# The flash memory was empty (0xFF...FF)!\n");
            p_design->write_conf_file16(f_out, cfg_data_rd, nbytes & 0x7FFFFFFF);
            fclose(f_out);
        }
        else
        // write to the internal flash
        if (strcmp(argv[idx],"--wr_flash") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);
            strcpy(inp_file, argv[++idx]);
            f_inp = fopen(inp_file,"r");
            // read the config
            nbytes = p_design->read_conf_file32(f_inp, cfg_data, 0); // read a 32-bit word per line
            printf("# %d bytes read as 32-bit words, write to flash block %d\n", nbytes, block);
            fclose(f_inp);
            nbytes = p_design->write_flash_block(block, nbytes, cfg_data);
        }
        else
        if (strcmp(argv[idx],"--wr_flash16") == 0 )
        {
            p_design->send_command(1 << BIT_CONF_CMD_CPU_RST);
            p_design->send_command(1 << BIT_CONF_CMD_CPU_OFF);

            block = p_design->block_code(argv[++idx]);
            strcpy(inp_file, argv[++idx]);
            f_inp = fopen(inp_file,"r");
            // read the config
            nbytes = p_design->read_conf_file16(f_inp, cfg_data, 0); // read a 16-bit word per line
            printf("# %d bytes read as 16-bit words, write to flash block %d\n", nbytes, block);
            fclose(f_inp);
            nbytes = p_design->write_flash_block(block, nbytes, cfg_data);
        }
        else
        {
            printf("Unknown command line option %s\n",argv[idx]);
            print_usage(1);
            return(1);
        }
    }

    delete p_design;

    if (debug_main)
        printf("Bytes sent/received %d / %d\n", my_uart32->BytesSent(), my_uart32->BytesReceived());

    delete my_uart32;
}

int32_t read_hex_dec(char *c, uint32_t *res)
{
    int32_t e,d;
    uint32_t w;
    e = -1;
    if(strlen(c) > 2)
    {
        if (strncmp(c, "0x",2) == 0) // hex
        {
                e = sscanf(c, "0x%x", &w);
                *res = w;
        } else { // dec
                e = sscanf(c, "%d", &d);
                *res = d;
        }
    }
    else
    { // short dec
        e = sscanf(c, "%d", &d);
        *res = d;
    }

    if (e == 1) return 0;
    else return -1;
}

