// $Id: i2c.cpp 1137 2025-01-27 18:20:49Z angelov $:
//#include <unistd.h>  /* UNIX standard function definitions, usleep */
#include "lib/uart32.h"
#include "i2c.h"

C_i2c::C_i2c(uart32* p_uart32, uint32_t new_base_addr)
{
    m_uart32 = p_uart32;
    base_addr = new_base_addr;
}

C_i2c::~C_i2c()
{
}

    // copy a bit slice from src[pos_high-pos_low..0] to dst[pos_high..pos_low]
uint16_t C_i2c::cp_bit_slice(uint16_t src, uint16_t dst, uint8_t pos_high, uint8_t pos_low)
{
    uint32_t msk, neg;

    msk = 1L << (pos_high+1);
    msk--;
    neg = 1L <<  pos_low    ;
    neg--;

    msk ^= neg;  // selected bits
    neg = ~msk;  // the not selected bits

    return ( ( (src << pos_low) & msk) | (dst & neg) ) & 0xFFFF;
}

    // Functions operating with the hardware, begin with hw_!

    // wait until the status AND-ed with the flag_mask is 0
    // mask = 0xFF << 16 can be used to wait until the state machines are idle
int C_i2c::i2c_wt_rdy(uint16_t timeout)
{
    if (timeout == 0) timeout = I2C_STD_TIMEOUT;
    return i2c_wt_rdy(0xFFL << BIT_I2C_STA_SMS, timeout);
}

int C_i2c::i2c_wt_rdy(uint32_t flag_mask, uint16_t timeout)
{
    do
    {
       last_status = m_uart32->ReadDWord(OFFS_I2C_STA+base_addr);
       timeout--;
    } while ( (last_status & flag_mask) && (timeout > 0) );
    // when timeout, return an error code
    if (timeout == 0) return 2;
    // no timeout, return 0
    return (last_status >> BIT_I2C_STA_TOUTL) & 1;
}

uint32_t C_i2c::i2c_last_sent()
{
    last_sent = m_uart32->ReadDWord(OFFS_I2C_DATA+base_addr);
    return last_sent;
}

int C_i2c::i2c_sm_cmd(uint32_t cmd_word)
{
    return m_uart32->WriteAuto(OFFS_I2C_CMD+base_addr, cmd_word);
}


int C_i2c::i2c_sm_reset()
{
    return i2c_sm_cmd(1L << BIT_I2C_RESET_SM);
}

uint32_t C_i2c::i2c_wr(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nbytes, uint8_t wait_for_rdy)
{
    uint32_t w[2];
    int i;

    nbytes--;
    nbytes &= 3;
    slave_addr &= 0x7F;

    w[0] = 0;
    send_bytes += nbytes;   // begin with the MS byte, as it will be sent as last
    for (i=0; i<=nbytes; i++)
    {
        w[0] <<= 8;
        w[0] |= *send_bytes--;
    }

    w[1] = slave_addr << 1 | 0; // complete slave address & r/w bit
    w[1] |= (uint32_t) CMD_I2C_NULL << BIT_I2C_SPEC_CMD;
    w[1] |= (uint32_t) nbytes       << BIT_I2C_LENGTH;
    if (DEBUG_I2C_WR)
        logWrite( "I2c write to slave 0x%02x %d bytes, w0=0x%08x, w1=0x%08x\n", slave_addr, nbytes+1, w[0], w[1]);

    m_uart32->SendBurst(2, 1, 1, OFFS_I2C_DATA+base_addr, w );

    if (wait_for_rdy)
    {
        i = i2c_wt_rdy();
        if (DEBUG_I2C_WR)
            logWrite( "I2c final status after write 0x%08x\n", last_status);
        return i;
    }
    return 0;
}


uint32_t C_i2c::i2c_rd(uint8_t slave_addr, uint8_t *recv_bytes, uint8_t nbytes)
{
    uint32_t err;
    // start the transaction
    i2c_rd(slave_addr, nbytes);
    // wait until the i2c master is ready
    err = i2c_wt_rdy();
    // get the result
    i2c_rd(recv_bytes, nbytes);
    return err;
}

uint32_t C_i2c::i2c_rd(uint8_t slave_addr, uint8_t nbytes)
{
    uint32_t w;

    nbytes--;
    nbytes &= 3;
    slave_addr &= 0x7F;

    w = slave_addr << 1 | 1; // complete slave address & r/w bit
    w |= (uint32_t) CMD_I2C_NULL << BIT_I2C_SPEC_CMD;
    w |= (uint32_t) nbytes       << BIT_I2C_LENGTH;
    if (DEBUG_I2C_RD)
        logWrite( "I2c read from slave 0x%02x %d bytes, w1=0x%08x\n", slave_addr, nbytes+1, w);

    m_uart32->WriteDWord(OFFS_I2C_CMD+base_addr, w );
    return 0;
}

uint32_t C_i2c::i2c_rd(uint8_t *recv_bytes, uint8_t nbytes)
{
    uint32_t w;
    int i;

    nbytes--;
    nbytes &= 3;

    w = m_uart32->ReadDWord(OFFS_I2C_DATA+base_addr);
    if (DEBUG_I2C_RD)
        logWrite( "I2c final status of port 0x%x after read 0x%08x, data 0x%08x\n", last_status, w);

    for (i=0; i<=nbytes; i++)
    {
        *recv_bytes = w & 0xFF;
        w >>= 8;
        recv_bytes++;
    }

    return 0;
}


uint32_t C_i2c::i2c_wr_rd(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nsbytes, uint8_t *recv_bytes, uint8_t nrbytes)
{
    uint32_t err;
    // start the transaction
    i2c_wr_rd(slave_addr, send_bytes, nsbytes, nrbytes);
    // wait until the i2c master is ready
    err = i2c_wt_rdy();
    // get the result
    i2c_rd(recv_bytes, nrbytes);
    return err;
}

uint32_t C_i2c::i2c_wr_rd(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nsbytes, uint8_t nrbytes)
{
    uint32_t w[2];
    int i;

    nsbytes--;
    nrbytes--;
    nsbytes &= 3;
    nrbytes &= 3;
    slave_addr &= 0x7F;

    w[0] = 0;
    send_bytes += nsbytes;   // begin with the MS byte, as it will be sent as last
    for (i=0; i<=nsbytes; i++)
    {
        w[0] <<= 8;
        w[0] |= *send_bytes--;
    }

    w[1] = slave_addr << 1 | 0; // complete slave address & r/w bit
    w[1] |= (uint32_t) CMD_I2C_RSTRT << BIT_I2C_SPEC_CMD;
    w[1] |= (uint32_t) nsbytes       << BIT_I2C_LENGTH;
    w[1] |= (uint32_t) nrbytes       << BIT_I2C_LENGTH2;
    if (DEBUG_I2C_WR)
        logWrite( "I2c write & read to slave 0x%02x %d bytes, w0=0x%08x, w1=0x%08x\n", slave_addr, nsbytes+1, w[0], w[1]);

    m_uart32->SendBurst(2, 1, 1, OFFS_I2C_DATA+base_addr, w );

    return 0;
}


uint32_t C_i2c::mcp4728_wr(uint8_t dac_ch, uint8_t eep, uint8_t vref, uint8_t pd, uint8_t gain, uint16_t dac, uint8_t wait_for_rdy)
{
    uint8_t i2c_dat[4];

    eep     &= 1;
    vref    &= 1;
    dac_ch  &= 3;
    pd      &= 3;
    gain    &= 1;
    dac     &= MCP4728_DAC_MSK;

    if (eep)
        i2c_dat[0] = MCP4728_CMD_WR_DAC_EEP(dac_ch);
    else
        i2c_dat[0] = MCP4728_CMD_WR_DAC(dac_ch);

    i2c_dat[1] = MCP4728_FIRST_BYTE(vref, pd, gain, dac);

    i2c_dat[2] = MCP4728_SECOND_BYTE(dac);

    return i2c_wr(MCP4728_SLV_ADDR, i2c_dat, 3, wait_for_rdy);
}

uint32_t C_i2c::mcp4728_wr(uint8_t dac_ch, uint8_t eep, uint16_t dac, uint8_t ldebug)
{
    dac_ch &= 3;
    eep &= 1;
    dac &= MCP4728_DAC_MSK;

    // short version, with proper VREF, PWR_DOWN, GAIN settings & waiting for the I2C Master ready
    if (ldebug)
    {
        if (eep)
            logWrite("Write to EEPROM & DAC");
        else
            logWrite("Write to DAC");

        logWrite(" channel %d DAC value %d or 0x%03x or %0.3f [V]\n",
                    dac_ch, dac, dac, MCP4728_DAC2V(dac) );
    }

    return mcp4728_wr(dac_ch, eep, MCP4728_VREF_2_048V, MCP4728_PD_BITS_NORMAL, MCP4728_GAIN_1, dac, 1);
}

uint16_t C_i2c::mcp4728_v2dac(float v_dac)
{
    uint16_t d;

    if (v_dac <= 0) return 0;

    d = uint16_t ( (v_dac / MCP4728_VREF)*MCP4728_DAC_STEPS);
    // limit the DAC code to the max
    if (d > MCP4728_DAC_MSK) d=MCP4728_DAC_MSK;

    return d;
}

// DAC = (1.653 + 1.653/4.88563*VIN)
// IN 0          => out  1.653
// IN -4.88563 V => out  0
uint16_t C_i2c::mcp4728_thr2dac_neg(float v_in)
{
    uint16_t d;
    float v_dac;
    // convert the negative threshold to DAC voltage
    v_dac = 1.653 + 1.653/4.88563*v_in;
    // the DAC voltage can not be negative!
    if (v_dac < 0) v_dac = 0;
    // convert to DAC code
    d = mcp4728_v2dac(v_dac);

    return d;
}

uint16_t C_i2c::mcp4728_thr2dac_pos(float v_in)
{
    // resistor divider with serially 33 Ohm and to GND 16 Ohm.

    uint16_t d;
    float v_dac;

    v_dac = 16.0/(16.0 + 33.0)*v_in;
    if (v_dac < 0) v_dac = 0;
    d = mcp4728_v2dac(v_dac);

    return d;
}


uint16_t C_i2c::mcp4728_thr2dac(int dac_ch, float v_in)
{
    dac_ch &= 3;
    if (dac_ch & 1) // odd, the positive channels
        return mcp4728_thr2dac_pos(v_in);
    else            // negative channels
        return mcp4728_thr2dac_neg(v_in);
}

uint16_t C_i2c::ad5624_thr2dac(uint16_t ch, float v_in, uint8_t ldebug)
{
    int16_t d;
    ch &= 3;
    // gain 2, offset -VREF, V is from -VREF to +VREF
    d = int16_t ( (v_in + AD5624_VREF) / (2*AD5624_VREF) * AD5624_DAC_STEPS );
    if (d < 0) d = 0;
    if (d > 0xFFF) d = 0xFFF;
    return d;

}

float C_i2c::ad5624_dac2thr(uint16_t dac_v, uint8_t ldebug)
{
    float volt;
    volt = dac_v*2*AD5624_VREF/AD5624_DAC_STEPS - AD5624_VREF;
    if (ldebug) logWrite("# DAC code 0x%03x => Threshold %+0.3f V\n", dac_v, volt);
    return volt;

}

uint32_t C_i2c::spi_wr(uint16_t ch, uint16_t wdata, uint8_t ldebug)
{
    ch &= 3;
    wdata &= 0xFFF;

    if (ldebug)
        logWrite( "SPI write to AD5624 channel %d, data 0x%03x\n", ch, wdata);

    m_uart32->WriteWord(base_addr + ch, wdata );

    return 0;
}

uint32_t C_i2c::last_dac_rep()
{
    uint32_t dac_dat[4];
    int i;

    m_uart32->RecvBurst(4, 1, 1, base_addr, dac_dat );

    logWrite( "SPI last output data\n");
    for (i=0; i<4; i++)
    {
        logWrite("Last DAC[%d] = ", i);
        if ( (dac_dat[i] >> AD5624_DAC_UNKNOWN) & 1 )
            logWrite("unknown\n");
//        else
        logWrite("0x%03x = %4d => %+0.3f V\n", dac_dat[i] & 0xFFF, dac_dat[i] & 0xFFF, ad5624_dac2thr(dac_dat[i] & 0xFFF, 0));
    }
    return 0;
}

std::array<uint32_t, 4> C_i2c::last_dac_rep_values()
{
    std::array<uint32_t, 4> dac_dat;
    int i;
    m_uart32->RecvBurst(4, 1, 1, base_addr, dac_dat.data() );

    logWrite( "SPI last output data\n");
    for (i=0; i<4; i++)
    {
        logWrite("Last DAC[%d] = ", i);
        if ( (dac_dat[i] >> 12) & 1 )
            logWrite("unknown\n");
        else
        dac_dat[i] == dac_dat[i] & 0xFFF;
        logWrite("0x%03x = %4d\n", dac_dat[i], dac_dat[i]);
    }
    return dac_dat;
}
