// $Id: trigger.cpp 1137 2025-01-27 18:20:49Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions, for close() */
#include <fcntl.h>   /* File control definitions, for open() */
//#include <termios.h> /* POSIX terminal control definitions */
#include <signal.h>  /* ANSI C signal handling */
#include <time.h>
//#include <math.h>
#include <ctype.h>
#include <ctime>

#include <sys/time.h>

#include "lib/uart32.h"
#include "i2c.h"
#include "C_trigger.h"
#include "CDataStreamParser.h"


#define DEVICE_PREFIX "/dev/triggerDev"
#define STARTING_PORT 0
#define DEF_SET_LOW_LATENCY 0
#define DWIDTH 32
#define AWIDTH 24

//#define MCP_DAC_TEST_FUNC   1

int32_t read_hex_dec(char *c, uint32_t *res);

void print_usage()
{
    printf("* Test program for Telescope Trigger Board controlled by a Trenz MAX1000 board (with MAX10-FPGA + USB-UART interface)\n");
    printf("trigger [options]\n");
    printf("\t--usage : print this short help to all command line options\n");

    printf("\n*** USB to UART serial interface ***\n");
    printf("\t--dev <device_id> : specify the USB device, like /dev/ttyUSB0 (linux) or /dev/ttyS0 (cygwin) or //./COM5 (windows), default %s%d\n", DEVICE_PREFIX, DEVICE_PORT);
    printf("\t--port <%d..> : alternative to --dev, just specify the number of the port, default %d\n", STARTING_PORT, DEVICE_PORT);
    printf("\t--br <1200|2400|4800...115200|...1000000|...|4000000> : UART baudrate, default %d\n", DEF_BRATE);
    printf("\t--nll : skip setting the low latency mode of the USB-UART port\n");
    printf("\t--sll : set the low latency mode of the USB-UART port\n");
    printf("\t--dstatus : show design info & status\n");

    printf("\n*** DAC functions for the old version with MCP4728\n");
    printf("\t--i2c_reset : reset the I2C master in FPGA\n");
    printf("\t--dac_wr2eep : when preceeding --wr_thr: write the DAC value to the built-in EEPROM, otherwise write to volatile RAM\n");
    #ifdef MCP_DAC_TEST_FUNC
    printf("\t--wr_dac <ch> <data> : write the DAC code 0..4095 <data> to DAC channel 0..3 <ch> of the MCP4728 (output register only)\n");
    printf("\t--wr_dac_v <ch> <voltage> : same as --wr_dac, but specify the DAC output voltage 0..2.047V in [V] instead of DAC code\n");
    #endif

    printf("\n*** DAC functions independent on the board version\n");
    printf("\t--wr_thr <ch> <voltage> : write the threshold, old board channels 0..1 positive 0..4 V, channels 2..3 negative 0..-4 V, new board: ch=0..3, -2.048 ... +2.048\n");

    printf("\n*** Trigger functions, all timing settings (width, delay) are in trigger clocks, %0.3f MHz or %0.1f ns, defaults in (), the actual times are one clock longer!\n", TRIGGER_CLOCK/1e6, TRG_CLK_PERIOD_NS);
    printf("\t--inv_busy_inp_ena <ena_busy> <inv_busy> <inv_inp_mask> <ena_inp_mask> : enable (%d)/invert (%d) the busy input, bit mask to invert (0x%x)/enable (0x%x) the inputs\n",
           BUSY_ENA_DEF, BUSY_INV_DEF, INP_INV_DEF, INP_ENA_DEF);
    printf("\t--dgg_width_delays <dgg_width> <dgg_delay0> <dgg_delay1> <dgg_delay2> <dgg_delay3> : gate generator - gate width (%d) & individual delays (%d), all from 0 to %d\n",
           TRG_DGG_WIDTH_DEF, TRG_DGG_DELAY_DEF,  TRG_DGG_MASK);
    printf("\t--trg_inv_thr_width <inv_trg> <trg_thresh> <trg_width> : invert trigger output (%d), trigger threshold (%d) - number of coinc. channels > thresh, trigger width (%d) 0..%d\n",
                TRG_INV_DEF, TRG_THR_DEF, TRG_TWD_DEF, TRG_TWD_MASK);
    printf("\t--trg_dtime <dtime> : dead time after generated trigger (%d) 0 to %d\n", TRG_DTIME_DEF, TRG_DTM_MASK);
    printf("\t--pp_time <ch_mask> <pp_time> : to all channels in the channel mask set the past protection time (%d) 0 to %d\n", PAST_PROT_CH_DEF, TRG_DTM_MASK);
    printf("\t--trg_inp_io <inp_io> <cmp2io> : only for the new board: enable input from I/O instead from comp/tg mask, enable comp/tg to I/O mask. E.g. 0 15 means use inputs from comparators/test generator, I/O used to monitor the comp/tg\n");
    printf("\t--trg_config : report the trigger configuration\n");

    printf("\t--la_pres <presamples> : set the number of presamples in the logic analyzer\n");
    printf("\t--la : dump the last event from the logic analyzer\n");

    printf("\n*** CPU functions\n");
    printf("--cpu_off : permanetly reset the CPU\n");
    printf("--cpu_rst : shortly reset the CPU\n");
    printf("--cpu_on  : enable the CPU\n");
    printf("--wr_imem <filename> : initialize the instruction memory of the CPU, one 24-bit word in hex format per line in the text file\n");
    printf("--start <LA_samples> <CNT_mask> <prefix> : LA_samples - logic analyzer samples, 0 for no LA, max %d, CNT_mask - counter mask, 0 for no counters, max 0x%03x, write to two files: prefix_la.dat and prefix_cnt.dat\n",
                LA_SAMPLES, FULL_CNT_MASK);
    printf("--startr <LA_samples> <CNT_mask> <out_file> : same as --start, but write the incomming data without any decoding to only one file\n");
    printf("--stop : stop sending data by the CPU, can be executed on another terminal window\n");

    printf("\n*** Counter functions\n");
    printf("\t--trg_cnt <prescale> <endtime> <cont> : set prescale factor (18 bit) for the trigger clock %0.1f MHz, the counting time (32 bit) and continuous flag (0 or 1)\n", TRIGGER_CLOCK/1e6);
    printf("\t--trg_cnt_rd : latch and read once the current values of the counters\n");
    printf("\t--trg_cnt_rd_cont : read in a loop the new values of the counters, preferably in continuous mode\n");

    printf("\n*** Test generator functions\n");
    printf("\t--tg_disa : disable the test generator, use it to work with the inputs from the comparators!\n");
    printf("\t--tg_masks <mask0> <mask1> : set 4ch x 8 samples test pattern for each channel, bits 7..0 are first-last sample of ch0, bits 15..8 - of ch 1 etc., mask0 is for the first group of pulses, mask1 for the second\n");
    printf("\t--tg_dist_per <distance> <period> : set distance between the two groups of pulses and the period > distance, both 32-bit, in trigger clock ticks %0.1f MHz\n", TRIGGER_CLOCK/1e6);
    printf("\t--tg_rep <repetitions> : set the number of repetitions (24-bit) and start\n");
}

int main(int argc, char** argv)
{
    C_i2c*     my_i2c;
    C_trigger* my_trg;

    uart32*    my_uart32;

    char dev_id[512];
    int br = DEF_BRATE;
    int debug_main=0;
    int mcp_wr2flash = 0;

    // set the low latency of USB-UART in the linux OS
    // - not necessary in RP4 and some all Linux distributions
    int set_low_latency = DEF_SET_LOW_LATENCY;

    // extensions in interface, we use the default values here
    uint32_t id_mask = 1;

    uint32_t hw_ver;

    sprintf(dev_id, "%s%d", DEVICE_PREFIX, DEVICE_PORT); // default

    if (argc < 2)
    {
        printf("program path: %s\n",argv[0]);
        print_usage();
        return(1);
    }
    else
    for (int idx=1; idx<argc; idx++)
    {
        if (debug_main)
            printf("Current cmd line option is %s\n",argv[idx]);
        if (strcmp(argv[idx],"--dev") == 0 )
        {
            strcpy(dev_id, argv[++idx]);
        }
        else
        if (strcmp(argv[idx],"--port") == 0 )
        {
            sprintf(dev_id, "%s%d", DEVICE_PREFIX, atoi(argv[++idx]));
        }
        else
        if (strcmp(argv[idx],"--nll") == 0 )
        {
            set_low_latency = 0;
        }
        else
        if (strcmp(argv[idx],"--sll") == 0 )
        {
            set_low_latency = 1;
        }
        else
        if (strcmp(argv[idx],"--br") == 0 )
        {
            br = atoi(argv[++idx]);
        }
    }

    my_uart32 = new uart32(dev_id, br);
    // set the default values
    my_uart32->set_slave_mask(id_mask);
    my_uart32->set_max_wsize(DWIDTH);
    my_uart32->set_max_asize(AWIDTH);

    if (set_low_latency == 1)
    {
        char out_arr[534];
        printf("# Setting %s to low latency mode\n", dev_id);
        sprintf(out_arr, "setserial %s low_latency", dev_id);
        system(out_arr);
        usleep(10000);
    }

    // create new object with the ADC parameters
    my_trg = new C_trigger(my_uart32, ADDR_BRIDGE);
    hw_ver = my_trg->get_hw_ver();
    my_i2c = new C_i2c(my_uart32, OFFS_BASE_DAC);

    for (int idx=1; idx<argc; idx++)
    {
        if (debug_main)
            printf("Current cmd line option is %s\n",argv[idx]);
        if (strcmp(argv[idx],"--dev") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--port") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--br") == 0 )
        {
            idx++;
        }
        else
        if (strcmp(argv[idx],"--nll") == 0 )
        {
        }
        else
        if (strcmp(argv[idx],"--sll") == 0 )
        {
        }
        else
        if (strcmp(argv[idx],"--dstatus") == 0 )
        {
            my_trg->print_dstatus();
            my_trg->get_ser_nr(stdout);
        }
        else
        if (strcmp(argv[idx],"--i2c_reset") == 0 )
        {
            if (hw_ver < HW_VER_NEW)
                my_i2c->i2c_sm_reset();
            else
                printf("# i2c_reset function is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else
        if (strcmp(argv[idx],"--dac_wr2flash") == 0 )
        {
            if (hw_ver < HW_VER_NEW)
            {
                mcp_wr2flash = 1;
            }
            else
                printf("# Writing to DAC flash is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else
        if (strcmp(argv[idx],"--wr_thr") == 0 ) // <ch> <dac_voltage>
        {
            uint32_t dac_data, dac_ch;
            float dac_v;

            dac_ch = atoi(argv[++idx]);
            dac_ch &= 3;                        // here still board channel!

            dac_v = atof(argv[++idx]);          // threshold as Voltage
            if (hw_ver < HW_VER_NEW)
            {
                if ((dac_ch < 2) && (dac_v < 0))
                    printf("# Negative threshold possible only on channels 2 and 3 !\n");
                if ((dac_ch > 1) && (dac_v > 0))
                    printf("# Positive threshold possible only on channels 0 and 1 !\n");

                // Old board version
                // board channel 0, 1, 2, 3 are connected to DAC channel 1, 3, 0, 2
                if (dac_ch < 2) dac_ch = 2*dac_ch + 1;
                else            dac_ch = (dac_ch-2)*2;
                // dac_ch is here DAC channel!

                dac_data = my_i2c->mcp4728_thr2dac(dac_ch, dac_v);
                my_i2c->mcp4728_wr(dac_ch, mcp_wr2flash, dac_data, 1);
            }
            else
            {   // New version
                dac_data = my_i2c->ad5624_thr2dac(dac_ch, dac_v);
                my_i2c->spi_wr(dac_ch, dac_data, 1);
                my_i2c->last_dac_rep();
            }
        }
        else
        if (strcmp(argv[idx],"--last_thrs") == 0 ) // <ch> <dac_voltage>
        {
            if (hw_ver >= HW_VER_NEW)
                my_i2c->last_dac_rep();
            else
                printf("# Report last DAC sent values is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else


        if (strcmp(argv[idx],"--inv_busy_inp_ena") == 0 )
        {
            uint32_t ena_busy, inv_busy, inv_inp, ena_inp;

            ena_busy = atoi(argv[++idx]);
            inv_busy = atoi(argv[++idx]);
            read_hex_dec(argv[++idx], &inv_inp);
            read_hex_dec(argv[++idx], &ena_inp);
            my_trg->set_inv_ena(ena_busy, inv_busy, inv_inp, ena_inp);
        }
        else
        if (strcmp(argv[idx],"--dgg_width_delays") == 0 )
        {
            uint32_t dgg_del[4], dgg_width;

            dgg_width = atoi(argv[++idx]);
            for (int i=0; i<4; i++)
                dgg_del[i] = atoi(argv[++idx]);
            my_trg->set_dgg_width_delay(dgg_width, dgg_del[0], dgg_del[1], dgg_del[2], dgg_del[3]);
        }
        else
        if (strcmp(argv[idx],"--trg_inv_thr_width") == 0 )
        {
            uint32_t trg_inv, trg_thresh, trg_width;

            trg_inv = atoi(argv[++idx]);
            trg_thresh = atoi(argv[++idx]);
            trg_width = atoi(argv[++idx]);
            my_trg->set_trg_thresh_inv_width(trg_inv, trg_thresh, trg_width);
        }
        else
        if (strcmp(argv[idx],"--trg_dtime") == 0 )
        {
            uint32_t trg_dtime;

            trg_dtime = atoi(argv[++idx]);
            my_trg->set_trg_dtime(trg_dtime);
        }
        else
        if (strcmp(argv[idx],"--trg_inp_io") == 0 )
        {
            uint32_t inp_io_mask, cmp2io_mask;

            read_hex_dec(argv[++idx], &inp_io_mask);
            read_hex_dec(argv[++idx], &cmp2io_mask);
            if (hw_ver >= HW_VER_NEW)
                my_trg->set_cmp_inp_out(inp_io_mask, cmp2io_mask, 1);
            else
                printf("# I/Os routing is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else
        if (strcmp(argv[idx],"--pp_time") == 0 )
        {
            uint32_t pp_time, pp_ch_mask;

            read_hex_dec(argv[++idx], &pp_ch_mask);
            read_hex_dec(argv[++idx], &pp_time);
            my_trg->set_pp_time(pp_ch_mask, pp_time, 1);
        }
        else
        if (strcmp(argv[idx],"--trg_config") == 0 )
        {
            my_trg->print_trg_config();
        }
        else
        if (strcmp(argv[idx],"--tg_masks") == 0 )
        {
            uint32_t tg_mask0, tg_mask1;

            read_hex_dec(argv[++idx], &tg_mask0);
            read_hex_dec(argv[++idx], &tg_mask1);
            my_trg->set_tg_masks(tg_mask0, tg_mask1);
        }
        else
        if (strcmp(argv[idx],"--tg_dist_per") == 0 )
        {
            uint32_t tg_per, tg_dist;

            read_hex_dec(argv[++idx], &tg_dist);
            read_hex_dec(argv[++idx], &tg_per);
            my_trg->set_tg_distance_period(tg_dist, tg_per);
        }
        else
        if (strcmp(argv[idx],"--tg_rep") == 0 )
        {
            uint32_t tg_rep;

            read_hex_dec(argv[++idx], &tg_rep);
            //my_trg->set_tg_repetitions(tg_rep);
            my_trg->set_tg_repetitions(tg_rep, 1, 1, 0);
        }
        else
        if (strcmp(argv[idx],"--tg_disa") == 0 )
        {
            my_trg->tg_disable();
        }
        else
        if (strcmp(argv[idx],"--la_pres") == 0 )
        {
            uint32_t la_pres;

            read_hex_dec(argv[++idx], &la_pres);
            my_trg->set_la_pres(la_pres);
        }
        else
        if (strcmp(argv[idx],"--la") == 0 )
        {
            my_trg->get_la();
        }
        else
        if (strcmp(argv[idx],"--trg_cnt") == 0 )
        {
            uint32_t trg_cnt_pre, trg_cnt_endt, trg_cnt_cont;

            my_trg->trg_cnt_cmd( 1 << BIT_TRG_CNT_STOP );
            read_hex_dec(argv[++idx], &trg_cnt_pre);
            read_hex_dec(argv[++idx], &trg_cnt_endt);
            read_hex_dec(argv[++idx], &trg_cnt_cont);
            trg_cnt_cont &= 1;
            my_trg->set_trg_cnt(trg_cnt_pre, trg_cnt_endt, (1 << BIT_TRG_CNT_CLR) | (1 << BIT_TRG_CNT_START) | (trg_cnt_cont << BIT_TRG_CNT_CONT));
        }
        else
        if (strcmp(argv[idx],"--trg_cnt_rd") == 0 )
        {
            my_trg->print_trg_cnt(1) ;
        }
        else
        if (strcmp(argv[idx],"--trg_cnt_rd_cont") == 0 )
        {
            while (1)
            {
                my_trg->print_trg_cnt(1) ;
            }
        }
        else
        if (strcmp(argv[idx],"--wr_imem") == 0 )
        {
            uint32_t imem_data[CPU_IMEM_SIZE];
            char inp_file[512];
            FILE* f_inp;

            // open the file with the IMEM for reading
            strcpy(inp_file, argv[++idx]);
            f_inp = fopen(inp_file,"r");
            // read the IMEM from file
            my_trg->read_imem_file(f_inp, imem_data, CPU_IMEM_SIZE);
            fclose(f_inp);
            printf("# Write to IMEM\n");
            my_trg->set_cpu_state(1 << BIT_CONF_CPU_OFF); // CPU reset
            my_trg->set_sh_mem(CPU_IMEM_SIZE, ADDR_BRIDGE + OFFS_BASE_IMEM, imem_data, 1);
            my_trg->set_cpu_state(0); // CPU reset
        }
        else
        if (strcmp(argv[idx],"--cpu_off") == 0 )
        {
            my_trg->set_cpu_state(1 << BIT_CONF_CPU_OFF); // CPU reset
        }
        else
        if (strcmp(argv[idx],"--cpu_rst") == 0 )
        {
            my_trg->set_cpu_state(1 << BIT_CONF_CPU_OFF); // CPU reset
            my_trg->set_cpu_state(0);
        }
        else
        if (strcmp(argv[idx],"--cpu_on") == 0 )
        {
            my_trg->set_cpu_state(0);
        }
        else
        if ( (strcmp(argv[idx],"--start") == 0 ) || (strcmp(argv[idx],"--startr") == 0 ) )
        {
            uint32_t nsamples;
            uint32_t cnt_mask;
            char filename[128];
            int sm_state = SM_STREAM_IDLE; // 0 - idle, 1 - LA data, 2 - CNT data, 3 - error, 4 - end

            int flag_la = 1, flag_cnt = 1, flag_dec = 1;
            char c;
            FILE *f_out_la, *f_out_cnt;

            // class for readout only
            CDataStreamParser myParser;

            // without decoding?
            if (strcmp(argv[idx],"--startr") == 0 ) flag_dec = 0;
            // get the number of LA samples and the counter mask from the command line
            read_hex_dec(argv[++idx], &nsamples);
            read_hex_dec(argv[++idx], &cnt_mask);
            // correct when the mask is larger than the max possible
            cnt_mask &= FULL_CNT_MASK;
            // when cnt_mask is 0 => no counter data
            if ( cnt_mask == 0 ) flag_cnt = 0;
            // when nsamples is 0 => no LA data
            if ( nsamples == 0 ) flag_la = 0;
            // correct the number of samples when > max
            else if ( nsamples > LA_SAMPLES ) nsamples = LA_SAMPLES;

            // open the output files
            if (flag_dec)
            {
                idx++;
                if (flag_la)
                {
                    sprintf(filename, "%s_la.dat", argv[idx]);
                    f_out_la = fopen(filename, "w");
                    myParser.init_la(f_out_la);
                }
                if (flag_cnt)
                {
                    sprintf(filename, "%s_cnt.dat", argv[idx]);
                    f_out_cnt = fopen(filename, "w");
                    myParser.init_cnt(f_out_cnt);
                }
            }
            else
            {   // without decoding only one output file
                f_out_la = fopen(argv[++idx], "w");
            }

            // send INIT command
            my_trg->trg_cpu_run( (1 << CPU_TASK_BIT_INIT), nsamples, cnt_mask);
            // exit when no data enabled
            if ( (flag_la == 0) && (flag_cnt == 0) )
            {
                printf("# No task, both LA and CNT tasks are disabled, exitting!\n");
                return -2;
            }
            printf("# Start acquisition: counter mask 0x%03x, LA samples %d\n", cnt_mask, nsamples);
            // start the acquisition, set the corresponding bits in the CPU job word
            my_trg->trg_cpu_run( (1 << CPU_TASK_BIT_RUN ) | (flag_cnt << CPU_TASK_BIT_CNT ) | (flag_la << CPU_TASK_BIT_LA ), nsamples, cnt_mask);
            // loop to receive & store the UART data
            do
            {   // getting one character
                c = my_uart32->ReadSingleByte();
                // when decoding...
                if (flag_dec)
                {
                    switch (sm_state)
                    {
                        case SM_STREAM_IDLE :
                        // expected to detect the beginning of substream
                            if (c == BEG_TMS) sm_state = myParser.process_data_la(c, f_out_la);
                            else
                            if (c == CNT_PRD) sm_state = myParser.process_data_cnt(c, f_out_cnt);
                            else
                            // or the end marker
                            if (c == UART_END_OF_FILE) sm_state = SM_STREAM_END;
                            break;
                        // logic analyzer data expected
                        case SM_STREAM_LA :
                            sm_state = myParser.process_data_la(c, f_out_la);
                            break;
                        // counter data expected
                        case SM_STREAM_CNT :
                            sm_state = myParser.process_data_cnt(c, f_out_cnt);
                            break;
                    }
                }
                else
                // when just storing the input data without decoding...
                {
                    // end of file can come frequently in case of low data rate
                    // and must be ignored!
                    if (c != EOF)
                        fprintf(f_out_la, "%c", c);
                    // flush the data when new event or new counter block come
                    // to avoid loosing much data after Ctrl-C
                    if ( ( (c == BEG_TMS) && (flag_la ) ) || ( (c == CNT_MSK) && (flag_cnt) ) )
                        fflush(f_out_la);
                    if (c == UART_END_OF_FILE)
                        sm_state = SM_STREAM_END;
                }
            } while ( (sm_state != SM_STREAM_END) && (sm_state != SM_STREAM_ERR) );

            switch (sm_state)
            {
                case SM_STREAM_ERR : printf("# Stop because of data error!\n"); break;
                case SM_STREAM_END : printf("# Stop because of end of data marker!\n");break;
            }
            // close the open files
            if ((flag_dec == 0) || (flag_la  == 1) ) fclose(f_out_la);
            if ((flag_dec == 1) && (flag_cnt == 1) ) fclose(f_out_cnt);
        }
        else
        // this command can be executed from another terminal window
        if (strcmp(argv[idx],"--stop") == 0 )
        {
            my_trg->trg_cpu_run( 1 << CPU_TASK_BIT_STOP, 0);
        }
        else
#ifdef MCP_DAC_TEST_FUNC
        // DAC MCP4728 Test functions
        if (strcmp(argv[idx],"--wr_dac") == 0 ) // <ch> <dac_data>
        {
            uint32_t dac_data, dac_ch;

            dac_ch = atoi(argv[++idx]);
            read_hex_dec(argv[++idx], &dac_data);
            if (hw_ver < HW_VER_NEW)
                my_i2c->mcp4728_wr(dac_ch, mcp_wr2flash, dac_data, 1);
            else
                printf("# Write to MCP DAC is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else
        if (strcmp(argv[idx],"--wr_dac_v") == 0 ) // <ch> <dac_voltage>
        {
            uint32_t dac_data, dac_ch;
            float dac_v;

            dac_ch = atoi(argv[++idx]);
            dac_v  = atof(argv[++idx]);
            if (hw_ver < HW_VER_NEW)
            {
                dac_data = my_i2c->mcp4728_v2dac(dac_v);
                my_i2c->mcp4728_wr(dac_ch, mcp_wr2flash, dac_data, 1);
            }
            else
                printf("# Write to MCP DAC is not available on a board with hardware version %d !!!\n", hw_ver);
        }
        else
#endif
        {
            printf("*********** UNKNOWN COMMAND OPTION !!! %s\n", argv[idx]);
            print_usage();
            return(1);
        }
    }
    return 0;
}

int32_t read_hex_dec(char *c, uint32_t *res)
{
    int32_t e,d;
    uint32_t w;
    e = -1;
    if(strlen(c) > 2)
    {
        if (strncmp(c, "0x",2) == 0) // hex
        {
                e = sscanf(c, "0x%x", &w);
                *res = w;
        } else { // dec
                e = sscanf(c, "%d", &d);
                *res = d;
        }
    }
    else
    { // short dec
        e = sscanf(c, "%d", &d);
        *res = d;
    }

    if (e == 1) return 0;
    else return -1;
}
