#include "CDataStreamParser.h"

CDataStreamParser::CDataStreamParser()
{

}

CDataStreamParser::~CDataStreamParser()
{
    // nothing
}




int32_t CDataStreamParser::init_cnt(FILE *f_out)
{
    fprintf(f_out, "# Period# | Counters: inp 0..3 | inp blocked by pp 4..7 | 8-trg, trg-no-busy-dead_time, 10-trg-busy-no-dead_time, 11-trg-busy-dead_time\n");
    return 0;
}

int32_t CDataStreamParser::init_la(FILE *f_out)
{
   ubuff = 0;
   // counter for diagnostics
   n_L = n_S = n_W = n_R = 0;

   printf("Preparing output files...\n");
   fprintf(f_out, "# Time[%dns] | Ch0..3 | inp edge | DGG-out | VETO  | Trg-Start | Trg-Out |\n", TIME_STEP);
   nsamples = 0;
   event_nr = 0;
   last_timestamp = 0;
   ch = UNKNOWN;
   rep_cnt = 0;

   return 0;
}


int32_t CDataStreamParser::print_one_sample(uint32_t sdata, FILE *f_out)
{
    uint32_t la_data;

    la_data = sdata & 0x7FFF;

    fprintf(f_out, "%4d ", now_time);
    for (int bit=0; bit < LA_SIGNALS; bit++)
    {
        fprintf(f_out, " %d", la_data & 1);
        if ((bit & 3) == 3)
        fprintf(f_out, "  ");
        la_data >>= 1;
    }
    fprintf(f_out, "\n");
    nsamples--;
    now_time++;

    if (nsamples  < 0)
    {
        fprintf(f_out, "# ERROR - data after END OF EVENT\n");
        return 1;
    }

    if (nsamples == 0)
    {
        event_nr++;
        fprintf(f_out, "# END OF EVENT %d\n", event_nr);
        fflush(f_out);
    }
    return 0;
}


int32_t CDataStreamParser::process_data_cnt(int c, FILE *f_out)
{
    nibble = -1;

    if ( (c >= '0') && (c <= '9') )
        nibble = c - '0';
    if ( (c >= 'A') && (c <= 'F') )
        nibble = c - 'A' + 10;
    if ( (c >= 'a') && (c <= 'f') )
        nibble = c - 'A' + 10;
    // Convert the character to a 32-Bits unsigned integer
    if (nibble >= 0)
    {
        if (debug_main_str)
            printf("(%x) ", nibble);
        ubuff <<= 4;
        ubuff |= nibble & 0xF;
        // was a hex digit - exit
        return SM_STREAM_CNT;
    }
    if  (c == CNT_PRD)
    {
        ch = CNT_CH_N;
        // was 'N' - exit
        return SM_STREAM_CNT;
    }
    else
    if (c == CNT_MSK )
    {
        ch = CNT_CH_M;
        // was 'M' - exit
        return SM_STREAM_CNT;
    }
    else
    if (c == UART_END_OF_EVNT)
    {
        return SM_STREAM_IDLE;
    }
    else
    if (c == UART_LINE_FEED) // LINE FEED, expected numerical data accumulated in ubuff
    {
        if (ch == CNT_CH_N) // new timestamp => new event
        {
            cnt_prd = ubuff;
            ubuff = 0;
            fprintf(f_out, "%3d", cnt_prd);
            return SM_STREAM_CNT;
        }
        else
        if (ch == CNT_CH_M) // new data length
        {
            cnt_mask = ubuff;
            cnt_mask_cp = ubuff;
            ch = CNT_CH_C;
            ubuff = 0;
            if (cnt_mask == 0)
            {
                // no more counter data will come, just write to the file
                // 0 for all counters and exit
                for (int i=0; i<N_COUNTERS; i++)
                    fprintf(f_out, " %3d", 0);
                fprintf(f_out, "\n");
                fflush(f_out);
                return SM_STREAM_IDLE;
            }
            else
                return SM_STREAM_CNT;
        }
        else
        if (ch >= CNT_CH_C) // counter data
        {
            // find the first channel with non-zero data
            while ( ( (cnt_mask & 1) == 0) && (cnt_mask) )
            {
                ch++;
                cnt_mask >>= 1;
            }
            // if a non-zero channel was found - store the counter data to the array element
            if (cnt_mask)
            {
                counters[ch-CNT_CH_C] = ubuff;
                ubuff = 0;
                // prepare the next channel nr.
                do
                {
                    ch++;
                    cnt_mask >>= 1;
                } while ( ( (cnt_mask & 1) == 0) && (cnt_mask) );
            }
            if (cnt_mask == 0) // no more data - write the counter data to the file
            {
                for (int i=0; i<N_COUNTERS; i++)
                {
                    if (cnt_mask_cp & 1)
                        fprintf(f_out, " %3d", counters[i]);
                    else
                        fprintf(f_out, " %3d", 0);
                    cnt_mask_cp >>= 1;
                }
                fprintf(f_out, "\n");
                fflush(f_out);
                // was last expected counter value - exit to idle
                return SM_STREAM_IDLE;
            }
            // expected more counter data, stay in the same state
            return SM_STREAM_CNT;
        }
    }
    return SM_STREAM_ERR;
}

int32_t CDataStreamParser::process_data_la(int c, FILE *f_out)
{
//    static int i_old = -1;

    if (c >= 0)
    {
        if (debug_main_str)
           printf(" %02x", c);
        nibble = -1;

        if ( (c >= '0') && (c <= '9') )
           nibble = c - '0';
        if ( (c >= 'A') && (c <= 'F') )
           nibble = c - 'A' + 10;
        if ( (c >= 'a') && (c <= 'f') )
           nibble = c - 'A' + 10;
        // Convert the character to a 32-Bits unsigned integer
        if (nibble >= 0)
        {
           if (debug_main_str)
              printf("(%x) ", nibble);
           ubuff <<= 4;
           ubuff |= nibble & 0xF;
        }

          if      (c == BEG_TMS)
          {
              ch = TMS;
              n_S++;
          }
          else if ( ch != UNKNOWN)
          {
              if (c == BEG_LEN )
              {
                  ch = LEN;
                  n_L++;
              }
              else if (c == BEG_WAV)
              {
                  ch = WAV;
                  n_W++;
              }
              else if (c == REP_BLK)
              {
                  ch = REP;
                  n_R++;
              }
              else if (c == UART_END_OF_EVNT)
              {
                  return SM_STREAM_IDLE;
              }
              else if (c == UART_LINE_FEED) // LINE FEED
              {
                    if (ch == TMS) // new timestamp => new event
                    {
                        fprintf(f_out, "\n\n\n");
                        fprintf(f_out, "# Timestamp 0x%08x, increment %d\n",
                            (uint32_t) ubuff, (uint32_t) ( (uint32_t) ubuff - last_timestamp) );
                        last_timestamp = ubuff;
                    }
                    else
                    if (ch == LEN) // new data length
                    {
                        nsamples = ubuff;
                        fprintf(f_out, "# Expected time window %d ns\n", nsamples*TIME_STEP);
                        now_time = 0; // ns
                    }
                    else
                    if (ch == REP) // new repeat block
                    {
                        rep_cnt = ubuff;
                        //printf("# Repeat block with %d entries\n", rep_cnt);
                        while (rep_cnt > 0)
                        {
                            print_one_sample(last_sample_word, f_out);
                            rep_cnt--;
                        }
                        ch = WAV;
                    }
                    else
                    if (ch == WAV) // new sample data
                    {
                        last_sample_word = ubuff;
                        print_one_sample(ubuff, f_out);
                    }
                    ubuff = 0;
              }
          }
          if ( (c == UART_END_OF_FILE) || (c == EOF) )
          {
             if (debug_main_str)
                printf("\n# *** EOF ***\n");

             //Close all files
             fclose(f_out);
             return SM_STREAM_END;
          }
    }
    return SM_STREAM_LA;
}
