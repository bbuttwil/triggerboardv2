// $Id: i2c.h 1137 2025-01-27 18:20:49Z angelov $:
#ifndef I2C_H
#define I2C_H

// $Id: i2c.h 1137 2025-01-27 18:20:49Z angelov $:

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>  /* String function definitions */
#include <array>
#include "lib/CLogger.h"

#define DEBUG_I2C_WR            0
#define DEBUG_I2C_RD            0

//#define N_I2C_MASTERS   3               // in the prototype is 1, later is 3, the hardware & software
                                        // are developed for 1..4

// 32-bit data words configuration offsets in FPGA SPI master
#define OFFS_I2C_DATA           0       // write here the data to be sent:
                                        //      bits 7..0 sent first, bits 15..8 send next and so on..., up to 4 bytes can be send
                                        // read from here the received data:
                                        //      bits 7..0 received first, bits 15..8 received next and so on..., up to 4 bytes can be received
#define OFFS_I2C_CMD            1       // write only, command to be executed
    #define BIT_I2C_R1W0        0       // bit 0 is read (1) or write (0)
    #define BIT_I2C_SLV_AD      1       // bits 7..1 is the slave address
    #define BIT_I2C_SPEC_CMD    8       // bits 10..8 is command

      #define CMD_I2C_NULL  0           // simple i2c read or write
      #define CMD_I2C_RSTRT 7           // write something, then repeated start and read something
      // these commands normally are not intended to be used directly by the user!
      #define CMD_I2C_STOP  1           // send stop
      #define CMD_I2C_START 2           // send start
      #define CMD_I2C_GETA  3           // get  acknowledge
      #define CMD_I2C_GIVA  4           // give acknowledge
      #define CMD_I2C_PUTB  5           // send one byte
      #define CMD_I2C_GETB  6           // receive one byte

    #define BIT_I2C_LENGTH     12       // 13..12 : 0..3 for bytes in the first packet (slave address not counted!)
    #define BIT_I2C_LENGTH2    14       // 15..14 : 0..3 for bytes in the second packet, when SPEC_CMD is RSTRT (slave address not counted!)
    #define BIT_I2C_RESET_SM   16       // reset the state machines

#define OFFS_I2C_STA            1       // read only status information
                                        // bits 15..0 have the same meaning as in the CMD
    #define BIT_I2C_STA_SMS    16       // 4-bits state of the slave state machine, 0 is idle
    #define BIT_I2C_STA_SMM    20       // 4-bits state of the master state machine, 0 is idle
    #define BIT_I2C_STA_TOUT   24       // timeout now
    #define BIT_I2C_STA_TOUTL  25       // timeout latched
    #define BIT_I2C_STA_IDLE   28       // all state machines are idle

#define I2C_STD_TIMEOUT        10       // how many times to get the status of the state machine waiting for ready

// 1. send SLV_ADDR << 1, write bit 0
#define MCP4728_SLV_ADDR  0x60  // 7-bit address, need to be shifted left, bits 2..0 can be programmed
// 2. send the command to write to the DAC register and EEPROM
#define MCP4728_CMD_WR_DAC_EEP(ch) (0x58 | ((ch & 3) << 1) )
// 2. send the command to write to the DAC registers only
#define MCP4728_CMD_WR_DAC(ch) (0x40 | ((ch & 3) << 1) )
// 3. send the first byte with the DAC settings + the upper 4 bits
#define MCP4728_FIRST_BYTE(vref, pd, gain, dac)  ( ( (vref & 1) << 7) | ( (pd & 3) << 5) | ( ( gain & 1) << 4 ) | ((dac & 0xFFF) >> 8) )
// 4. send the lower 8 bits
#define MCP4728_SECOND_BYTE(dac)  ( dac & 0xFF )

// power down bits, normally
#define MCP4728_PD_BITS_NORMAL    0
#define MCP4728_PD_BITS_1K_GND    1
#define MCP4728_PD_BITS_100K_GND  2
#define MCP4728_PD_BITS_500K_GND  3

#define MCP4728_VREF_VDD          0
#define MCP4728_VREF_2_048V       1
#define MCP4728_VREF          2.048 // V
#define MCP4728_NBITS            12
#define MCP4728_DAC_STEPS      (1L << MCP4728_NBITS)
#define MCP4728_DAC_MSK        (MCP4728_DAC_STEPS-1)
#define MCP4728_DAC2V(dac)    (MCP4728_VREF*dac/MCP4728_DAC_STEPS) // dac*Full_Voltage/Full_DAC_steps

#define AD5624_VREF           2.048 // V
#define AD5624_NBITS             12
#define AD5624_DAC_UNKNOWN       AD5624_NBITS
#define AD5624_DAC_BUSY         (AD5624_NBITS+1)
#define AD5624_DAC_STEPS      (1L << AD5624_NBITS)


// we will work with the internal VREF=2.048V and Gain=1
#define MCP4728_GAIN_1            0
#define MCP4728_GAIN_2            1 // only in case of internal VREF possible

class C_i2c : public CLogger
{
protected:
    // pointer to the UART interface
    uart32* m_uart32;
    // base address of the SPI master in the FPGA design
    uint32_t base_addr;
    uint32_t last_status;
    uint32_t last_sent;   // read back the last sent data in case of simple write
    // copy a bit slice from src[pos_high-pos_low..0] to dst[pos_high..pos_low]
    uint16_t cp_bit_slice(uint16_t src, uint16_t dst, uint8_t pos_high, uint8_t pos_low);
    // returns 0 when ok, 1 when i2c timeout (missing slave) and 2 when communication timeout
    int i2c_wt_rdy(uint32_t flag_mask, uint16_t timeout);
    int i2c_wt_rdy(uint16_t timeout=I2C_STD_TIMEOUT);
    int i2c_sm_cmd(uint32_t cmd_word);
    // mask = 0xFF << 16 can be used to wait until the state machines are idle
    uint32_t i2c_last_sent();
    // write 1 to 4 bytes, with or without waiting for the master to be ready. When waiting the return code
    // comes from i2c_wt_rdy (s. above), otherwise is 0.
    uint32_t i2c_wr(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nbytes, uint8_t wait_for_rdy=1);
    // full read transaction
    uint32_t i2c_rd(uint8_t slave_addr, uint8_t *recv_bytes, uint8_t nbytes);
    // start the read transaction, don't wait for the result
    uint32_t i2c_rd(uint8_t slave_addr, uint8_t nbytes);
    // wait until i2c master ready and get the result
    uint32_t i2c_rd(uint8_t *recv_bytes, uint8_t nbytes);

    // start the write-read transaction, don't wait for the result
    uint32_t i2c_wr_rd(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nsbytes, uint8_t nrbytes);
    // full read transaction
    uint32_t i2c_wr_rd(uint8_t slave_addr, uint8_t *send_bytes, uint8_t nsbytes, uint8_t *recv_bytes, uint8_t nrbytes);
public:
    // set the pointer to the UART device, the base address in FPGA design, init is still not used
    C_i2c(uart32* p_uart32, uint32_t new_base_addr);
    ~C_i2c();

    // reset the I2C state machine
    int i2c_sm_reset();

    uint32_t mcp4728_wr(uint8_t dac_ch, uint8_t eep, uint8_t vref, uint8_t pd, uint8_t gain, uint16_t dac, uint8_t wait_for_rdy);
    // convert Voltage to DAC code
    uint16_t mcp4728_v2dac(float v_dac);
    // convert negative threshold to DAC code
    uint16_t mcp4728_thr2dac_neg(float v_in);
    // convert positive threshold to DAC code
    uint16_t mcp4728_thr2dac_pos(float v_in);

    // write to DAC with our default configuration VREF=2.048V, no power down, Gain=1, wait until ready
    uint32_t mcp4728_wr(uint8_t dac_ch, uint8_t eep, uint16_t dac, uint8_t ldebug=0);

    // convert threshold to DAC code
    uint16_t mcp4728_thr2dac(int dac_ch, float v_in);

    // functions for the SPI DAC AD5624
    uint16_t ad5624_thr2dac(uint16_t ch, float v_in, uint8_t ldebug = 0);
    float    ad5624_dac2thr(uint16_t dac_v, uint8_t ldebug = 0);
    uint32_t spi_wr(uint16_t ch, uint16_t wdata, uint8_t ldebug=0);
    uint32_t last_dac_rep();

    std::array<uint32_t, 4> last_dac_rep_values();

};

#endif // I2C_H
