#ifndef DATA_STREAM_PARSER_H
#define DATA_STREAM_PARSER_H

#include <stdint.h>

#include <stdio.h>   /* Standard input/output definitions, for perror() */
#include <string.h>  /* String function definitions */

#define debug_main_str  0

#define N_INPUTS        8

#define UNKNOWN         0
#define TMS             1
#define LEN             2
//#define TMP             3
#define WAV             3
#define REP             4

#define UART_LINE_FEED   0x0A
// if encounter this character, stop receiving data and write to a file
#define UART_END_OF_FILE 'Z'  // 0x04
#define UART_END_OF_EVNT 'V'  // 0x0C

#define BEG_TMS         'S'
//#define BEG_TMP         'T'
#define BEG_LEN         'L'
#define BEG_WAV         'W'
//#define END_WAV         'w'
#define REP_BLK         'R'

// begin block with counters, first number if the period counter
#define CNT_PRD         'N'
// then comes M followed by the mask with the counters sent
#define CNT_MSK         'M'
// then come only the selected counters, each with LF at the end

#define TIME_STEP        5             // ns
#define LA_SIGNALS      15

#define SM_STREAM_IDLE   0
#define SM_STREAM_LA     1
#define SM_STREAM_END    4
#define SM_STREAM_CNT    2
#define SM_STREAM_ERR    3

#define CNT_CH_N        0
#define CNT_CH_M        1
#define CNT_CH_C        2
#define N_COUNTERS     12

class CDataStreamParser
{
   public:
      CDataStreamParser();
      ~CDataStreamParser();
      int32_t print_one_sample(uint32_t sdata, FILE *f_out);
//      int32_t print_counters(uint32_t sdata, FILE *f_out_la);
      int32_t process_data_la( int c, FILE* f_out);
      int32_t process_data_cnt(int c, FILE* f_out);
      int32_t init_la( FILE *f_out);
      int32_t init_cnt(FILE *f_out);

   private:
      int32_t nibble;
      int32_t ch;       // indicate the type of the data

      uint64_t ubuff;   // Save the incomming unsigned number
      uint64_t last_sample_word;
      int32_t n_S, n_L, n_W, n_R;

      uint32_t nsamples;
      uint32_t cnt_mask, cnt_mask_cp, cnt_prd = 0;
      int32_t  rep_cnt;
      uint32_t event_nr;
      uint32_t now_time;
      uint32_t last_timestamp;
      int32_t i;
      uint32_t counters[N_COUNTERS];
};

#endif

// Example of data:
//  S6C2E62BE   S - timestamp
//  L40         L - length
//  WC          W - waveform, C here is the first LA sample
//  RE          R - repeat, here E-times
//  0               sample (without leading 0's)
//  22
//  2
//  R4
//  0
//  44
//  9D
//  D
//  R1
//  60D
//  E0D
//  R1
//  2E0D
//  5E0D
//  R2
//  5E0C
//  5F0C
//  1F0C
//  190C
//  110C
//  R6
//  100C
//  R12
//  VN3C5         V - end of the event, N - counter data, here 3C5 - counter for the periods
//  M10F          M - counter mask
//  A                 counter0 data
//  A                 counter1 data
//  A                 counter2 data
//  A                 counter3 data
//  A                 counter8 data
