// $Id: C_trigger.cpp 1137 2025-01-27 18:20:49Z angelov $:
//#include <unistd.h>  /* UNIX standard function definitions, usleep */
#include "lib/uart32.h"
#include "C_trigger.h"

C_trigger::C_trigger(uart32* p_uart32, uint32_t new_base_addr)
{
    m_uart32 = p_uart32;
    base_addr_cnf = new_base_addr + OFFS_BASE_CONF ;
    base_addr_trg = new_base_addr + OFFS_BASE_TRIGG;
    last_status = UNDEFINED;
    last_svn    = UNDEFINED;
    last_cmp    = UNDEFINED;
    get_status();
}

C_trigger::~C_trigger()
{
}

uint32_t C_trigger::set_cpu_state(int cpu_cmd_bits, int ldebug)
{
    cpu_cmd_bits &= 0xFF;
    if (ldebug)
    {
        logWrite("# Writing to the CPU state register 0x%02x =>", cpu_cmd_bits);
        if ((cpu_cmd_bits >> BIT_CONF_CPU_OFF) & 1) logWrite(" CPU_OFF");
        else
                                                    logWrite(" CPU_ON");
        if ((cpu_cmd_bits >> BIT_CONF_SFT_RST) & 1) logWrite(" SFT_RST");
        logWrite("\n");
    }
    WriteReg(1, base_addr_cnf + OFFS_CONF_CMD, cpu_cmd_bits);
    return last_status;
}

uint32_t C_trigger::get_status(void)
{
    if (last_status == UNDEFINED) last_status = ReadReg(4, base_addr_cnf + OFFS_CONF_VER);
    return last_status;
}

uint32_t C_trigger::get_hw_ver(void)
{
    return ( get_status() >> BIT_CONF_VER_HW) & 0xF;
}

uint32_t C_trigger::get_fpga_size(void)
{
    return (last_status >> STA_FPGA_SIZE  ) & 0xFF;
}

uint8_t C_trigger::print_dstatus()
{
    logWrite(  "#\n");
    get_status();
    logWrite(  "# FPGA size %d kLUT, Hardware Ver %d, Assembly Ver %d\n",
        (last_status >> STA_FPGA_SIZE  ) & 0xFF,
        (last_status >> BIT_CONF_VER_HW) & 0xF,
        (last_status >> BIT_CONF_VER_AS) & 0xF);
    logWrite(  "# Design compiled for image %d\n",
        (last_status >> BIT_CONF_VER_IMG) & 1);
    logWrite(  "# CPU IMEM size %d words, DMEM size %d dwords\n",
        1 << ( (last_status >> STA_CPU_IM_AB) & 0xF),
        1 << ( (last_status >> STA_CPU_DM_AB) & 0xF) );

    print_svn_id();
    print_compile_id();

    logWrite(  "#\n");

    return 0;
}

/* uint32_t C_trigger::config_status()
{
    int i;
    uint32_t rd_reg[8]; // the dual config core has 8 addresses, we will read not all of them, but will reserve all 8
    m_uart32->WriteByte(AV_RCU_BASE+2, 0x0F); // trigger the reading
    // read data max 17-bit , reading 0..2 is not ok, they are write only!
    m_uart32->RecvBurst24(5, 1, 1, AV_RCU_BASE+3, rd_reg+3);
//    if (debug)
//        for (i=3; i<8; i++)
//            logWrite(  "# Status regs at offset %d: 0x%08x\n", i, rd_reg[i] );

    logWrite(  "# ");

    print_msm_cs((rd_reg[6] >> 4) & 0xF);
    logWrite(  " (oldest)\n#\t => ");

    print_msm_cs((rd_reg[5] >> 4) & 0xF);
    logWrite(  " (previous)\n#\t\t => ");

    print_msm_cs((rd_reg[4] >> 13) & 0xF);
    logWrite(  " (present)\n");

    logWrite(  "# config_sel_overwrite is %d, config_sel(intern) is %d\n", rd_reg[7] & 1, (rd_reg[7] >> 1) & 1);

    return 0;
}


uint8_t C_trigger::print_fstatus()
{
    uint64_t tr_id;
    uint32_t tr_id_short;

    logWrite(  "#\n");

    
    logWrite(  "# FPGA size is %02d kLUT\n", (last_status >> STA_FPGA_SIZE) & 0xFF);
    logWrite(  "# Hardware Ver %d, Assembly Ver %d\n", (last_status >> BIT_CONF_VER_HW) & 0xF, (last_status >> BIT_CONF_VER_AS) & 0xF);
    logWrite(  "# Design compiled for image %d\n", (last_status >> BIT_CONF_VER_IMG) & 1);

    tr_id = read_short_trace_id();
    tr_id_short = (tr_id ^ (tr_id >> 32)) & 0xFFFFFFFF;
    logWrite(  "# Full TRACE_ID is: %08x %08x, short id is 0x%08x\n", (uint32_t) (tr_id >> 32), (uint32_t) (tr_id & 0xFFFFFFFF), tr_id_short );

    print_svn_id();
    print_compile_id();
    logWrite(  "# Boot info:\n");

    config_status();
    logWrite(  "#\n");

    return 0;
} */


    // get (and print) subversion revision nr, date & time, compilation time etc.
uint32_t C_trigger::get_svn_id(void)
{
//    last_svn=m_uart32->Read3Bytes(base_addr_cnf + OFFS_CONF_SVN);
    last_svn=ReadReg(4, base_addr_cnf + OFFS_CONF_SVN);
    return last_svn;
}

uint16_t C_trigger::get_svn_nr(void)
{
    return (get_svn_id() >> 13) & 0x7FF;
}

uint32_t C_trigger::get_compile_id(void)
{
//    last_cmp=m_uart32->Read3Bytes(base_addr_cnf + OFFS_CONF_CMP);
    last_cmp=ReadReg(4, base_addr_cnf + OFFS_CONF_CMP);
    return last_cmp;
}

uint32_t C_trigger::print_compile_id(uint32_t cmp_id)
{
   logWrite(  "# Compiled on %4d-%02d-%02d at %02d:%02d\n", (cmp_id       & 0x0F) + 2015,
                                                            (cmp_id >> 4) & 0x0F,
                                                            (cmp_id >> 8) & 0x1F,
                                                            (cmp_id >>13) & 0x1F,
                                                            (cmp_id >>18) & 0x3F);

    return cmp_id;
}

uint32_t C_trigger::print_compile_id()
{
    return print_compile_id(get_compile_id());
}

uint32_t C_trigger::print_svn_id(uint32_t svn_id)
{
    logWrite(  "# SVN revision %d from %4d-%02d-%02d\n", (svn_id >>13) & 0x7FF,
                                                         (svn_id       & 0x0F) + 2015,
                                                         (svn_id >> 4) & 0x0F,
                                                         (svn_id >> 8) & 0x1F);
    return svn_id;
}

uint32_t C_trigger::print_svn_id()
{
    return print_svn_id(get_svn_id() );
}


    // copy a bit slice from src[pos_high-pos_low..0] to dst[pos_high..pos_low]
uint16_t C_trigger::cp_bit_slice(uint16_t src, uint16_t dst, uint8_t pos_high, uint8_t pos_low)
{
    uint32_t msk, neg;

    msk = 1L << (pos_high+1);
    msk--;
    neg = 1L <<  pos_low    ;
    neg--;

    msk ^= neg;  // selected bits
    neg = ~msk;  // the not selected bits

    return ( ( (src << pos_low) & msk) | (dst & neg) ) & 0xFFFF;
}

// invert busy, invert discr. inputs mask, enable discr. inputs mask
int C_trigger::set_inv_ena(uint8_t ena_busy, uint8_t inv_busy, uint8_t inv_dis_inp, uint8_t ena_dis_inp, uint8_t ldebug)
{
    uint16_t w;

    // inv_b(8) & inv(7..4) & ena(3..0) => 9 bits

    w = ena_busy & 1;
    w <<= 1;
    w |= inv_busy & 1;
    w <<= TRG_NCH;

    w |= inv_dis_inp & TRG_CH_MASK;
    w <<= TRG_NCH;

    w |= ena_dis_inp & TRG_CH_MASK;

    if (ldebug)
        logWrite("# Set: enable/invert busy %d/%d, invert inputs mask 0x%x, enable inputs mask 0x%x\n",
            ena_busy, inv_busy, inv_dis_inp, ena_dis_inp);

    int ret = m_uart32->WriteWord(OFFS_TRG_INP_INV_ENA+base_addr_trg, w );
    printf("BBBBBBB got ret: %d", ret);
    return ret;
}

std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> C_trigger::get_inv_ena()
{
    uint32_t trg_config[1], w;
    m_uart32->RecvBurst(1, 1, 1, base_addr_trg + OFFS_TRG_INP_INV_ENA, trg_config );
    w = trg_config[0];
    logWrite("# Got: enable busy %d, invert busy %d, invert inputs mask 0x%x, enable inputs mask 0x%x\n",
            (w >> (2*TRG_NCH +1)) & 1,
            (w >> (2*TRG_NCH)) & 1,
            (w >>    TRG_NCH)  & TRG_CH_MASK,
             w                 & TRG_CH_MASK);
    uint8_t enable_busy = w >> (2*TRG_NCH + 1) & 1;
    uint8_t inv_busy = w >> (2*TRG_NCH) & 1;
    uint8_t inv_inputs_mask = w >> TRG_NCH  & TRG_CH_MASK;
    uint8_t enable_inputs_mask = w & TRG_CH_MASK;
    return std::make_tuple(inv_busy, inv_inputs_mask, enable_busy, enable_inputs_mask);
}


// delay gate genarators for each discriminator inputs, width of the generated pulse and individual delays
int C_trigger::set_dgg_width_delay(uint8_t dgg_width, uint8_t dgg_del0, uint8_t dgg_del1, uint8_t dgg_del2, uint8_t dgg_del3, uint8_t ldebug)
{
    uint32_t w;

    // width & delay[3] & delay[2] & delay[1] & delay[0] => 20 bits

    dgg_width--;
    dgg_width &= TRG_DGG_MASK;
    dgg_del3  &= TRG_DGG_MASK;
    dgg_del2  &= TRG_DGG_MASK;
    dgg_del1  &= TRG_DGG_MASK;
    dgg_del0  &= TRG_DGG_MASK;

    w  = dgg_width;
    w <<= TRG_DGG_BITS;

    w |= dgg_del3;
    w <<= TRG_DGG_BITS;

    w |= dgg_del2;
    w <<= TRG_DGG_BITS;

    w |= dgg_del1;
    w <<= TRG_DGG_BITS;

    w |= dgg_del0;

    if (ldebug)
    {
        logWrite("# Set delay gate generators at the inputs: DELAYs 0..3: %d = %0.1f ns, %d = %0.1f ns, %d = %0.1f ns, %d = %0.1f ns,",
            dgg_del0,  TRG_CLK_PERIOD_NS*dgg_del0,
            dgg_del1,  TRG_CLK_PERIOD_NS*dgg_del1,
            dgg_del2,  TRG_CLK_PERIOD_NS*dgg_del2,
            dgg_del3,  TRG_CLK_PERIOD_NS*dgg_del3);
        logWrite(" GATE %d = %0.1f ns\n",
            dgg_width+1, TRG_CLK_PERIOD_NS*(dgg_width+1) );
    }
    int ret = m_uart32->WriteDWord(OFFS_TRG_DGG_WIDTH_DEL+base_addr_trg, w );

    return ret;
}

std::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> C_trigger::get_dgg_width_delay()
{
    uint32_t trg_config[1], w;
    m_uart32->RecvBurst(1, 1, 1, base_addr_trg + OFFS_TRG_DGG_WIDTH_DEL, trg_config );
    w = trg_config[0];

    uint8_t dgg_delay0 = w & TRG_DGG_MASK;
    w >>= TRG_DGG_BITS;
    uint8_t dgg_delay1 = w & TRG_DGG_MASK;
    w >>= TRG_DGG_BITS;
    uint8_t dgg_delay2 = w & TRG_DGG_MASK;
    w >>= TRG_DGG_BITS;
    uint8_t dgg_delay3 = w & TRG_DGG_MASK;
    w >>= TRG_DGG_BITS;
    uint8_t dgg_width = w & TRG_DGG_MASK;
    dgg_width++;
    printf("GATE: %d    Delay0: %d    Delay1: %d     Delay2: %d     Delay3: %d\n", dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3);
    return std::make_tuple(dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3);
}

// set the trigger threshold (how many discriminator channels must overlap for trigger)
//         invert trigger or not, width of the output pulse in trigger clocks
int C_trigger::set_trg_thresh_inv_width(uint8_t trg_inv, uint8_t trg_thresh, uint8_t trg_width, uint8_t ldebug)
{
    uint16_t w;
    // inv(7) & thresh(6..4) & width(3..0) => 8 bits
    trg_inv &= 1;
    trg_thresh &= TRG_THR_MASK;
    trg_width  &= TRG_TWD_MASK;

    if (trg_width < 2) trg_width=2;

    if (ldebug)
        logWrite("# Set trigger output config: threshold %d, invert %d, width %d = %0.1f ns\n",
            trg_thresh, trg_inv, trg_width, TRG_CLK_PERIOD_NS*trg_width);

    trg_width--; // program width-1, but 0 is not allowed
    w = trg_inv;
    w <<= TRG_THR_BITS;
    w |= trg_thresh;
    w <<= TRG_TWD_BITS;
    w |= trg_width;
    int ret = 0;
    ret = m_uart32->WriteWord(OFFS_TRG_INV_THR_WIDTH+base_addr_trg, w );
    return ret;
}

std::tuple<uint8_t, uint8_t, uint8_t> C_trigger::get_trg_thresh_inv_width()
{
    uint32_t trg_config[1], w;
    m_uart32->RecvBurst(1, 1, 1, base_addr_trg + OFFS_TRG_INV_THR_WIDTH, trg_config );
    w = trg_config[0];
    uint8_t trg_width = w & TRG_TWD_MASK;
    w >>= TRG_TWD_BITS;
    trg_width++;
    uint8_t trg_thresh = w & TRG_THR_MASK;
    w >>= TRG_THR_BITS;
    uint8_t trg_inv = w & 1;
    printf("TRG:  inv %d    thres %d    width %d\n", trg_inv, trg_thresh, trg_width);
    return std::make_tuple(trg_inv, trg_thresh, trg_width);
}

// set the dead time after a fired trigger
int C_trigger::set_trg_dtime(uint16_t trg_dtime, uint8_t ldebug)
{
    trg_dtime &= TRG_DTM_MASK;
    if (trg_dtime < 2) trg_dtime++;

    if (ldebug)
    {
        if (trg_dtime > 1)
            logWrite("# Set trigger dead time to %d clocks = %0.3f us\n",
                trg_dtime, TRG_CLK_PERIOD_NS*trg_dtime/1000);
        else
            logWrite("# Trigger deat time disabled\n");
    }

    trg_dtime--; // program width-1, but 0 is not allowed

    int ret = m_uart32->WriteWord(OFFS_TRG_DTIME+base_addr_trg, trg_dtime );
    return ret;
}

uint16_t C_trigger::get_trg_dtime()
{
    uint32_t trg_config[1], w;
    m_uart32->RecvBurst(1, 1, 1, base_addr_trg + OFFS_TRG_DTIME, trg_config );
    w = trg_config[0];
    uint16_t trg_dtime = w;
    trg_dtime++;
    printf("Dead time: %d\n", trg_dtime);
    return trg_dtime;
}

int C_trigger::set_pp_time(uint8_t ch_mask, uint16_t pp_time, uint8_t ldebug)
{
    int ch;
    int ret = 0;

    pp_time &= TRG_DTM_MASK;
    ch_mask &= TRG_CH_MASK;

    if (pp_time < 2) pp_time++;

    if (ldebug)
    {
        if (pp_time > 1)
            logWrite("# Set past protection time to %d clocks = %0.3f us on the channels in mask 0x%x\n",
                pp_time, TRG_CLK_PERIOD_NS*pp_time/1000, ch_mask);
        else
            logWrite("# Past Protection on the channels in mask 0x%x disabled\n", ch_mask);
    }
    pp_time--;
    
    for (ch=0; ch<TRG_NCH; ch++)
    {
        if (ch_mask & 1)
            ret = m_uart32->WriteWord(OFFS_PP_CHX(ch)+base_addr_trg, pp_time );

        ch_mask >>= 1;
    }
    return ret;
}

std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> C_trigger::get_pp_time()
{
    uint32_t trg_config[TRG_NCH], w;
    m_uart32->RecvBurst(4, 1, 1, base_addr_trg + OFFS_PP_CHX(0), trg_config );

    uint16_t pp_time[TRG_NCH];
    for (int i=0; i <TRG_NCH; i++)
    {
        pp_time[i] = trg_config[i];
        pp_time[i]++;
    }
    
    printf("PP time: ch0 %d,    ch1 %d,    ch2 %d,    ch3 %d\n", pp_time[0], pp_time[1], pp_time[2], pp_time[3]);
    return std::make_tuple(pp_time[0], pp_time[1], pp_time[2], pp_time[3]);
}


int C_trigger::print_trg_config()
{
    int i;
    uint32_t trg_config[MAX_REG_TRG_CNF+1], w, dgg_del;

    m_uart32->RecvBurst(MAX_REG_TRG_CNF+1, 1, 1, base_addr_trg, trg_config );

    w = trg_config[OFFS_TRG_INP_INV_ENA];
    logWrite("# Got: enable/invert busy %d/%d, invert inputs mask 0x%x, enable inputs mask 0x%x\n",
            (w >> ((2*TRG_NCH)+1)) & 1,
            (w >>  (2*TRG_NCH)   ) & 1,
            (w >>    TRG_NCH)  & TRG_CH_MASK,
             w                 & TRG_CH_MASK);

    w = trg_config[OFFS_TRG_DGG_WIDTH_DEL];

    logWrite("# Got delay gate generators at the inputs: DELAYs 0..3: ");

    for (i=0; i<TRG_NCH; i++)
    {
        dgg_del = w & TRG_DGG_MASK;
        logWrite("%d = %0.1f ns, ", dgg_del, TRG_CLK_PERIOD_NS*dgg_del );
        w >>= TRG_DGG_BITS;
    }
    w++;
    logWrite("GATE %d = %0.1f ns\n", w, TRG_CLK_PERIOD_NS*w);

    w = trg_config[OFFS_TRG_INV_THR_WIDTH];
    logWrite("# Got trigger output config: threshold %d, invert %d, width %d = %0.1f ns\n",
            (w >> TRG_TWD_BITS) & TRG_THR_MASK,
            (w >> (TRG_TWD_BITS + TRG_THR_BITS) ) & 1,
            (w & TRG_TWD_MASK)+1, TRG_CLK_PERIOD_NS*( (w & TRG_TWD_MASK) + 1));

    w = trg_config[OFFS_TRG_DTIME] + 1;

    logWrite("# Got trigger dead time =  %d clocks = %0.3f us\n",
            w, TRG_CLK_PERIOD_NS*w/1000);

    for (i=0; i<TRG_NCH; i++)
    {
        w = trg_config[OFFS_PP_CHX(i)];
        if (w==0)
            logWrite("# Past Protection in channel %d disabled\n", i);
        else
        {
            w++;
            logWrite("# Got past protection time of ch%d =  %d clocks = %7.3f us\n", i,
                    w, TRG_CLK_PERIOD_NS*w/1000);
        }
    }
    print_cmp_inp_out(trg_config[OFFS_INP_FR_CMP]);
    return 0;
}

int C_trigger::set_tg_masks(uint32_t tg_mask0, uint32_t tg_mask1, uint8_t ldebug)
{
    uint32_t masks[2];
    masks[0] = tg_mask0;
    masks[1] = tg_mask1;
    if (ldebug)
    {
        logWrite("# Set test pattern masks in the test generator to 0x%08x and 0x%08x\n", tg_mask0, tg_mask1);
        logWrite("# First pulse\n");
        print_tg_mask(tg_mask0);
        logWrite("# Second pulse\n");
        print_tg_mask(tg_mask1);
    }
    return m_uart32->SendBurst(2, 1, 1, base_addr_trg + OFFS_TG_MSK0, masks );

}

int C_trigger::print_tg_mask(uint32_t tg_mask)
{
    int i, j;
    for (i=0; i<TRG_NCH; i++)
    {
        logWrite("CH %d _", i);
        for (j=SEQ_LEN-1; j>=0; j--)
            if ((tg_mask >> j) & 1) logWrite("-");
            else                    logWrite("_");
        logWrite("_\n");
        tg_mask >>= 8;
    }
    logWrite("\n");
    return 0;
}

int C_trigger::set_tg_distance_period(uint32_t tg_dist, uint32_t tg_period, uint8_t ldebug)
{
    int ret = 0;
    if (ldebug) logWrite("# Set period to %d or %0.3f us or %0.3f kHz, distance between the two pulses %d or %0.3f us\n",
        tg_period, TRG_CLK_PERIOD_US*tg_period, 1.0/(TRG_CLK_PERIOD_MS*tg_period), tg_dist, TRG_CLK_PERIOD_US*tg_dist);
    ret = WriteReg(4, OFFS_TG_DIST + base_addr_trg, tg_dist);
    ret = WriteReg(4, OFFS_TG_PER  + base_addr_trg, tg_period);
    return ret;
}

int C_trigger::set_tg_repetitions(uint32_t tg_rep, uint8_t tg_start, uint8_t tg_use, uint8_t ldebug)
{
    uint32_t w, mr;

    tg_start &= 1;
    tg_use &= 1;
    mr = 1;
    mr <<= BITS_REPETITIONS;
    mr--; // mask for the repetitions and the max value

    tg_rep &= mr;

    w = tg_start;
    w <<= 1;
    w |= tg_use;
    w <<= BIT_TG_REP_USE_TG;
    w |= tg_rep;

    if (ldebug)
    {
        if (tg_rep == mr)
            logWrite("# Max number of repetitions set => infinite loop");
        else
            logWrite("# Set repetitions to %d", tg_rep);

        logWrite(", start %d, use test generator %d\n", tg_start, tg_use);
    }
    return m_uart32->WriteDWord(OFFS_TG_REP+base_addr_trg, w );
}

int C_trigger::set_cmp_inp_out(uint8_t tg_inp_from_io, uint8_t cmp_out2io, uint8_t ldebug)
{
    uint32_t w;

    cmp_out2io &= TRG_CH_MASK;
    tg_inp_from_io &= TRG_CH_MASK;
    w = cmp_out2io;
    w <<= TRG_NCH;
    w |= tg_inp_from_io;
    if (ldebug) print_cmp_inp_out(w);

    return m_uart32->WriteDWord(OFFS_INP_FR_CMP+base_addr_trg, w );
}

int C_trigger::print_cmp_inp_out(uint8_t tg_inp_cmp_ena)
{
    int i;
    uint8_t tg_inp_from_io, cmp_out2io;

    tg_inp_from_io =  tg_inp_cmp_ena             & TRG_CH_MASK;
    cmp_out2io     = (tg_inp_cmp_ena >> TRG_NCH) & TRG_CH_MASK;

    for (i=0; i<TRG_NCH; i++)
    {
        logWrite("# Channel %d, trigger input is from ", i);
        if ( (tg_inp_from_io >> i) & 1)
            logWrite("I/O");
        else
            logWrite("CMP");

        logWrite(", comparator output to I/O is ");
        if ( (cmp_out2io >> i) & 1)
            logWrite("enabled.\n");
        else
            logWrite("disabled.\n");
    }
    return 0;
}

//int C_trigger::set_tg_repetitions(uint32_t tg_rep, uint8_t ldebug)
//{
//    return set_tg_repetitions(tg_rep, 1, 1, ldebug);
//}

int C_trigger::tg_disable(uint8_t ldebug)
{
    if (ldebug) logWrite("# Disable the test generator\n");
    return set_tg_repetitions(0, 0, 0, 0);
}


int C_trigger::set_trg_cnt(uint32_t trg_cnt_presc, uint32_t trg_cnt_endtm, uint8_t trg_cnt_cmd, uint8_t ldebug)
{
    uint32_t w[4];

    trg_cnt_presc &= (1L << BITS_TRG_PRESCALE) - 1;
    w[0] = trg_cnt_presc;
    w[1] = trg_cnt_endtm;
    w[0]--;
    w[1]--;
    w[2] = trg_cnt_cmd;
    if (ldebug)
    {
        logWrite("# Set prescale counter to %d, tick period is %0.3f ms, tick frequency is %0.3f kHz\n",
            trg_cnt_presc, TRG_CLK_PERIOD_MS*trg_cnt_presc, 1.0/(TRG_CLK_PERIOD_MS*trg_cnt_presc) );
        logWrite("# Set endtime to %0.3f ms or %0.3f s\n",
            TRG_CLK_PERIOD_MS*trg_cnt_endtm*trg_cnt_presc, 0.001*TRG_CLK_PERIOD_MS*trg_cnt_endtm*trg_cnt_presc );
        print_trg_cnt_cmd(trg_cnt_cmd);
    }
    return m_uart32->SendBurst(3, 1, 1, base_addr_trg + OFFS_TRG_PRESCALE, w );
}

int C_trigger::print_trg_cnt_cmd(uint8_t trg_cnt_cmd)
{
    logWrite("# Counter command ");
    if ((trg_cnt_cmd >> BIT_TRG_CNT_CLR  ) & 1) logWrite(" CLR");
    if ((trg_cnt_cmd >> BIT_TRG_CNT_START) & 1) logWrite(" START/RUN");
    if ((trg_cnt_cmd >> BIT_TRG_CNT_LATCH) & 1) logWrite(" LATCH/NEW");
    if ((trg_cnt_cmd >> BIT_TRG_CNT_STOP ) & 1) logWrite(" STOP");
    if ((trg_cnt_cmd >> BIT_TRG_CNT_CONT ) & 1) logWrite(" CONT");
    logWrite("\n");

    return 0;
}

int C_trigger::trg_cnt_cmd(uint8_t trg_cnt_cmd, uint8_t ldebug)
{
    if (ldebug) print_trg_cnt_cmd(trg_cnt_cmd);
    m_uart32->WriteByte(OFFS_TRG_CMD_STA+base_addr_trg, trg_cnt_cmd );
    return 0;
}

std::array<uint32_t, 16> C_trigger::get_trg_cnt()
{
    uint32_t w[2*TRG_NCH+4+4], i, sta, cont, tt;
    bool wait_new = true;

    if (wait_new)
    {
        do
        {
            sta = ReadReg(1, OFFS_TRG_CMD_STA + base_addr_trg);
        } while ( ( (sta >> BIT_TRG_CNT_NEW) & 1) == 0);
    }
    else
    {
        trg_cnt_cmd(1 << BIT_TRG_CNT_LATCH);
    }
    // the configuration of the counters is in 4 addresses, mapped many times in the lower part of the address space
    // therefore starting at OFFS_TRG_COUNTERS - 4 we read the counters config & status
    // counter 0..3 just count in channels 0..3
    // counters 4..7 count when the past protection disables the corresponding channel
    // counters 8..11 count the special cases, see below
    m_uart32->RecvBurst(2*TRG_NCH+4+4, 1, 1, base_addr_trg + OFFS_TRG_COUNTERS - 4, w );

    cont = (w[2] >> BIT_TRG_CNT_CONT) & 1;

    logWrite("# Timer prescaler %d, trigger clock %d\n", w[0], TRIGGER_CLOCK);
    logWrite("# Counters status 0x%x => running %d, continuous %d, new %d, periods %d\n",
        w[2], (w[2] >> BIT_TRG_CNT_RUN) & 1, cont, (w[2] >> BIT_TRG_CNT_NEW) & 1, w[2] >> BIT_TRG_CNT_PRDS);

    w[0]++;
    w[1]++;
    if (cont == 0) // in this case is the time, otherwise mask with non-zero counters
        w[3]++;

    if (cont)
    {
        logWrite("# Mask with non-zero counters = 0x%03x\n", w[3] );
        tt = w[1];
    }
    else
    {
        logWrite("# Timer %d = 0x%08x\n", w[3], w[3] );
        logWrite("# Counters timer at %0.3f of %0.3f s\n", 0.001*TRG_CLK_PERIOD_MS*w[3]*w[0] , 0.001*TRG_CLK_PERIOD_MS*w[1]*w[0] );
        tt = w[3];
    }

    logWrite("# Counters 0..3, 4..7 past protection on channels 0..3, 8-trigger, 9-trigger-no-busy-dead_time, 10-trigger-busy-no-dead_time, 11-trigger-busy-dead_time\n");

    for (i=0; i< (2*TRG_NCH+4) ; i++)
        logWrite("# %2d   %10d   %10.3f cnts/s\n", i, w[4+i], 1.0*w[4+i]/ ( 0.001*TRG_CLK_PERIOD_MS*tt*w[0] ) );

    // clear the new flag
    WriteReg(1, OFFS_TRG_COUNTERS + base_addr_trg, 0);

    if (cont == 0) // restart
        trg_cnt_cmd( (1 << BIT_TRG_CNT_CLR) | (1 << BIT_TRG_CNT_START), 0);

    std::array<uint32_t, 16> counter_array;// = {w[4], w[5], w[6], w[7], w[8], w[9], w[10], w[11], w[12], w[13], w[14], w[15]};
    std::copy(std::begin(w), std::end(w), std::begin(counter_array));
    return counter_array;
}

int C_trigger::print_trg_cnt(int wait_new)
{
    uint32_t w[2*TRG_NCH+4+4], i, sta, cont, tt;


    if (wait_new)
    {
        do
        {
            sta = ReadReg(1, OFFS_TRG_CMD_STA + base_addr_trg);
        } while ( ( (sta >> BIT_TRG_CNT_NEW) & 1) == 0);
    }
    else
    {
        trg_cnt_cmd(1 << BIT_TRG_CNT_LATCH);
    }
    // the configuration of the counters is in 4 addresses, mapped many times in the lower part of the address space
    // therefore starting at OFFS_TRG_COUNTERS - 4 we read the counters config & status
    // counter 0..3 just count in channels 0..3
    // counters 4..7 count when the past protection disables the corresponding channel
    // counters 8..11 count the special cases, see below
    m_uart32->RecvBurst(2*TRG_NCH+4+4, 1, 1, base_addr_trg + OFFS_TRG_COUNTERS - 4, w );

    cont = (w[2] >> BIT_TRG_CNT_CONT) & 1;

    logWrite("# Timer prescaler %d, trigger clock %d\n", w[0], TRIGGER_CLOCK);
    logWrite("# Counters status 0x%x => running %d, continuous %d, new %d, periods %d\n",
        w[2], (w[2] >> BIT_TRG_CNT_RUN) & 1, cont, (w[2] >> BIT_TRG_CNT_NEW) & 1, w[2] >> BIT_TRG_CNT_PRDS);

    w[0]++;
    w[1]++;
    if (cont == 0) // in this case is the time, otherwise mask with non-zero counters
        w[3]++;

    if (cont)
    {
        logWrite("# Mask with non-zero counters = 0x%03x\n", w[3] );
        tt = w[1];
    }
    else
    {
        logWrite("# Timer %d = 0x%08x\n", w[3], w[3] );
        logWrite("# Counters timer at %0.3f of %0.3f s\n", 0.001*TRG_CLK_PERIOD_MS*w[3]*w[0] , 0.001*TRG_CLK_PERIOD_MS*w[1]*w[0] );
        tt = w[3];
    }

    logWrite("# Counters 0..3, 4..7 past protection on channels 0..3, 8-trigger, 9-trigger-no-busy-dead_time, 10-trigger-busy-no-dead_time, 11-trigger-busy-dead_time\n");

    for (i=0; i< (2*TRG_NCH+4) ; i++)
        logWrite("# %2d   %10d   %10.3f cnts/s\n", i, w[4+i], 1.0*w[4+i]/ ( 0.001*TRG_CLK_PERIOD_MS*tt*w[0] ) );

    // clear the new flag
    WriteReg(1, OFFS_TRG_COUNTERS + base_addr_trg, 0);

    if (cont == 0) // restart
        trg_cnt_cmd( (1 << BIT_TRG_CNT_CLR) | (1 << BIT_TRG_CNT_START), 0);

    return (w[2] >> BIT_TRG_CNT_RUN) & 1;
}

uint8_t  C_trigger::WriteReg(uint8_t len, uint32_t addr, uint32_t wdata)
{
    uint8_t ret;
    switch (len)
    {
        case 1: { ret = m_uart32->WriteByte(  addr, wdata &     0xFF); break; }
        case 2: { ret = m_uart32->WriteWord(  addr, wdata &   0xFFFF); break; }
        case 3: { ret = m_uart32->Write3Bytes(addr, wdata & 0xFFFFFF); break; }
        case 4: { ret = m_uart32->WriteDWord( addr, wdata           ); break; }
        default:
        {
            printf("Illegal register length %d (must be from 1 to 4)!\n", len);
            return 1;
        }
    }
    return ret;
}

uint32_t  C_trigger::ReadReg(uint8_t len, uint32_t addr)
{
    switch (len)
    {
        case 1: return m_uart32->ReadByte(  addr);
        case 2: return m_uart32->ReadWord(  addr); //, WSIZE_BYTE);
        case 3: return m_uart32->Read3Bytes(addr); //, WSIZE_BYTE);
        case 4: return m_uart32->ReadDWord( addr); //, WSIZE_BYTE);
        default:
        {
            printf("Illegal register length %d (must be from 1 to 4)!\n", len);
            return 0;
        }
    }
    return 0;
}

uint32_t C_trigger::read_imem_file(FILE *f, uint32_t *prog_data, uint32_t max_length)
{
    char linebuf[128];
    uint32_t dat;
    int  args;
    uint32_t nwords, nwords0;

    nwords = 0;
    nwords0 = 0;
    while ( (fgets(linebuf, 128, f)) && (nwords < max_length) )
    {
        args = sscanf(linebuf, "%x", &dat);
        if (args >= 1)
        {
            *prog_data = dat;
            if (dat != 0) nwords0=0; else nwords0++;
            prog_data++;
            nwords++;
        }
    }
    logWrite("# Size of the block with 0x00000000 at the end of the IMEM %d of all %d words, max is %d\n", nwords0, nwords, max_length);
    return nwords;
}

int32_t C_trigger::set_sh_mem(uint32_t msize, uint32_t baddr, uint32_t *sh_mem, uint8_t ainc)
{
    uint16_t csize;

    do
    {
        if (msize > BLOCK_MAX_SIZE)
            csize = BLOCK_MAX_SIZE;
        else
            csize = msize;
        m_uart32->SendBurst(csize, ainc, 1, baddr, sh_mem);
        msize -= csize;
        if (ainc)
            baddr += csize;
        sh_mem+= csize;
    } while (msize > 0);
    return 0;
}

// get the logic analyzer data, still don't check & wait until some event stored!
uint32_t C_trigger::get_la()
{
    uint16_t la_data[LA_SAMPLES];
    uint32_t timer, smpl, bit, i;

    timer  = ReadReg(4, base_addr_trg + OFFS_LA_TIMESTA);
    logWrite("# Timestamp in %d ns %u = %0.3f s\n", TIME_RESOL, timer, (timer*1e-9)*TIME_RESOL );
    rep_la();
    logWrite("# Time[ns] | Ch0..3 | inp edge | DGG-out | VETO  | Trg-Start | Trg-Out |\n");

    m_uart32->RecvBurst(LA_SAMPLES, 1, 1, base_addr_trg + OFFS_TRG_LA, la_data);

    for (i=0; i<LA_SAMPLES; i++)
    {
        logWrite("%4d ", i);
        smpl = la_data[i];
        for (bit=0; bit < LA_SIGNALS; bit++)
        {
            logWrite(" %d", smpl & 1);
            if ((bit & 3) == 3)
            logWrite("  ");
            smpl >>= 1;
        }
        logWrite("\n");
    }
    WriteReg(1, base_addr_trg + OFFS_LA_TIMESTA, LA_CMD_CLR);
    rep_la();
    return 0;
}

// get the logic analyzer data, still don't check & wait until some event stored!
void C_trigger::set_la_pres(uint32_t la_pres)
{
    WriteReg(2, base_addr_trg + OFFS_LA_PRESAMPL, la_pres);
    return;
}

uint32_t C_trigger::rep_la()
{
    uint32_t status, presampl;

    status = ReadReg(4, base_addr_trg + OFFS_LA_PRESAMPL);
    presampl = status & 0xFFF;
    status >>= 16;

    logWrite("# Presamples %d\n", presampl );
    logWrite("# Max number of samples %d, timestep %d ns\n", 2 << (status & 0xF), TIME_RESOL );
    logWrite("# State of the LA %d (1 - idle, 2 - triggered, 4 - ready, 8 - restarted)\n", status >> 4);

    return status;
}


void C_trigger::trg_cpu_run(uint32_t trg_cmd, uint32_t nsamples, uint32_t cnt_mask)
{
    if ((trg_cmd & (1 << CPU_TASK_BIT_STOP)) == 0)
    {
        if (nsamples > LA_SAMPLES) nsamples = LA_SAMPLES;
        WriteReg(4, ADDR_CPU_NSAMPLES, nsamples);
        WriteReg(4, ADDR_CPU_CNT_MASK, cnt_mask);
    }
    WriteReg(4, ADDR_CPU_TASK, trg_cmd);
    return;
}

// some functions from class: one_max10_fpga8_32
uint32_t C_trigger::read_short_trace_id()
{
    int i;
    uint32_t trace_id;
    uint8_t  trid[8];

    m_uart32->RecvBurst(8, 1, 1, AV_CHIP_ID, trid);

//  printf("# Reading from 0x%08x ", AV_CHIP_ID);
//  for (i=0; i<8; i++) printf(" %02x", trid[i]);
//  printf("\n");

    for (i=3; i>=0; i--)
    {
        trace_id <<= 8;
        trace_id |= trid[i] ^ trid[i+4];
    }
    return trace_id;
}

int C_trigger::get_ser_nr(FILE *f)
{
    uint32_t trace_id, i;

    trace_id = read_short_trace_id();
    fprintf(f, "# Short trace id is 0x%08x, ", trace_id);
    i = NUMBER_OF_BOARDS;
    while ( (i > 0) && (all_board_ids[i].fpga_id != trace_id) )
    {
        i--;
    }
    if (i > 0)
        fprintf(f, "Ser. Nr. is %2d => D27-%d\n", all_board_ids[i].ser_nr, all_board_ids[i].doc_version);
    else
        fprintf(f, "Ser. Nr. is unknown!\n");
    return all_board_ids[i].ser_nr;
}
