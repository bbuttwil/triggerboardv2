import sys
import os
import time
#sys.path.append(os.getcwd())    #This line ads the current folder to the path, making triggerboard and its shared C libraries available to python
#sys.path.append(os.getcwd()+ '/../C')
import triggerboard

uart32 = triggerboard.uart32("/dev/triggerDev0", 4000000)
uart32.set_slave_mask(1)
uart32.set_max_wsize(32)
uart32.set_max_asize(24)
trigger = triggerboard.trigger(uart32, 0x000)
i2c = triggerboard.i2c(uart32, 0x0200)

#
trigger.set_inv_ena(1, 0, 0b1111, 0b1111, 0)
trigger.set_dgg_width_delay(10, 1, 2, 3, 4, 0)
print(f"Got back: {trigger.set_trg_thresh_inv_width(0, 1, 20, 0)}")
trigger.set_trg_dtime(12345, 0)
trigger.set_pp_time(0b1111, 5, 0)
trigger.set_trg_cnt(200000, 1000, 1, 0)
trigger.set_tg_masks(0x7e7e, 0x7e7e, 0)
#trigger.print_tg_mask(0x0F00FF00)
trigger.set_tg_distance_period(500, 20000, 0)
trigger.set_tg_repetitions(10, 1, 1, 0)
trigger.print_trg_config()
#trigger.print_trg_cnt(1)
#trigger.tg_disable(0)

trigger.get_trg_dtime()
trigger.get_pp_time()
