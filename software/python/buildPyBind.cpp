#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "../C/C_trigger.cpp"
#include "../C/i2c.cpp"

//1. Make command in cpp_src folder, create shared library (*.so) files for the linker to be used in next step
//2.Copy over the generated .so files into the /usr/lib folder. Or modify the LD_LIBRARY_PATH variable
//2. Compile this file using 
/*
g++ -O3 -Wall -shared -std=c++11 -fPIC $(python3 -m pybind11 --includes) buildPyBind.cpp -o triggerboard$(python3-config --extension-suffix) -L/home/bent/work/triggerboardDev/v2/trigPyAPI/C_lib_wrapper_boost/cpp_src -lC_trigger -li2c
*/


namespace py = pybind11;


PYBIND11_MODULE(triggerboard, m) {
    m.doc() = "pybind11 triggerboard wrapper"; // optional module docstring
    py::class_<uart32>(m, "uart32")
        .def(py::init<std::string, uint32_t>())
        .def("set_slave_mask", &uart32::set_slave_mask)
        .def("set_max_wsize", &uart32::set_max_wsize)
        .def("set_max_asize", &uart32::set_max_asize);
    py::class_<C_i2c>(m, "i2c")
        .def(py::init<uart32*, uint32_t>())
        .def("spi_wr", &C_i2c::spi_wr)
        .def("last_dac_rep", &C_i2c::last_dac_rep)
        .def("last_dac_rep_values", &C_i2c::last_dac_rep_values)
        .def("ad5624_thr2dac", &C_i2c::ad5624_thr2dac);
    py::class_<C_trigger>(m, "trigger")
        .def(py::init<uart32*, uint32_t>())
        .def("print_trg_config", &C_trigger::print_trg_config)
        .def("set_inv_ena", &C_trigger::set_inv_ena)
        .def("get_inv_ena", &C_trigger::get_inv_ena)
        .def("set_dgg_width_delay", &C_trigger::set_dgg_width_delay)
        .def("get_dgg_width_delay", &C_trigger::get_dgg_width_delay)
        .def("set_trg_thresh_inv_width", &C_trigger::set_trg_thresh_inv_width)
        .def("get_trg_thresh_inv_width", &C_trigger::get_trg_thresh_inv_width)
        .def("set_trg_dtime", &C_trigger::set_trg_dtime)
        .def("get_trg_dtime", &C_trigger::get_trg_dtime)
        .def("set_pp_time", &C_trigger::set_pp_time)
        .def("get_pp_time", &C_trigger::get_pp_time)
        .def("print_tg_mask", &C_trigger::print_tg_mask)
        .def("set_tg_masks", &C_trigger::set_tg_masks)
        .def("set_tg_distance_period", &C_trigger::set_tg_distance_period)
        .def("set_tg_repetitions", &C_trigger::set_tg_repetitions)
        .def("tg_disable", &C_trigger::tg_disable)
        .def("set_trg_cnt", &C_trigger::set_trg_cnt)
        .def("trg_cnt_cmd", &C_trigger::trg_cnt_cmd)
        .def("print_trg_cnt", &C_trigger::print_trg_cnt)
        .def("get_trg_cnt", &C_trigger::get_trg_cnt);
        
        //.def("print_fstatus", &C_trigger::print_fstatus);
}
