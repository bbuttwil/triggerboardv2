import sys
import os
sys.path.append(os.getcwd())    #This line ads the current folder to the path, making triggerboard and its shared C libraries available to python
import triggerboard

uart32 = triggerboard.uart32("/dev/triggerDev0", 4000000)
uart32.set_slave_mask(1)
uart32.set_max_wsize(32)
uart32.set_max_asize(24)
trigger = triggerboard.trigger(uart32, 0x000)
i2c = triggerboard.i2c(uart32, 0x0200)

i2c.last_dac_rep()
i2c.spi_wr(0,10,0)
i2c.last_dac_rep()