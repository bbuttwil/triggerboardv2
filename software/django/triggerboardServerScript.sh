#!/bin/bash
session_name=triggerS

# Check if the tmux session already exists
/usr/bin/tmux has-session -t $session_name 2>/dev/null

if [ $? != 0 ]; then
  # Create a new tmux session with two windows
  /usr/bin/tmux new-session -d -s $session_name -n server 'python3 triggerboard_django/manage.py runserver 0.0.0.0:8000'
  /usr/bin/tmux split-window -t $session_name:0 -h 'cd triggerboard_django && celery -A triggerboard_django worker -l INFO'
  /usr/bin/tmux select-pane -t $session_name:0.0
  /usr/bin/tmux set-window-option remain-on-exit on
fi

# Attach to the tmux session
#tmux attach-session -t $session_name
