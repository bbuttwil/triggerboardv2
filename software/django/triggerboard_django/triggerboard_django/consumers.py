from channels.generic.websocket import AsyncWebsocketConsumer
import json

'''
This python file is responsible for the websocket communication of this django webinterface.
'''

class ConfigUpdateConsumer(AsyncWebsocketConsumer):
    '''
    This class represents the server websocket for the TriggerConfig updates.
    '''
    async def connect(self):
        # Add this channel to the 'updates' group
        await self.channel_layer.group_add(
            'updates',
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Remove this channel from the 'updates' group
        await self.channel_layer.group_discard(
            'updates',
            self.channel_name
        )
        pass

    async def receive(self, text_data):
        pass
    
    async def update_message(self, event):
        '''
        This function sends new data from the TriggerboardConfig to the client via websockets.
        
        It is called from the "webinterface/models.py" file when a new entry for the TriggerboardConfig gets saved to the database. The name of this function is important,
        it is expected to be update_message by the model updating function when a new entry is saved.
        '''
        data = event['data']

        # Send the update message and data to the WebSocket
        await self.send(text_data=data)





class CounterUpdateConsumer(AsyncWebsocketConsumer):
    '''
    This class represents the server websocket for the TriggerCounter updates.
    '''
    async def connect(self):
        # Add this channel to the 'counter_updates_channel' group
        await self.channel_layer.group_add(
            'counter_updates_channel',
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Remove this channel from the 'counter_updates_channel' group
        await self.channel_layer.group_discard(
            'counter_updates_channel',
            self.channel_name
        )
        pass

    async def receive(self, text_data):
        pass
    
    async def send_newdata(self, event):
        await self.send(text_data=json.dumps(event['data']))





class CounterTaskNewConsumer(AsyncWebsocketConsumer):
    '''
    This class represents the server websocket for the TriggerCounterTask updates.
    '''
    async def connect(self):
        # Add this channel to the 'counterTask_updates_channel' group
        await self.channel_layer.group_add(
            'counterTask_updates_channel',
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Remove this channel from the 'counterTask_updates_channel' group
        await self.channel_layer.group_discard(
            'counterTask_updates_channel',
            self.channel_name
        )
        pass

    async def receive(self, text_data):
        pass
    
    async def update_newdata(self, event):
        await self.send(text_data=json.dumps(event['data']))