import os

from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'triggerboard_django.settings')

app = Celery('triggerboard_django', include=['webinterface.tasks'])

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

#Uncomment if you want to make the tasks as beat again
#app.conf.beat_schedule = {
#    'trigger-conf-task': {
#        'task': 'webinterface.tasks.trigger_conf_beat',
#        'schedule': 60.0,
#    },
#    'trigger-count-task': {
#        'task': 'webinterface.tasks.trigger_count_beat',
#        'schedule': 1.0,
#    }
#}

