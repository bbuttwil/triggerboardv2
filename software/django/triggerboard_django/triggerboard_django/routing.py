from django.urls import path
from . import consumers

'''Here the url routes for the websockets are linked to the "consumer.py" file'''

websocket_urlpatterns = [
    path('ws/config_updates/', consumers.ConfigUpdateConsumer.as_asgi()),
    path('ws/counter_updates/', consumers.CounterUpdateConsumer.as_asgi()),
    path('ws/counterTask_new/', consumers.CounterTaskNewConsumer.as_asgi()),
]