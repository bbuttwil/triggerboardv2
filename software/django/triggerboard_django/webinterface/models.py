from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.forms.models import model_to_dict

import json

# Create your models here.

class TriggerConfig(models.Model):
    '''
    This model representation is to store configs of the triggerboard inside the database.
    '''
    inv_busy = models.BooleanField("Busy inverted")
    inv_inputs_mask =models.PositiveSmallIntegerField("Inverted channel mask")
    enable_busy = models.BooleanField("Busy enabled")
    enable_inputs_mask = models.PositiveSmallIntegerField("Enabled channel mask")
    dgg_width = models.PositiveSmallIntegerField("Gate width")
    dgg_delay0 = models.PositiveSmallIntegerField("Delay channel 0")
    dgg_delay1 = models.PositiveSmallIntegerField("Delay channel 1")
    dgg_delay2 = models.PositiveSmallIntegerField("Delay channel 2")
    dgg_delay3 = models.PositiveSmallIntegerField("Delay channel 3")
    trg_inv = models.BooleanField("Trigger out inverted")
    trg_thresh = models.PositiveSmallIntegerField("Trigger out threshold")
    trg_width = models.PositiveSmallIntegerField("Trigger out width")
    trg_dtime = models.PositiveSmallIntegerField("Trigger out dead time")
    pp_time0 = models.PositiveSmallIntegerField("PP channel 0")
    pp_time1 = models.PositiveSmallIntegerField("PP channel 1")
    pp_time2 = models.PositiveSmallIntegerField("PP channel 2")
    pp_time3 = models.PositiveSmallIntegerField("PP channel 3")
    
    thres_ch0 = models.PositiveSmallIntegerField("Threshold dac value channel 0")
    thres_ch1 = models.PositiveSmallIntegerField("Threshold dac value channel 1")
    thres_ch2 = models.PositiveSmallIntegerField("Threshold dac value channel 2")
    thres_ch3 = models.PositiveSmallIntegerField("Threshold dac value channel 3")
    
    def to_json(self):
        data = model_to_dict(self)
        return json.dumps(data)
        

@receiver(post_save, sender=TriggerConfig)
def send_update(sender, **kwargs):
    new_entry = kwargs['instance']
    data_json = new_entry.to_json()
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)('updates', {
        'type': 'update.message',
        'text': 'A new entry was added to TriggerConfig',
        'data': data_json,
    })
    

class TriggerCounter(models.Model):
    '''
    In this model the counter numbers are stored in this database is during beam operation is cyclically the counter values written
    '''
    timestamp = models.DateTimeField(auto_now_add=True)
    
    counter_prescale = models.PositiveIntegerField("Counter prescale")
    counter_endtime = models.PositiveIntegerField("Counter endtime")
    counter_status = models.PositiveSmallIntegerField("Counter status bit, 4bits")
    counter_timer = models.PositiveIntegerField("Current counter timer")
    
    ch0_counter = models.PositiveIntegerField("Counter for CH0")
    ch1_counter = models.PositiveIntegerField("Counter for CH1")
    ch2_counter = models.PositiveIntegerField("Counter for CH2")
    ch3_counter = models.PositiveIntegerField("Counter for CH3")
    
    ch0_pp_counter = models.PositiveIntegerField("PP counter for CH0")
    ch1_pp_counter = models.PositiveIntegerField("PP counter for CH1")
    ch2_pp_counter = models.PositiveIntegerField("PP counter for CH2")
    ch3_pp_counter = models.PositiveIntegerField("PP counter for CH3")
    
    trigger_counter = models.PositiveIntegerField("Counter for successfull Trigger")
    trigger_nB_DT = models.PositiveIntegerField("Counter for rejected Trigger due to dead time")
    trigger_B_nDT = models.PositiveIntegerField("Counter for rejected Trigger due to busy")
    trigger_B_DT = models.PositiveIntegerField("Counter for rejected Trigger due to busy and dead time")
    
    

    def to_dict_custom(self):
        opts = self._meta
        data = {}
        from itertools import chain
        from datetime import datetime
        for f in chain(opts.concrete_fields, opts.private_fields):
            #It surprisingly needs to handle DateTimeFields differently by converting it to and DateTime first, wow!
            if not isinstance(f, models.fields.DateTimeField):
                data[f.name] = f.value_from_object(self)
            else:
                time = datetime.fromisoformat(str(f.value_from_object(self)))
                data[f.name] = time.isoformat()
        #for f in opts.many_to_many:
        #    data[f.name] = [i.id for i in f.value_from_object(self)]
        return data

    
@receiver(post_save, sender=TriggerCounter)
def send_update(sender, **kwargs):
    '''
    This function gets called when a new entry for the TriggerCounter is saved to the database.
    '''
    new_entry = kwargs['instance']
    data_dict = new_entry.to_dict_custom()
    #print(data_dict)
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)('counter_updates_channel', {
        'type': 'send.newdata',     #This value "send.newdata" is the new of the function to be called on the websocket send interface. There its called "send_newdata"
        'text': 'A new entry was added to TriggerCounter',
        'data': data_dict,
    })

class CounterTask(models.Model):
    '''
    This model saves the task id for the celery task of TriggerCounter.
    '''
    task_id = models.CharField(max_length=255)
    
    def to_json(self):
        data = model_to_dict(self)
        return json.dumps(data)
    
@receiver(post_save, sender=CounterTask)
def send_update(sender, **kwargs):
    new_entry = kwargs['instance']
    data_json = new_entry.to_json()
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)('counterTask_updates_channel', {
        'type': 'update.newdata',
        'text': 'A new entry was added to CounterTask',
        'data': data_json,
    })