from django.urls import path, include

from . import views

#The entries in this "webinterface/urls.py" file are responsible to call the correct view functions when an incoming connection wants to connect to the specific URL

urlpatterns = [
    path("config/", views.config_view, name="config_view"),
    path("ch_enable/", views.ch_enable),
    path("dgg_delay_width/", views.dgg_delay_width),
    path('thresholds_channel/', views.thresholds_channel),
    path('trg_thre_inv_w/', views.trg_thre_inv_w),
    path('trg_dtime/', views.trg_dtime),
    path('ch_pp_time/', views.ch_pp_time),
    path('stats/', views.stats_view, name="stats_view"),
    path('counter/', views.counter_view, name="counter_view"),
    path('start_counter/', views.counter_start),
    path('stop_counter/', views.counter_stop),
    path('the_django_plotly_dash/', include('django_plotly_dash.urls', namespace='the_django_plotly_dash')),
    path('test_generator', views.testgenerator_view, name="test_generator_view"),
    path('tg_dist_per/', views.tg_dist_per),
    path('tg_rep/', views.tg_rep),
    path('tg_ctrl/', views.tg_ctrl),
    path('tg_mask/', views.tg_mask),
    
]
