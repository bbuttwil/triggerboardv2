# Generated by Django 4.2.4 on 2023-10-30 16:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0007_countertask'),
    ]

    operations = [
        migrations.AddField(
            model_name='triggerconfig',
            name='thres_ch0',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Threshold dac value channel 0'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='triggerconfig',
            name='thres_ch1',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Threshold dac value channel 1'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='triggerconfig',
            name='thres_ch2',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Threshold dac value channel 2'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='triggerconfig',
            name='thres_ch3',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Threshold dac value channel 3'),
            preserve_default=False,
        ),
    ]
