import time
from celery import shared_task
from .trigobj import triggerObj
from webinterface.models import TriggerConfig, TriggerCounter

#These are the python objects of the triggerboard and i2c communication with the DACs
trigger = triggerObj.trigger
i2c = triggerObj.i2c
#TODO find a way to reload when the triggerboard device was power cycled

@shared_task
def trigger_conf_beat():
    '''
    It will gather the current information regarding the triggerboard config parameters and store them in the "Triggerconfig" model of django
    '''
    print("Gathering information")
    inv_busy, inv_inputs_mask, enable_busy, enable_inputs_mask = trigger.get_inv_ena()
    dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3 = trigger.get_dgg_width_delay()
    trg_inv, trg_thresh, trg_width = trigger.get_trg_thresh_inv_width()
    trg_dtime = trigger.get_trg_dtime()
    pp_time0, pp_time1, pp_time2, pp_time3 = trigger.get_pp_time()
    
    thres_ch0, thres_ch1, thres_ch2, thres_ch3 = i2c.last_dac_rep_values()
    
    TriggerConfig(inv_busy = inv_busy, inv_inputs_mask = inv_inputs_mask, enable_busy = enable_busy, enable_inputs_mask=enable_inputs_mask,
                  dgg_width=dgg_width, dgg_delay0=dgg_delay0, dgg_delay1=dgg_delay1, dgg_delay2=dgg_delay2, dgg_delay3=dgg_delay3,
                  trg_inv=trg_inv, trg_thresh=trg_thresh, trg_width=trg_width,
                  trg_dtime=trg_dtime,
                  pp_time0=pp_time0, pp_time1=pp_time1, pp_time2=pp_time2, pp_time3=pp_time3,
                  thres_ch0=thres_ch0, thres_ch1=thres_ch1, thres_ch2=thres_ch2, thres_ch3=thres_ch3).save()
    

@shared_task
def trigger_count_beat():
    '''
    This task can be started and stopped and will periodically count the triggerboard signals using the internal counter function.
    
    First it stops the counter, then starts a new counter cycle and waits until the time should have elapsed. After the time is over it reads
    the counter values and stores them inside the "TriggerCounter" model of django.
    '''
    while True:
        trigger.trg_cnt_cmd( 1 << 3, 0)                             #Stop counter
        trigger.set_trg_cnt(200000, 1000, (1 << 0) | (1 << 1), 0)    #Set prescale and endtime, clear and start counter 
        #  To calculate the time duration in [s] for which the counter will go use this formular,
        #  time duration [s] = prescale [#clks] * endtime [#clks] / 200MHz
        #  So for 1s use prescale 200000 and endtime 1000, for 0.1s use prescale 200000 and endtime 100
        time.sleep(1)
        counter_array = trigger.get_trg_cnt()                       #Stop counter and readout
        TriggerCounter(counter_prescale = counter_array[0], counter_endtime = counter_array[1], counter_status = counter_array[2], counter_timer = counter_array[3],
                    ch0_counter = counter_array[4], ch1_counter = counter_array[5], ch2_counter = counter_array[6], ch3_counter = counter_array[7], 
                    ch0_pp_counter = counter_array[8], ch1_pp_counter = counter_array[9], ch2_pp_counter = counter_array[10], ch3_pp_counter = counter_array[11],
                    trigger_counter = counter_array[12], trigger_nB_DT = counter_array[13], trigger_B_nDT = counter_array[14], trigger_B_DT = counter_array[15]).save()
        print(counter_array)