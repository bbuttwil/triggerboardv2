from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

from .tasks import trigger_conf_beat
from .models import CounterTask

from .trigobj import triggerObj

from . import plotly

#These are the python objects of the triggerboard and i2c communication with the DACs
trigger = triggerObj.trigger
i2c = triggerObj.i2c
#TODO find a way to reload when the triggerboard device was power cycled

def ch_enable(request):
    '''
    This view handles the HTTP request for "ch_enable/" and sets the values on the inputs channels of the Triggerboard to the recieved values.
    
    It recieves the ch_enable, ch_invert map and busy enable and invert boolean from the webinterface via an HTTP POST method.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        ch_enable_map = request.POST.get('ch_enable_map')
        ch_invert_map = request.POST.get('ch_invert_map')
        busy_enable = request.POST.get('busy_enable')
        busy_inverted = request.POST.get('busy_inverted')
        result = trigger.set_inv_ena(int(busy_enable), int(busy_inverted), int(ch_invert_map), int(ch_enable_map), 0)
        print("trig res: ", result) #The result is 0 when ok, but not something else when error
        trigger_conf_beat.apply()
    return JsonResponse({'result': result})

def dgg_delay_width(request):
    '''
    This view handles the HTTP request for "dgg_delay_width/" and sets the values on the inputs channels of the on the Triggerboard to the recieved values.
    
    It recieves the dgg_width and delays of each channel from the webinterface via an HTTP POST method.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        dgg_width = request.POST.get('gate_width')
        dgg_del0 = request.POST.get('ch0_delay')
        dgg_del1 = request.POST.get('ch1_delay')
        dgg_del2 = request.POST.get('ch2_delay')
        dgg_del3 = request.POST.get('ch3_delay')
        result = trigger.set_dgg_width_delay(int(dgg_width), int(dgg_del0), int(dgg_del1), int(dgg_del2), int(dgg_del3), 0)
        trigger_conf_beat.apply()
        
    return JsonResponse({'result': result})

def thresholds_channel(request):
    '''
    This view handles the HTTP request for "thresholds_channel/" and sets the values for the threshold voltages of the channels on the Triggerboard to the recieved values.
    
    It recieves the wished voltage level thres_ch* of each channel from the webinterface via an HTTP POST method and sets the value via the I2C functions.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        thres_ch0 = request.POST.get('ch0_thres')
        thres_ch1 = request.POST.get('ch1_thres')
        thres_ch2 = request.POST.get('ch2_thres')
        thres_ch3 = request.POST.get('ch3_thres')
        dac_value = i2c.ad5624_thr2dac(0, float(thres_ch0), 0)
        result = i2c.spi_wr(0, dac_value, 0)
        dac_value = i2c.ad5624_thr2dac(1, float(thres_ch1), 0)
        result |= i2c.spi_wr(1, dac_value, 0)
        dac_value = i2c.ad5624_thr2dac(2, float(thres_ch2), 0)
        result |= i2c.spi_wr(2, dac_value, 0)
        dac_value = i2c.ad5624_thr2dac(3, float(thres_ch3), 0)
        result |= i2c.spi_wr(3, dac_value, 0)
        #print("Result of threshold set: ", result)
        trigger_conf_beat.apply()
        
    return JsonResponse({'result': result})

def trg_thre_inv_w(request):
    '''
    This view handles the HTTP request for "trg_thre_inv_w/" and sets the values for the trigger settings on the Triggerboard to the recieved values.
    
    It recieves the trg_threshold (i.e. number of coincidental input signals), to invert the trigger and its width in clk cycles.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        trg_thres = request.POST.get('trg_thres')
        trg_inv = request.POST.get('trg_inv')
        trg_width = request.POST.get('trg_width')
        result = trigger.set_trg_thresh_inv_width(int(trg_inv), int(trg_thres), int(trg_width), 0)
        #result = trigger.set_trg_thresh_inv_width(0, 1, 20, 1)
        trigger_conf_beat.apply()
        
    return JsonResponse({'result': result})

def trg_dtime(request):
    '''
    This view handles the HTTP request for "trg_dtime/" and sets the values for the trigger settings on the Triggerboard to the recieved values.
    
    It recieves the trigger dead time for the trigger in clk cycles.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        trg_dtime = request.POST.get('trg_dtime')
        result = trigger.set_trg_dtime(int(trg_dtime), 0)
        trigger_conf_beat.apply()
        
    return JsonResponse({'result': result})

def ch_pp_time(request):
    '''
    This view handles the HTTP request for "ch_pp_time/" and sets the values on the inputs channels on the Triggerboard to the recieved values.
    
    It recieves the past protection time in clk cycles for each input channel and sets them in the triggerboard config.
    
    Returns:
        JsonResponse wether the settings change was successful or not
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        ch0_pp = request.POST.get('ch0_pp')
        ch1_pp = request.POST.get('ch1_pp')
        ch2_pp = request.POST.get('ch2_pp')
        ch3_pp = request.POST.get('ch3_pp')
        #Very dumb way, but was lazy and didnt want to add a function lol
        result = trigger.set_pp_time(0b0001, int(ch0_pp), 0)
        result = trigger.set_pp_time(0b0010, int(ch1_pp), 0)
        result = trigger.set_pp_time(0b0100, int(ch2_pp), 0)
        result = trigger.set_pp_time(0b1000, int(ch3_pp), 0)
        trigger_conf_beat.apply()

    return JsonResponse({'result': result})

def config_view(request):
    '''
    This view is responsible for displaying the actual "config/" page of the webinterface.
    
    It takes the current values for the triggerboard config and pastes them into the config.html file on loading the page.
    
    It returns the actual html page.
    '''
    threshold_channels = i2c.last_dac_rep_values()
    threshold_channels_v = [round(x/2**12 * 4.096 -2.048, 3) for x in threshold_channels] #This function converts between dac (which are readout) and the voltage level
    inv_busy, inv_inputs_mask, enable_busy, enable_inputs_mask = trigger.get_inv_ena()
    dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3 = trigger.get_dgg_width_delay()
    trg_inv, trg_thresh, trg_width = trigger.get_trg_thresh_inv_width()
    trg_dtime = trigger.get_trg_dtime()
    pp_time0, pp_time1, pp_time2, pp_time3 = trigger.get_pp_time()
    
    context = {'enable_ch0': 'ch-active' if (enable_inputs_mask & 0b0001) else '',
               'ch0_disabled': 'disabled' if not (enable_inputs_mask & 0b0001) else '',
               'ch0_inverted': 'ch-inv' if (inv_inputs_mask & 0b0001) else '',
               'enable_ch1': 'ch-active' if (enable_inputs_mask & 0b0010) else '',
               'ch1_disabled': 'disabled' if not (enable_inputs_mask & 0b0010) else '',
               'ch1_inverted': 'ch-inv' if (inv_inputs_mask & 0b0010) else '',
               'enable_ch2': 'ch-active' if (enable_inputs_mask & 0b0100) else '',
               'ch2_disabled': 'disabled' if not (enable_inputs_mask & 0b0100) else '',
               'ch2_inverted': 'ch-inv' if (inv_inputs_mask & 0b0100) else '',
               'enable_ch3': 'ch-active' if (enable_inputs_mask & 0b1000) else '',
               'ch3_disabled': 'disabled' if not (enable_inputs_mask & 0b1000) else '',
               'ch3_inverted': 'ch-inv' if (inv_inputs_mask & 0b1000) else '',
               'busy_enable': 'ch-active' if enable_busy else '',
               'busy_disabled': 'disabled' if not enable_busy else '',
               'busy_inverted': 'ch-inv' if inv_busy else '',
               'gate_width': dgg_width,
               'ch0_delay_value': dgg_delay0,
               'ch1_delay_value': dgg_delay1,
               'ch2_delay_value': dgg_delay2,
               'ch3_delay_value': dgg_delay3,
               'ch0_pp': pp_time0,
               'ch1_pp': pp_time1,
               'ch2_pp': pp_time2,
               'ch3_pp': pp_time3,
               'trg_threshold': trg_thresh,
               'trg_inv': 'ch-inv' if trg_inv else '',
               'trg_width': trg_width,
               'trg_dtime': trg_dtime,
               'threshold_channel0': threshold_channels_v[0],
               'threshold_channel1': threshold_channels_v[1],
               'threshold_channel2': threshold_channels_v[2],
               'threshold_channel3': threshold_channels_v[3],
               }
    
    return render(request, 'webinterface/config.html', context)

def stats_view(request):
    return render(request, 'webinterface/stats.html')



from celery.result import AsyncResult
def counter_view(request):
    '''
    This view is the actual html respond to a request for the "counter/" URL
    
    It fills in the current status of the TriggerCounterTask and return the HTML with inserted plotly plots.
    '''
    task = CounterTask.objects.last()
    task_id = task.task_id
    if task_id == None:
        task_id == 0
    result = AsyncResult(task_id)
    counter_text = 'Stop counter' if result.status == 'STARTED' else 'Start counter'
    # Return the Plotly graph as an HTML response
    return render(request, 'webinterface/counter.html', {'counter_daq_running': counter_text})


#TODO move the counter_start and counter_stop view function into one, to better handle the stop/start synchro?
from webinterface.tasks import trigger_count_beat
def counter_start(request):
    '''
    This view recieves the request to start the TiggerCounterTask on URL "start_counter/" and return an HttpResponse accordingly.
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        task = trigger_count_beat.apply_async()
        #request.session['task_id'] = str(task.id)
        #Either line above or below to save task id
        taskModel = CounterTask(task_id=task.id)
        taskModel.save()
        print("Got counter start request")
        return HttpResponse('Celery Beat started')
    return HttpResponse('Not HTTP POST request')

def counter_stop(request):
    '''
    This view recieves the request to stop the TiggerCounterTask on URL "stop_counter/" and return an HttpResponse accordingly.
    '''
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        from triggerboard_django.celery import app
        #task_id = request.session.get('task_id')
        task = CounterTask.objects.last()
        task_id = task.task_id
        if task_id is not None:
            app.control.revoke(task_id, terminate=True)
            taskModel = CounterTask(task_id="None")
            taskModel.save()
            return HttpResponse('Task stopped')
        print("Got counter stop request")
        
    return HttpResponse('Not HTTP POST request')

def testgenerator_view(request):
    '''
    This view returns the HTML to present the TriggerTestGenerator setting page "test_generator".
    '''
    return render(request, 'webinterface/testGenerator.html')

def tg_dist_per(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        tg_dist = request.POST.get('tg_dist')
        tg_per = request.POST.get('tg_per')
        result = trigger.set_tg_distance_period(int(tg_dist), int(tg_per), 0)
        print(result)
        
    return JsonResponse({'result': result})

def tg_rep(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        tg_rep = request.POST.get('tg_rep')
        result = trigger.set_tg_repetitions(int(tg_rep), 0, 0, 0)
        print(result)
        
    return JsonResponse({'result': result})

def tg_ctrl(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        tg_enable = request.POST.get('tg_enable')
        tg_rep = request.POST.get('tg_rep')
        result = trigger.set_tg_repetitions(int(tg_rep), int(tg_enable), int(tg_enable), 0)
        print(result)
        
    return JsonResponse({'result': result})


def tg_mask(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == 'POST':
        hexMask0 = request.POST.get('hexMask0')
        hexMask1 = request.POST.get('hexMask1')
        print("Hexmask0: ", hexMask0)
        print("Hexmask0: ", int(hexMask0, 16))
        print("Hexmask1:", int(hexMask1,16))
        result = trigger.set_tg_masks(int(hexMask0, 16), int(hexMask1, 16), 0)
        #trigger.print_tg_mask()
        print(result)
        
    return JsonResponse({'result': result})
