from webinterface.models import TriggerCounter
from django.db.models import Q

import plotly.graph_objs as go
from django_plotly_dash import DjangoDash
from dash import dcc, html
from dash.dependencies import Input, Output

import pandas as pd
from datetime import timedelta

def figCounter(data_df, counter_name):
    '''
    Take data from panda dataframe and put it into a binned plot.
    
    The data to plot is taken from the dataframe, it extracts the time and count information and plots them to Hz on a line plot.
    This is only one plot and the function gets called for all the different values on the counter page.
    
    Args:
        data_df (pd Dataframe): Data of the model entries to plot
        counter_name (string): The name of the plot
    
    Returns:
        plotly figure with the data and layout included
    '''
    print("counter_endtime: ", data_df['counter_endtime'][0])
    trace = go.Scatter(x=data_df['timestamp'], y=data_df[counter_name]*(200000000/(data_df['counter_prescale'][0]*data_df['counter_endtime'][0])), mode='lines', line_shape='hvh', connectgaps= False)
    layout = go.Layout(title=TriggerCounter._meta.get_field(counter_name).verbose_name, xaxis=dict(title='Time'), yaxis=dict(title='#', rangemode='nonnegative'))
    figure = go.Figure(data=trace, layout=layout)
    
    return figure

# Get the model entries for the TriggerCounts and filter them to only allow the last 1 minute of data
last_entry = TriggerCounter.objects.last().timestamp
pre_time = timedelta(minutes=1)
first_entry = last_entry-pre_time

data = TriggerCounter.objects.filter(Q(timestamp__gte=first_entry) & Q(timestamp__lte=last_entry))

df = pd.DataFrame.from_records(data.values())

app = DjangoDash('counters')

#Get the list of counters to plot
counters = []
for f in TriggerCounter._meta.concrete_fields:
    if not f.name in ["id", "timestamp", 'counter_prescale', 'counter_endtime', 'counter_status', 'counter_timer']:
        counters.append(f.name)



# This is the layout for the plotly section. This will be inserted into the "counter.html" file where it says {%load plotly_dash%} and {%plotly_direct name="counters"%}
# It is build from one label and input for the time window setting and 11 plots all below each other
app.layout = html.Div([
    html.Label(children=[
        'Last seconds to plot: ', 
        dcc.Input(
            id="input_timerange",
            type='number',
            placeholder="input last time range",
            value=60,
        )
    ])
    ] +
    [dcc.Graph(id=name, figure=figCounter(df, name)) for name in counters]
)



@app.callback(
[Output(name, 'figure') for name in counters],
[Input('input_timerange', 'value')])
def set_figure_timerange(timerangeS):
    '''
    This function is responsible for the time window setting
    
    It gets called when the value in the time window settings gets changed and will call the figures of the plots to update their data
    '''
    last_entry = TriggerCounter.objects.last().timestamp
    pre_time = timedelta(seconds=timerangeS)
    first_entry = last_entry-pre_time

    data = TriggerCounter.objects.filter(Q(timestamp__gte=first_entry) & Q(timestamp__lte=last_entry))

    df = pd.DataFrame.from_records(data.values())
    return [figCounter(df, name) for name in counters]