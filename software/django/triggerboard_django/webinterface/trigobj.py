import sys, os
# IMPORT the python wrapped C++ code to control the triggerboard
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(current_dir, '../../..', 'python')) #This line ads the current folder to the path, making triggerboard and its shared C libraries available to python
sys.path.append(os.path.join(current_dir, '../../..', 'C'))
import triggerboard

#print(triggerboard.__file__)


class triggerbrdClass():
    
    def __init__(self) -> None:
        '''
        Initialize new triggerboard object using the device path aliased udev rules
        '''
        #self.uart32 = triggerboard.uart32("/dev/triggerDev", 4000000)
        #self.i2c = triggerboard.i2c(self.uart32, 0x00080000 + 0x0080)
        #self.trigger = triggerboard.trigger(self.uart32, 0x00080000)
        
        self.uart32 = triggerboard.uart32("/dev/triggerDev0", 4000000)
        self.uart32.set_slave_mask(1)
        self.uart32.set_max_wsize(32)
        self.uart32.set_max_asize(24)
        self.trigger = triggerboard.trigger(self.uart32, 0x000)
        self.i2c = triggerboard.i2c(self.uart32, 0x0200)
    
    def newConnection(self):
        self.uart32 = triggerboard.uart32("/dev/triggerDev0", 4000000)
        self.uart32.set_slave_mask(1)
        self.uart32.set_max_wsize(32)
        self.uart32.set_max_asize(24)
        self.trigger = triggerboard.trigger(self.uart32, 0x000)
        self.i2c = triggerboard.i2c(self.uart32, 0x0200)
        
    def getConfArray(self):
        inv_busy, inv_inputs_mask, enable_busy, enable_inputs_mask = self.trigger.get_inv_ena()
        dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3 = self.trigger.get_dgg_width_delay()
        trg_inv, trg_thresh, trg_width = self.trigger.get_trg_thresh_inv_width()
        trg_dtime = self.trigger.get_trg_dtime()
        pp_time0, pp_time1, pp_time2, pp_time3 = self.trigger.get_pp_time()
        return [inv_busy, inv_inputs_mask, enable_busy, enable_inputs_mask,
                dgg_width, dgg_delay0, dgg_delay1, dgg_delay2, dgg_delay3,
                trg_inv, trg_thresh, trg_width,
                trg_dtime,
                pp_time0, pp_time1, pp_time2, pp_time3]
            

triggerObj = triggerbrdClass()